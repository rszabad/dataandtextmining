package processing;

import java.awt.Dimension;
import java.text.DecimalFormat;
import java.util.ArrayList;

import model.Cities;
import model.City;
import model.Connection;
import model.GroupedConnections;
import processing.core.*;
import traer.physics.*;

/**
 * Nimmt die eigentliche Visualisierung der �bergebenen Daten vor
 * 
 * @author rszabad
 *
 */

public class PSketch extends PApplet {

	private static final long serialVersionUID = -7315986107328071732L;

	private int citySize = 10;

	PImage europeMapImage = null;
	PImage europeMapImage2 = null;
	PImage loadingImage = null;

	// Zeitstempel um Einblenden des Ladesymbols zu erm�glichen
	long lastTime = 0;
	// Ausgew�hlter Visualisierungs-Schritt,
	// bei Visualisierungen mit mehreren Werten
	private int chosenStep = 0;

	// Zusammengefasste Verbindungen
	private GroupedConnections groupedConnections = null;
	// Liste aller beteiligten St�dte
	private Cities involvedCities = null;
	// Liste der Referenzwerte f�r die Verbindungen
	private GroupedConnections referenceConnections = null;

	// F�r Berechnung des Hintergrundbildes, wird von Aussen stets ge�ndert
	private int appWidth = 50;
	private int appHeight = 50;

	// F�r Umrechnung von Lat/Long in px ben�tigte Angaben zu R�ndern des
	// Kartenausschnitts
	// werden von verwendetem TileMill geliefert
	/*
	 * private float boundsLeft = (float) 5.1636; private float boundsRight =
	 * (float) 18.4351; private float boundsTop = (float) 55.2666; private float
	 * boundsBottom = (float) 46.9653; // R�ckgabeliste aus TileMill // : Left,
	 * Bottom, Right, // Top
	 */

	private float boundsLeft = (float) 3.0762;
	private float boundsRight = (float) 18.1274;
	private float boundsTop = (float) 55.7766;
	private float boundsBottom = (float) 46.3621; // R�ckgabeliste aus TileMill
													// : Left, Bottom, Right,
													// Top

	// Physic
	private ParticleSystem particleSystem = new ParticleSystem(0, (float) 1.0);
	// Liste der Physic-Partikel der St�dte f�r Visualisierung
	private ArrayList<Particle> cityParticleList = new ArrayList<Particle>();
	// Liste der Physic-Federn der St�dte f�r die Visualisierung
	private ArrayList<Spring> citySpringList = new ArrayList<Spring>();
	// Listen f�r Partikel und Federn f�r Ankerpunkte
	private ArrayList<Particle> anchorParticleList = new ArrayList<Particle>();
	private ArrayList<Spring> anchorSpringList = new ArrayList<Spring>();
	// Anzeigeflags
	private boolean showAnchor = false;
	private boolean showInfo = false;
	private String factor = "time/distance";
	private String visualisation = "Map Simple";
	private boolean threadDone = false;
	private boolean threadWorking = false;
	public PImage destination = null;
	PImage tempDestination = null;

	private String base = "All Dataset Connections";

	private String carTrain = "Car";

	private boolean record = false;

	private String path;

	DecimalFormat df = new DecimalFormat();

	private boolean normal = false;

	/**
	 * Vorbereitung verschiedener Processing-Parameter f�r das Verhalten der
	 * Sketches
	 */
	public void setup() {

		size(50, 50);
		smooth();
		frameRate(30);
		frame.setResizable(true);

		if (appHeight > 0 && appWidth > 0) {
			// setSize(appHeight, appHeight);
			this.setSize(appWidth, appHeight);
		}
		loadingImage = loadImage("loading.png");
		lastTime = millis();

		df.setMaximumFractionDigits(4);

	}

	/**
	 * Erzeugt die grafische Ausgabe der Sketches, l�uft ab Sketch-Start in
	 * Dauerschleife mit der in FrameRate festgelegten Wiederholrate
	 * 
	 */
	public void draw() {

		// dieses Flag ist gesetzt, wenn "Screenshot" gedr�ckt wurde
		// einen Ausgabevorgang lang wird die Visualisierung in ein PDF
		// geschrieben, dann das Flag wieder gel�scht.
		if (this.record == true && !this.visualisation.equals("Table")) {
			beginRecord(PDF, this.path + ".pdf");
		}

		Dimension d = new Dimension(appWidth, appHeight);
		if (appHeight > 0 && appWidth > 0 && this.getSize() != d) {
			this.setSize(appWidth, appHeight);
		}

		// wei�en des Hintergrundes = l�schen der bisherigen Visualisierung
		// Processing zeichnet alles neue �bereinander
		background(255);

		// bleibt wei�, wenn keine Daten vorhanden
		if (groupedConnections == null) {
			fill(255);
			rect(0, 0, appWidth, appHeight);
		}

		// Wenn Anzeigedatenvorhanden und "Ladezeit" abgelaufen
		if (millis() - lastTime < 1000 && groupedConnections != null) {
			image(loadingImage, 0, 0);
		}
		// Wenn "Ladezeit" abgelaufen und Anzeigedaten vorhanden
		if (millis() - lastTime > 1000) {
			if (groupedConnections != null) {

				// Wenn keine berechneten Visualisierungsdaten vorhanden =
				// Programmstart oder Gr��en�nderung
				if (involvedCities == null) {

					// Berechnen und anlegen von:
					// St�dten als Partikel, X/Y-Werte f�r St�dte, X/Y Werte in
					// Verbindungen
					computeCities();

					// Erzeugt Ankerpunkte und Springs f�r St�dte
					computeAnchors();

					// Erzeugt Springs f�r Verbindungen zwischen St�dten
					computeSprings();

					// Hintergrundkarte laden, Gr��e anpassen, ausgeben
					try {
						europeMapImage = loadImage("dtm2_cebe4e.png");
						europeMapImage.resize(appWidth, appHeight);
						europeMapImage2 = loadImage("dtm2_cebe4e.png");
						// europeMapImage2 = loadImage("dtm2_d395e7.png");
						europeMapImage2.resize(appWidth, appHeight);
						// image(europeMapImage, 0, 0);
					} catch (Exception e) {
						e.printStackTrace();
					}
				}

				// Restlengths f�r CitySprings berechnen
				// Normale L�nge der jeweiligen Spring wird mit Faktor ver�ndert
				// und neue L�nge in Liste geschrieben
				if (base.equals("Chosen Connections")) {
					for (int i = 0; i < groupedConnections.sizeOfGroups(); i++) {
						Connection temp = groupedConnections.getConnection(chosenStep, i);
						float floatFactor = 1;

						if (normal == false) {

							if (this.factor.equals("time/distance")) {
								// floatFactor = (((float) temp.getAvg()) /
								// ((float)
								// referenceConnections.getConnection(1,
								// i).getAvg()));
								floatFactor = (((float) referenceConnections.getConnection(1, i).getAvg())
										/ ((float) temp.getAvg()));
							}
							if (this.factor.equals("time")) {
								// floatFactor = ((float) temp.getDuration() /
								// (float)
								// referenceConnections.getConnection(1,i).getDuration());
								floatFactor = ((float) referenceConnections.getConnection(1, i).getDuration()
										/ (float) temp.getDuration());
							}
						}
						citySpringList.get(i).setRestLength((float) temp.getNormalLength() * floatFactor);
					}
				}

				if (base.equals("All Dataset Connections")) {
					for (int i = 0; i < groupedConnections.sizeOfGroups(); i++) {
						Connection temp = groupedConnections.getConnection(chosenStep, i);
						float floatFactor = 1;

						if (normal == false) {
							if (this.factor.equals("time/distance")) {
								// floatFactor = (((float) temp.getAvg()) /
								// ((float)
								// referenceConnections.getConnection(0,
								// i).getAvg()));
								floatFactor = (((float) referenceConnections.getConnection(0, i).getAvg())
										/ ((float) temp.getAvg()));
							}
							if (this.factor.equals("time")) {
								// floatFactor = ((float) temp.getDuration() /
								// (float)
								// referenceConnections.getConnection(0,i).getDuration());
								floatFactor = ((float) referenceConnections.getConnection(0, i).getDuration()
										/ (float) temp.getDuration());
							}
						}
						citySpringList.get(i).setRestLength((float) temp.getNormalLength() * floatFactor);
					}
				}

				if (base.equals("Common Connections")) {
					for (int i = 0; i < groupedConnections.sizeOfGroups(); i++) {
						Connection temp = groupedConnections.getConnection(chosenStep, i);
						float floatFactor = 1;
						if (normal == false) {
							if (this.factor.equals("time/distance")) {
								// floatFactor = (((float) temp.getAvg()) /
								// ((float)
								// referenceConnections.getConnection(2,
								// i).getAvg()));
								floatFactor = (((float) referenceConnections.getConnection(2, i).getAvg())
										/ ((float) temp.getAvg()));
							}
							if (this.factor.equals("time")) {
								// floatFactor = ((float) temp.getDuration() /
								// (float)
								// referenceConnections.getConnection(2,i).getDuration());
								floatFactor = ((float) referenceConnections.getConnection(2, i).getDuration()
										/ (float) temp.getDuration());
							}
						}
						citySpringList.get(i).setRestLength((float) temp.getNormalLength() * floatFactor);
					}
				}

				// Physicsimulation einen Frame weiterschalten
				particleSystem.tick();
				// Wei�e Hintergrundfarbe
				// fill(255);

				if (visualisation.equals("Table")) {
					fill(111, 111, 111);
					rect(10, 10, 100, 100);
				}

				if (visualisation.equals("Map Simple") || visualisation.equals("Map Warped")) {

					background(255);

					if (visualisation.equals("Map Warped")) {

						if (threadWorking == false) {
							threadWorking = true;
							thread("calculateWarp");
						}

						if (threadDone == true) {

							destination = new PImage(tempDestination.width, tempDestination.height);
							destination.copy(tempDestination, 0, 0, tempDestination.width, tempDestination.height, 0, 0,
									tempDestination.width, tempDestination.height);

							threadWorking = false;
							threadDone = false;
						}

						if (destination != null) {

							image(destination, 0, 0);
						}

					}

					else {
						// Ausgeben des Hintergrundkartenbildes
						if (europeMapImage != null) {
							image(europeMapImage, 0, 0);
						}

					}

					if (showAnchor) {
						// Ankerverbindungen
						for (int i = 0; i < anchorSpringList.size(); i++) {

							stroke(255, 0, 0);
							line(anchorSpringList.get(i).getOneEnd().position().x(),
									anchorSpringList.get(i).getOneEnd().position().y(),
									anchorSpringList.get(i).getTheOtherEnd().position().x(),
									anchorSpringList.get(i).getTheOtherEnd().position().y());
						}

						// Anker
						for (int i = 0; i < anchorParticleList.size(); i++) {
							stroke(255, 0, 0);
							fill(255);
							ellipse(anchorParticleList.get(i).position().x(), anchorParticleList.get(i).position().y(),
									5, 5);
						}
					}
					// St�dteverbindungen
					for (int i = 0; i < citySpringList.size(); i++) {

						Spring temp = citySpringList.get(i);
						stroke(111, 111, 111);
						fill(111, 111, 111);
						line(temp.getOneEnd().position().x(), temp.getOneEnd().position().y(),
								temp.getTheOtherEnd().position().x(), temp.getTheOtherEnd().position().y());

					}

					// St�dte
					for (int i = 0; i < cityParticleList.size(); i++) {
						stroke(0);
						fill(255);
						ellipse(cityParticleList.get(i).position().x(), cityParticleList.get(i).position().y(),
								citySize, citySize);

						// St�dtename
						fill(0);
						textSize(12);
						textAlign(LEFT, TOP);
						text(involvedCities.get(i).getName().substring(0, involvedCities.get(i).getName().indexOf(",")),
								cityParticleList.get(i).position().x() + citySize / 2,
								cityParticleList.get(i).position().y() + citySize / 2);

					}

					// Informationen zu Verbindung
					for (int i = 0; i < citySpringList.size(); i++) {

						Spring temp = citySpringList.get(i);
						stroke(0);

						if (showInfo) {

							PVector A = new PVector(temp.getOneEnd().position().x(), temp.getOneEnd().position().y());
							PVector B = new PVector(temp.getTheOtherEnd().position().x(),
									temp.getTheOtherEnd().position().y());

							fill(200, 200, 0);

							A.sub(B);
							A.div(2);
							B.add(A);

							fill(0);
							textSize(10);
							textLeading(11);
							textAlign(LEFT, TOP);
							String s = "Distance: " + (int) referenceConnections.get(i).getDistance() + " km\n";
							s += "Travel-Time: " + (int) groupedConnections.getConnection(chosenStep, i).getDuration()
									+ " min\n";
							s += "Average: " + df.format(groupedConnections.getConnection(chosenStep, i).getAvg())
									+ " kilometres/min";

							fill(73, 193, 255, 80);
							noStroke();
							// rect(B.x - 75, B.y - 25, 110, 40);
							rect(B.x, B.y, 115, 45);
							fill(0);
							text(s, B.x + 5, B.y + 5, 115, 45);
							fill(0);
							rect(B.x, B.y, 4, 4);

						}

					}

					// Info oben rechts zur Base
					noStroke();
					fill(255);
					rect(appWidth - 175, 0, 175, 40);

					fill(color(0, 0, 0));
					textSize(11);
					textAlign(RIGHT, TOP);
					text(base, appWidth, 0);
					if (base.equals("All Dataset Connections")) {
						text("Base-Average: " + df.format(referenceConnections.getConnection(0, 0).getAvg())
								+ " km/min", appWidth, 12);
					}
					if (base.equals("Chosen Connections")) {
						text("Base-Average: " + df.format(referenceConnections.getConnection(1, 0).getAvg())
								+ " km/min", appWidth, 12);
					}
					if (base.equals("Common Connections")) {
						text("Base-Average: " + df.format(referenceConnections.getConnection(2, 0).getAvg())
								+ " km/min", appWidth, 12);
					}
					if (groupedConnections.size() > 10) {
						text("Date: 2016 January " + (chosenStep + 1), appWidth, 25);
					}

				}

				if (visualisation.equals("Bar Graph")) {

					if (base.equals("All Dataset Connections")) {
						// Balkenfarbe
						// int colors[] = {color(16,75,245), color(13,161,255)
						// ,color(0,224,232)};
						int colors[] = { color(73, 193, 255, 200), color(78, 226, 232, 200), color(97, 150, 245, 200) };
						// int colors[] = {color(8,64,129) ,color(103,193,203),
						// color(180,226,186)};
						// Legende
						String legends[] = { "Distance in kilometres", "Travel time in minutes",
								"Average kilometres/minutes" };

						int w = appWidth;
						int h = appHeight;
						// int upper = 40;
						// int lower = h - 70

						// maximalWerte der Anzeige
						float[] maxima = { 1000, 1000, 4 };

						strokeWeight(1);
						stroke(0);
						fill(255);
						rect(0, 40, w - 1, h - 41);
						line(0, h - 70, w, h - 70);
						noStroke();

						// Legende ausf�llen
						for (int i = 0; i < 3; i++) {
							fill(colors[i]);
							rect(0, 0 + (i * 12), 10, 10);

							fill(color(0, 0, 0));
							textSize(11);
							textAlign(LEFT, TOP);
							text(legends[i], 15, 0 + (i * 12));
						}

						fill(color(0, 0, 0));
						textSize(11);
						textAlign(RIGHT, TOP);
						text(base, w, 0);
						text("Base-Average: " + df.format(referenceConnections.getConnection(0, 0).getAvg())
								+ " km/min", w, 12);

						int numberOfData = groupedConnections.sizeOfGroups();

						// Linien
						strokeWeight(1);
						stroke(111, 111, 111, 150);

						for (int i = 1; i < 10; i++) {
							dashline(0, 40 + ((h - 110) / 10 * i), w, 40 + ((h - 110) / 10 * i), 10, 5);
						}

						strokeWeight(2);

						stroke(colors[0]);
						line(0, h - 70 - map(referenceConnections.get(0).getDistance(), 0, maxima[0], 0, h - 110), w,
								h - 70 - map(referenceConnections.get(0).getDistance(), 0, maxima[0], 0, h - 110));

						stroke(colors[1]);
						line(0, h - 70 - map(referenceConnections.get(0).getDuration(), 0, maxima[1], 0, h - 110), w,
								h - 70 - map(referenceConnections.get(0).getDuration(), 0, maxima[1], 0, h - 110));

						stroke(colors[2]);
						// line(0,
						// h-70-map(referenceConnections.get(0).getDuration()/referenceConnections.get(0).getDistance(),
						// 0, maxima[2], 0, h-110) ,w ,
						// h-70-map(referenceConnections.get(0).getDuration()/referenceConnections.get(0).getDistance(),
						// 0, maxima[2], 0, h-110));
						line(0, h - 70
								- map(referenceConnections.get(0).getDistance()
										/ referenceConnections.get(0).getDuration(), 0, maxima[2], 0, h - 110),
								w, h
										- 70 - map(
												referenceConnections.get(0).getDistance()
														/ referenceConnections.get(0).getDuration(),
												0, maxima[2], 0, h - 110));

						for (int i = 0; i < numberOfData; i++) {

							noStroke();
							strokeWeight(1);
							stroke(0, 0, 0, 150);
							int barWidth = w / numberOfData / 5;
							fill(colors[0]);
							rect(barWidth * (float) 2.5 - barWidth / 2 - barWidth - 2 + (w / numberOfData * i),
									h - 70 - map(groupedConnections.getConnection(chosenStep, i).getDistance(), 0,
											maxima[0], 0, h - 110),
									barWidth, map(groupedConnections.getConnection(chosenStep, i).getDistance(), 0,
											maxima[0], 0, h - 110),
									5, 5, 0, 0);

							fill(colors[1]);
							rect(barWidth * (float) 2.5 - barWidth / 2 + (w / numberOfData * i),
									h - 70 - map(groupedConnections.getConnection(chosenStep, i).getDuration(), 0,
											maxima[1], 0, h - 110),
									barWidth, map(groupedConnections.getConnection(chosenStep, i).getDuration(), 0,
											maxima[1], 0, h - 110),
									5, 5, 0, 0);

							fill(colors[2]);
							// rect(barWidth*(float)2.5-barWidth/2 + barWidth +
							// 2 + (w/numberOfData * i),
							// h-70-map(groupedConnections.getConnection(chosenStep,
							// i).getDuration() /
							// groupedConnections.getConnection(chosenStep,
							// i).getDistance(), 0, maxima[2], 0, h-110),
							// barWidth,
							// map(groupedConnections.getConnection(chosenStep,
							// i).getDuration() /
							// groupedConnections.getConnection(chosenStep,
							// i).getDistance(), 0, maxima[2], 0, h-110), 5, 5 ,
							// 0, 0);
							rect(barWidth * (float) 2.5 - barWidth / 2 + barWidth + 2 + (w / numberOfData * i),
									h - 70 - map(
											groupedConnections.getConnection(chosenStep, i).getDistance()
													/ groupedConnections.getConnection(chosenStep, i).getDuration(),
											0, maxima[2], 0, h - 110),
									barWidth,
									map(groupedConnections.getConnection(chosenStep, i).getDistance()
											/ groupedConnections.getConnection(chosenStep, i).getDuration(), 0,
											maxima[2], 0, h - 110),
									5, 5, 0, 0);

							if (showInfo) {
								textSize(9);
								textLeading(8);

								pushMatrix();
								translate(barWidth * (float) 2.5 - barWidth + (w / numberOfData * i) - 2,
										h - 70 - map(groupedConnections.getConnection(chosenStep, i).getDistance(), 0,
												maxima[0], 0, h - 110) + 5);
								rotate(3 * PI / 2);
								fill(color(0, 0, 0));

								textAlign(RIGHT, CENTER);
								String text = Integer
										.toString((int) groupedConnections.getConnection(chosenStep, i).getDistance());
								text(text, 0, 0);
								popMatrix();

								pushMatrix();
								translate(barWidth * (float) 2.5 + (w / numberOfData * i),
										h - 70 - map(groupedConnections.getConnection(chosenStep, i).getDuration(), 0,
												maxima[1], 0, h - 110) + 5);
								rotate(3 * PI / 2);
								fill(color(0, 0, 0));

								textAlign(RIGHT, CENTER);
								text = Integer
										.toString((int) groupedConnections.getConnection(chosenStep, i).getDuration());
								text(text, 0, 0);
								popMatrix();

								pushMatrix();
								translate(barWidth * (float) 2.5 + barWidth + (w / numberOfData * i) + 2,
										h - 70 - map(
												groupedConnections.getConnection(chosenStep, i).getDistance()
														/ groupedConnections.getConnection(chosenStep, i).getDuration(),
												0, maxima[2], 0, h - 110) + 5);
								rotate(3 * PI / 2);
								fill(color(0, 0, 0));

								textAlign(RIGHT, CENTER);
								text = Float.toString(groupedConnections.getConnection(chosenStep, i).getAvg());
								text(text, 0, 0);
								popMatrix();

							}
							pushMatrix();
							translate(barWidth * (float) 2.5 + (w / numberOfData * i), h - 70 + 7);
							rotate(3 * PI / 2);
							fill(color(0, 0, 0));
							textSize(10);
							textLeading(9);
							textAlign(RIGHT, CENTER);
							String textOrig = groupedConnections.getConnection(chosenStep, i).getOrigin();
							String textDest = groupedConnections.getConnection(chosenStep, i).getDestination();
							textOrig = textOrig.substring(0, textOrig.indexOf(","));
							textOrig += "\n";
							textOrig += textDest.substring(0, textDest.indexOf(","));
							text(textOrig, 0, 0);
							popMatrix();
						}

					}

					if (base.equals("Chosen Connections")) {
						// Balkenfarbe
						// int colors[] = {color(16,75,245), color(13,161,255)
						// ,color(0,224,232)};
						int colors[] = { color(73, 193, 255, 200), color(78, 226, 232, 200), color(97, 150, 245, 200) };
						// int colors[] = {color(8,64,129) ,color(103,193,203),
						// color(180,226,186)};
						// Legende
						String legends[] = { "Distance in kilometres", "Travel time in minutes",
								"Average kilometres/minutes" };

						int w = appWidth;
						int h = appHeight;

						// maximalWerte der Anzeige
						float[] maxima = { 1000, 1000, 4 };

						strokeWeight(1);
						stroke(0);
						fill(255);
						rect(0, 40, w - 1, h - 41);
						line(0, h - 70, w, h - 70);
						noStroke();

						// Legende ausf�llen
						for (int i = 0; i < 3; i++) {
							fill(colors[i]);
							rect(0, 0 + (i * 12), 10, 10);

							fill(color(0, 0, 0));
							textSize(11);
							textAlign(LEFT, TOP);
							text(legends[i], 15, 0 + (i * 12));
						}

						fill(color(0, 0, 0));
						textSize(11);
						textAlign(RIGHT, TOP);
						text(base, w, 0);
						text("Base-Average: " + df.format(referenceConnections.getConnection(1, 1).getAvg())
								+ " km/min", w, 12);

						int numberOfData = groupedConnections.sizeOfGroups();

						// Linien
						strokeWeight(1);
						stroke(111, 111, 111, 150);

						for (int i = 1; i < 10; i++) {

							dashline(0, 40 + ((h - 110) / 10 * i), w, 40 + ((h - 110) / 10 * i), 10, 5);
						}

						strokeWeight(2);

						stroke(colors[0]);
						line(0, h - 70
								- map(referenceConnections.getConnection(1, 0).getDistance(), 0, maxima[0], 0, h - 110),
								w, h - 70 - map(referenceConnections.getConnection(1, 0).getDistance(), 0, maxima[0], 0,
										h - 110));

						stroke(colors[1]);
						line(0, h - 70
								- map(referenceConnections.getConnection(1, 0).getDuration(), 0, maxima[1], 0, h - 110),
								w, h - 70 - map(referenceConnections.getConnection(1, 0).getDuration(), 0, maxima[1], 0,
										h - 110));

						stroke(colors[2]);
						line(0, h - 70
								- map(referenceConnections.getConnection(1, 0).getDistance()
										/ referenceConnections.getConnection(1, 0).getDuration(), 0, maxima[2], 0,
								h - 110),
								w,
								h - 70 - map(
										referenceConnections.getConnection(1, 0).getDistance()
												/ referenceConnections.getConnection(1, 0).getDuration(),
										0, maxima[2], 0, h - 110));

						for (int i = 0; i < numberOfData; i++) {

							noStroke();
							strokeWeight(1);
							stroke(0, 0, 0, 150);
							int barWidth = w / numberOfData / 5;
							fill(colors[0]);
							rect(barWidth * (float) 2.5 - barWidth / 2 - barWidth - 2 + (w / numberOfData * i),
									h - 70 - map(groupedConnections.getConnection(chosenStep, i).getDistance(), 0,
											maxima[0], 0, h - 110),
									barWidth, map(groupedConnections.getConnection(chosenStep, i).getDistance(), 0,
											maxima[0], 0, h - 110),
									5, 5, 0, 0);

							fill(colors[1]);
							rect(barWidth * (float) 2.5 - barWidth / 2 + (w / numberOfData * i),
									h - 70 - map(groupedConnections.getConnection(chosenStep, i).getDuration(), 0,
											maxima[1], 0, h - 110),
									barWidth, map(groupedConnections.getConnection(chosenStep, i).getDuration(), 0,
											maxima[1], 0, h - 110),
									5, 5, 0, 0);

							fill(colors[2]);
							rect(barWidth * (float) 2.5 - barWidth / 2 + barWidth + 2 + (w / numberOfData * i),
									h - 70 - map(
											groupedConnections.getConnection(chosenStep, i).getDistance()
													/ groupedConnections.getConnection(chosenStep, i).getDuration(),
											0, maxima[2], 0, h - 110),
									barWidth,
									map(groupedConnections.getConnection(chosenStep, i).getDistance()
											/ groupedConnections.getConnection(chosenStep, i).getDuration(), 0,
											maxima[2], 0, h - 110),
									5, 5, 0, 0);

							if (showInfo) {
								textSize(9);
								textLeading(8);

								pushMatrix();
								translate(barWidth * (float) 2.5 - barWidth + (w / numberOfData * i) - 2,
										h - 70 - map(groupedConnections.getConnection(chosenStep, i).getDistance(), 0,
												maxima[0], 0, h - 110) + 5);
								rotate(3 * PI / 2);
								fill(color(0, 0, 0));

								textAlign(RIGHT, CENTER);
								String text = Integer
										.toString((int) groupedConnections.getConnection(chosenStep, i).getDistance());
								text(text, 0, 0);
								popMatrix();

								pushMatrix();
								translate(barWidth * (float) 2.5 + (w / numberOfData * i),
										h - 70 - map(groupedConnections.getConnection(chosenStep, i).getDuration(), 0,
												maxima[1], 0, h - 110) + 5);
								rotate(3 * PI / 2);
								fill(color(0, 0, 0));

								textAlign(RIGHT, CENTER);
								text = Integer
										.toString((int) groupedConnections.getConnection(chosenStep, i).getDuration());
								text(text, 0, 0);
								popMatrix();

								pushMatrix();
								translate(barWidth * (float) 2.5 + barWidth + (w / numberOfData * i) + 2,
										h - 70 - map(
												groupedConnections.getConnection(chosenStep, i).getDistance()
														/ groupedConnections.getConnection(chosenStep, i).getDuration(),
												0, maxima[2], 0, h - 110) + 5);
								rotate(3 * PI / 2);
								fill(color(0, 0, 0));

								textAlign(RIGHT, CENTER);
								text = Float.toString(groupedConnections.getConnection(chosenStep, i).getAvg());
								text(text, 0, 0);
								popMatrix();

							}
							pushMatrix();
							translate(barWidth * (float) 2.5 + (w / numberOfData * i), h - 70 + 7);
							rotate(3 * PI / 2);
							fill(color(0, 0, 0));
							textSize(10);
							textLeading(9);
							textAlign(RIGHT, CENTER);
							String textOrig = groupedConnections.getConnection(chosenStep, i).getOrigin();
							String textDest = groupedConnections.getConnection(chosenStep, i).getDestination();
							textOrig = textOrig.substring(0, textOrig.indexOf(","));
							textOrig += "\n";
							textOrig += textDest.substring(0, textDest.indexOf(","));
							text(textOrig, 0, 0);
							popMatrix();
						}

					}

					if (base.equals("Common Connections")) {
						// Balkenfarbe
						// int colors[] = {color(16,75,245), color(13,161,255)
						// ,color(0,224,232)};
						int colors[] = { color(73, 193, 255, 200), color(78, 226, 232, 200), color(97, 150, 245, 200) };
						// int colors[] = {color(8,64,129) ,color(103,193,203),
						// color(180,226,186)};
						// Legende
						String legends[] = { "Distance in kilometres", "Travel time in minutes",
								"Average kilometres/minutes" };

						int w = appWidth;
						int h = appHeight;

						// maximalWerte der Anzeige
						float[] maxima = { 1000, 1000, 4 };

						strokeWeight(1);
						stroke(0);
						fill(255);
						rect(0, 40, w - 1, h - 41);
						line(0, h - 70, w, h - 70);
						noStroke();

						// Legende ausf�llen
						for (int i = 0; i < 3; i++) {
							fill(colors[i]);
							rect(0, 0 + (i * 12), 10, 10);

							fill(color(0, 0, 0));
							textSize(11);
							textAlign(LEFT, TOP);
							text(legends[i], 15, 0 + (i * 12));
						}

						fill(color(0, 0, 0));
						textSize(11);
						textAlign(RIGHT, TOP);
						text(base, w, 0);
						text("Base-Average: " + df.format(referenceConnections.getConnection(2, 1).getAvg())
								+ " km/min", w, 12);

						int numberOfData = groupedConnections.sizeOfGroups();

						// Linien
						strokeWeight(1);
						stroke(111, 111, 111, 150);

						for (int i = 1; i < 10; i++) {

							dashline(0, 40 + ((h - 110) / 10 * i), w, 40 + ((h - 110) / 10 * i), 10, 5);
						}

						strokeWeight(2);

						stroke(colors[0]);
						line(0, h - 70
								- map(referenceConnections.getConnection(2, 0).getDistance(), 0, maxima[0], 0, h - 110),
								w, h - 70 - map(referenceConnections.getConnection(2, 0).getDistance(), 0, maxima[0], 0,
										h - 110));

						stroke(colors[1]);
						line(0, h - 70
								- map(referenceConnections.getConnection(2, 0).getDuration(), 0, maxima[1], 0, h - 110),
								w, h - 70 - map(referenceConnections.getConnection(2, 0).getDuration(), 0, maxima[1], 0,
										h - 110));

						stroke(colors[2]);
						line(0, h - 70
								- map(referenceConnections.getConnection(2, 0).getDistance()
										/ referenceConnections.getConnection(2, 0).getDuration(), 0, maxima[2], 0,
								h - 110),
								w,
								h - 70 - map(
										referenceConnections.getConnection(2, 0).getDistance()
												/ referenceConnections.getConnection(2, 0).getDuration(),
										0, maxima[2], 0, h - 110));

						for (int i = 0; i < numberOfData; i++) {

							noStroke();
							strokeWeight(1);
							stroke(0, 0, 0, 150);
							int barWidth = w / numberOfData / 5;
							fill(colors[0]);
							rect(barWidth * (float) 2.5 - barWidth / 2 - barWidth - 2 + (w / numberOfData * i),
									h - 70 - map(groupedConnections.getConnection(chosenStep, i).getDistance(), 0,
											maxima[0], 0, h - 110),
									barWidth, map(groupedConnections.getConnection(chosenStep, i).getDistance(), 0,
											maxima[0], 0, h - 110),
									5, 5, 0, 0);

							fill(colors[1]);
							rect(barWidth * (float) 2.5 - barWidth / 2 + (w / numberOfData * i),
									h - 70 - map(groupedConnections.getConnection(chosenStep, i).getDuration(), 0,
											maxima[1], 0, h - 110),
									barWidth, map(groupedConnections.getConnection(chosenStep, i).getDuration(), 0,
											maxima[1], 0, h - 110),
									5, 5, 0, 0);

							fill(colors[2]);
							rect(barWidth * (float) 2.5 - barWidth / 2 + barWidth + 2 + (w / numberOfData * i),
									h - 70 - map(
											groupedConnections.getConnection(chosenStep, i).getDistance()
													/ groupedConnections.getConnection(chosenStep, i).getDuration(),
											0, maxima[2], 0, h - 110),
									barWidth,
									map(groupedConnections.getConnection(chosenStep, i).getDistance()
											/ groupedConnections.getConnection(chosenStep, i).getDuration(), 0,
											maxima[2], 0, h - 110),
									5, 5, 0, 0);

							if (showInfo) {
								textSize(9);
								textLeading(8);

								pushMatrix();
								translate(barWidth * (float) 2.5 - barWidth + (w / numberOfData * i) - 2,
										h - 70 - map(groupedConnections.getConnection(chosenStep, i).getDistance(), 0,
												maxima[0], 0, h - 110) + 5);
								rotate(3 * PI / 2);
								fill(color(0, 0, 0));

								textAlign(RIGHT, CENTER);
								String text = Integer
										.toString((int) groupedConnections.getConnection(chosenStep, i).getDistance());
								text(text, 0, 0);
								popMatrix();

								pushMatrix();
								translate(barWidth * (float) 2.5 + (w / numberOfData * i),
										h - 70 - map(groupedConnections.getConnection(chosenStep, i).getDuration(), 0,
												maxima[1], 0, h - 110) + 5);
								rotate(3 * PI / 2);
								fill(color(0, 0, 0));

								textAlign(RIGHT, CENTER);
								text = Integer
										.toString((int) groupedConnections.getConnection(chosenStep, i).getDuration());
								text(text, 0, 0);
								popMatrix();

								pushMatrix();
								translate(barWidth * (float) 2.5 + barWidth + (w / numberOfData * i) + 2,
										h - 70 - map(
												groupedConnections.getConnection(chosenStep, i).getDistance()
														/ groupedConnections.getConnection(chosenStep, i).getDuration(),
												0, maxima[2], 0, h - 110) + 5);
								rotate(3 * PI / 2);
								fill(color(0, 0, 0));

								textAlign(RIGHT, CENTER);
								text = Float.toString(groupedConnections.getConnection(chosenStep, i).getAvg());
								text(text, 0, 0);
								popMatrix();

							}
							pushMatrix();
							translate(barWidth * (float) 2.5 + (w / numberOfData * i), h - 70 + 7);
							rotate(3 * PI / 2);
							fill(color(0, 0, 0));
							textSize(10);
							textLeading(9);
							textAlign(RIGHT, CENTER);
							String textOrig = groupedConnections.getConnection(chosenStep, i).getOrigin();
							String textDest = groupedConnections.getConnection(chosenStep, i).getDestination();
							textOrig = textOrig.substring(0, textOrig.indexOf(","));
							textOrig += "\n";
							textOrig += textDest.substring(0, textDest.indexOf(","));
							text(textOrig, 0, 0);
							popMatrix();
						}

					}

				}

			}

		}

		if (record == true) {

			endRecord();
			record = false;
		}

	}

	/**
	 * Heraussuchen und �bertragen aller beteiligter St�dte aus Verbindungsliste
	 * Umrechnen und eintragen der Lat/Lon Werte in X/Y Werte
	 */
	private void computeCities() {

		involvedCities = new Cities();

		// Alle Origins und Destinations durchgehen und noch nicht eingetragene
		// in St�dteliste schreiben
		for (int o = 0; o < groupedConnections.sizeOfGroups(); o++) {
			City c = new City(groupedConnections.getConnection(0, o).getOrigin(),
					groupedConnections.getConnection(0, o).getLatOrigin(),
					groupedConnections.getConnection(0, o).getLonOrigin());
			if (!involvedCities.contains(c)) {
				involvedCities.add(c);
			}
			c = new City(groupedConnections.getConnection(0, o).getDestination(),
					groupedConnections.getConnection(0, o).getLatDestination(),
					groupedConnections.getConnection(0, o).getLonDestination());
			if (!involvedCities.contains(c)) {
				involvedCities.add(c);
			}
		}

		// St�dteliste in Partikelliste �bertragen, Lat/Lon wird durch
		// berechnetes X/Y ersetzt
		MercatorMap mercatorMap = new MercatorMap(appWidth, appHeight, boundsTop, boundsBottom, boundsLeft,
				boundsRight);
		for (int i = 0; i < involvedCities.size(); i++) {
			PVector p = mercatorMap.getScreenLocation(
					new PVector((float) involvedCities.get(i).getLat(), (float) involvedCities.get(i).getLon()));
			cityParticleList.add(particleSystem.makeParticle((float) 1.0, (float) p.x, (float) p.y, (float) 0.0));
		}

		// Origin und Destination-IDs in Connection schreiben
		// N�tig, weil diese ID fl�chtig ist; wird innerhalb der Visualisierung
		// berechnet
		for (int i = 0; i < groupedConnections.size(); i++) {
			for (int o = 0; o < groupedConnections.sizeOfGroups(); o++) {
				groupedConnections.getConnection(i, o)
						.setOriginID(involvedCities.getCityID(groupedConnections.getConnection(i, o).getOrigin()));
				groupedConnections.getConnection(i, o).setDestinationID(
						involvedCities.getCityID(groupedConnections.getConnection(i, o).getDestination()));
			}
		}
		/*
		 * if(carTrain.equals("Train")){ for (int i = 0; i <
		 * referenceConnections.sizeOfGroups(); i++) {
		 * referenceConnections.get(i).setOriginID(involvedCities.getCityID(
		 * referenceConnections.get(i).getOrigin()));
		 * referenceConnections.get(i)
		 * .setDestinationID(involvedCities.getCityID(referenceConnections.get(i
		 * ).getDestination())); } }
		 */
		// Lat/Lon in X/Y Werte umrechnen und in Verbindungen eintragen
		for (int i = 0; i < groupedConnections.size(); i++) {
			for (int o = 0; o < groupedConnections.sizeOfGroups(); o++) {
				Connection temp = groupedConnections.getConnection(i, o);
				temp.setPxOrigin(cityParticleList.get(temp.getOriginID()).position().x(),
						cityParticleList.get(temp.getOriginID()).position().y());
				temp.setPxDestination(cityParticleList.get(temp.getDestinationID()).position().x(),
						cityParticleList.get(temp.getDestinationID()).position().y());
			}
		}
		if (carTrain.equals("Train")) {
			for (int i = 0; i < referenceConnections.sizeOfGroups(); i++) {
				Connection temp = referenceConnections.get(i);
				temp.setPxOrigin(cityParticleList.get(temp.getOriginID()).position().x(),
						cityParticleList.get(temp.getOriginID()).position().y());
				temp.setPxDestination(cityParticleList.get(temp.getDestinationID()).position().x(),
						cityParticleList.get(temp.getDestinationID()).position().y());
			}
		}
	}

	/**
	 * Erzeugt Springs zwischen St�dten f�r Verbindungen und schreibt die
	 * (entspannte) Bildschirml�nge in die Verbindung
	 */
	private void computeSprings() {

		// Springs erzeugen
		if (carTrain.equals("Train")) {
			for (int i = 0; i < referenceConnections.sizeOfGroups(); i++) {
				citySpringList.add(referenceConnections.get(i).computeSpring(particleSystem, cityParticleList));
			}
		}
		if (carTrain.equals("Car")) {
			for (int i = 0; i < groupedConnections.sizeOfGroups(); i++) {
				citySpringList.add(groupedConnections.get(i).computeSpring(particleSystem, cityParticleList));
			}
		}
		// Normale L�nge in Verbindung schreiben
		for (int i = 0; i < groupedConnections.size(); i++) {
			for (int o = 0; o < groupedConnections.sizeOfGroups(); o++) {
				groupedConnections.getConnection(i, o).setNormalLength(particleSystem, cityParticleList);
			}
		}

	}

	/**
	 * Erzeugt Partikel und Springs f�r Ankerpunkte der St�dte
	 */
	private void computeAnchors() {
		for (int i = 0; i < cityParticleList.size(); i++) {
			anchorParticleList.add(particleSystem.makeParticle((float) 1.0, cityParticleList.get(i).position().x(),
					cityParticleList.get(i).position().y(), (float) 0.0));
			anchorParticleList.get(i).makeFixed();
			anchorSpringList.add(particleSystem.makeSpring(anchorParticleList.get(i), cityParticleList.get(i),
					(float) 0.01, (float) 0.01, 0));
		}

	}

	public void setGroupedConnections(GroupedConnections groupedConnections) {
		this.groupedConnections = groupedConnections;
	}

	public void setAppWidth(int appWidth) {
		this.appWidth = appWidth;
	}

	public void setAppHeight(int appHeight) {
		this.appHeight = appHeight;
	}

	public void setReferenceConnections(GroupedConnections referenceConnectionsLeft) {
		this.referenceConnections = referenceConnectionsLeft;
	}

	public void setStep(int value) {
		this.chosenStep = value;
	}

	public void setShowAnchors(boolean showAnchor) {
		this.showAnchor = showAnchor;
	}

	public void setShowInfos(boolean showInfo) {
		this.showInfo = showInfo;
	}

	public void setFactor(String factor) {
		this.factor = factor;
	}

	public void setVisualisation(String visualisation) {
		this.visualisation = visualisation;

	}

	/**
	 * Berechnet die Verzerrung des Kartenmaterial bei Visu "Map Warped"
	 * arbeitsintensiv, sollte stets in eigenem Thread aufgerufen werden
	 */
	public void calculateWarp() {

		PImage source = new PImage(europeMapImage2.width, europeMapImage2.height);
		source.copy(europeMapImage2, 0, 0, europeMapImage2.width, europeMapImage2.height, 0, 0, europeMapImage2.width,
				europeMapImage2.height);
		int w = source.width;
		int h = source.height;
		tempDestination = new PImage(w, h);

		source.loadPixels();
		tempDestination.loadPixels();
		updatePixels();
		PVector vectorArray[] = new PVector[source.pixels.length];
		for (int i = 0; i < vectorArray.length; i++) {
			vectorArray[i] = new PVector(0, 0);
		}

		// f�r jede Stadt
		for (int i = 0; i < anchorSpringList.size(); i++) {
			Spring tempSpring = anchorSpringList.get(i);
			// Vektor vom Ankerpunkt zum neuen Punkt der Stadt
			PVector anchorVector = new PVector(
					tempSpring.getTheOtherEnd().position().x() - tempSpring.getOneEnd().position().x(),
					tempSpring.getTheOtherEnd().position().y() - tempSpring.getOneEnd().position().y());

			// f�r alle x
			for (int x = 0; x < w; x++) {
				// f�r alle y
				for (int y = 0; y < h; y++) {

					PVector tempVector = new PVector(anchorVector.x, anchorVector.y);
					// Abstand zwischen aktuellem Punkt und
					// Ankerpunkt
					float tempDistance = dist(tempSpring.getOneEnd().position().x(),
							tempSpring.getOneEnd().position().y(), x, y);

					// Je weiter Punkt von Ankerpunkt entfernt,
					// desto schw�cher die Verzerrung
					tempDistance = map(tempDistance, 0, appWidth / 4, 1, 0);
					// negative Werte ausschliessen
					if (tempDistance < 0) {
						tempDistance = 0;
					}

					tempVector.mult(tempDistance);

					vectorArray[y * w + x].add(tempVector);

				}
			}
		}

		// f�r alle x
		for (int x = 0; x < w; x++) {
			// f�r alle y
			for (int y = 0; y < h; y++) {

				int tempPixel = source.pixels[y * w + x];

				if (((y + (int) vectorArray[y * w + x].y) * w + (x + (int) vectorArray[y * w + x].x)) >= 0
						&& ((y + (int) vectorArray[y * w + x].y) * w
								+ (x + (int) vectorArray[y * w + x].x)) < tempDestination.pixels.length) {
					tempDestination.pixels[((y + (int) vectorArray[y * w + x].y) * w
							+ (x + (int) vectorArray[y * w + x].x))] = tempPixel; // entspricht
																					// get(x,y)
																					// aber
																					// schneller
																					// (y
																					// *
																					// width
																					// +
																					// x)
				}
			}
		}

		for (int i = 0; i < anchorSpringList.size(); i++) {

			for (int x = 1; x < w - 1; x++) {
				for (int y = 1; y < h - 1; y++) {
					if (tempDestination.pixels[y * w + x] == 0) {

						if (tempDestination.pixels[y * w + x + 1] == 0) {
							tempDestination.pixels[y * w + x] = tempDestination.pixels[(y + 1) * w + x];
						} else {
							tempDestination.pixels[y * w + x] = tempDestination.pixels[y * w + x + 1];
						}
					}
				}
			}

		}

		updatePixels();

		threadDone = true;

	}

	public void setBase(String string) {

		this.base = string;

	}

	public void setCarTrain(String string) {
		this.carTrain = string;

	}

	void dashline(float x0, float y0, float x1, float y1, float dash, float gap) {
		float[] spacing = { dash, gap };
		dashline(x0, y0, x1, y1, spacing);
	}

	public void dashline(float x0, float y0, float x1, float y1, float[] spacing) {
		float distance = dist(x0, y0, x1, y1);
		float[] xSpacing = new float[spacing.length];
		float[] ySpacing = new float[spacing.length];
		float drawn = (float) 0.0; 

		if (distance > 0) {
			int i;
			boolean drawLine = true; 

			for (i = 0; i < spacing.length; i++) {
				xSpacing[i] = lerp(0, (x1 - x0), spacing[i] / distance);
				ySpacing[i] = lerp(0, (y1 - y0), spacing[i] / distance);
			}

			i = 0;
			while (drawn < distance) {
				if (drawLine) {
					line(x0, y0, x0 + xSpacing[i], y0 + ySpacing[i]);
				}
				x0 += xSpacing[i];
				y0 += ySpacing[i];
	
				drawn = drawn + mag(xSpacing[i], ySpacing[i]);
				i = (i + 1) % spacing.length; 
				drawLine = !drawLine; 
			}
		}
	}

	public void takeScreenshot(String path) {

		this.path = path;
		this.record = true;
	}

	public void setNormal(boolean b) {

		this.normal = b;

	}

}
