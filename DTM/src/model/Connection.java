package model;

import java.util.ArrayList;
import java.util.Calendar;

import processing.core.PVector;
import traer.physics.*;

/**
 * Datenmodell f�r eine Verbindung zwischen zwei St�dten in der Visualisierung
 * 
 * @author rszabad
 *
 */

public class Connection {

	private String origin = null;
	private String destination = null;
	private double latOrigin;
	private double lonOrigin;
	private double latDestination;
	private double lonDestination;
	private int xOrigin;
	private int yOrigin;
	private int xDestination;
	private int yDestination;
	private Calendar departure = null;
	private float duration;
	private float distance;
	private float avg;
	private boolean isTrain = false;
	
	
	private int originID;
	private int DestinationID;
	
	private float normalLength;
	private Spring spring;
	
	/**
	 * Initialisierung mit allen typischen Parametern
	 * 
	 * @param origin
	 * @param destination
	 * @param latOrigin
	 * @param lonOrigin
	 * @param latDestination
	 * @param lonDestination
	 * @param departure
	 * @param duration
	 * @param distance
	 * @param avg
	 */
	public Connection(String origin, String destination, double latOrigin, double lonOrigin, double latDestination, double lonDestination,  Calendar departure, float duration, float distance, float avg){
		
		this.origin = origin;
		this.destination = destination;
		this.latOrigin = latOrigin;
		this.lonOrigin = lonOrigin;
		this.latDestination = latDestination;
		this.lonDestination = lonDestination;
		if(departure != null){
			isTrain = true;
			this.departure = departure;
		}
		this.duration = duration;
		this.distance = distance; 
		this.avg = avg;
	}
	
	@Override
	public String toString(){
		
		String tempString;
		
		return tempString = origin + " - " + destination + " latOr:" + latOrigin  + " lonOr:" + lonOrigin + " latDes:" + latDestination + " lonDes:" + lonDestination + " duration:" + duration + " distance:" + distance + " avg:" + avg;
		
	}
	
	
	public String getOrigin() {
		return origin;
	}


	public String getDestination() {
		return destination;
	}


	public Calendar getDeparture() {
		return departure;
	}



	public float getDuration() {
		return duration;
	}


	public float getDistance() {
		return distance;
	}


	public boolean isTrain() {
		return isTrain;
	}
	
	public double getLatOrigin() {
		return latOrigin;
	}

	public double getLonOrigin() {
		return lonOrigin;
	}

	public double getLatDestination() {
		return latDestination;
	}

	public double getLonDestination() {
		return lonDestination;
	}

	public Spring getSpring() {
		return spring;
	}

	public void setSpring(Spring spring) {
		this.spring = spring;
	}

	public float getNormalLength() {
		return normalLength;
	}

	/**
	 * gibt der Verbindung ihr Partikelsystem und die St�dteliste, woraus die Normall�nge der Verbindung in Pixel berechnet wird
	 * 
	 * @param particleSystem
	 * @param cityList
	 */
	public void setNormalLength(ParticleSystem particleSystem, ArrayList<Particle> cityList) {
		PVector A = new PVector(xDestination, yDestination, 0);
	    PVector B = new PVector(xOrigin, yOrigin, 0);
	    float dist = A.dist(B);
	    
	    this.normalLength = dist;
	}

	public int getDestinationID() {
		return DestinationID;
	}

	public void setDestinationID(int destinationID) {
		DestinationID = destinationID;
	}

	public int getOriginID() {
		return originID;
	}

	public void setOriginID(int originID) {
		this.originID = originID;
	}
	
	
	/**
	 * Erstellt ein Spring-Element f�r die Partikelsimulation, passend zu dieser Verbindung und gibt es zur�ck
	 * 
	 * @param particleSystem
	 * @param cityList
	 * @return
	 */
	public Spring computeSpring(ParticleSystem particleSystem, ArrayList<Particle> cityList){
		PVector A = new PVector(xDestination, yDestination, 0);
	    PVector B = new PVector(xOrigin, yOrigin, 0);
	    float dist = A.dist(B);
	    
	    this.normalLength = dist;
	    
	    spring = particleSystem.makeSpring(cityList.get(this.getOriginID()), cityList.get(this.getDestinationID()), (float)0.1, (float)0.1, this.normalLength);
	    spring.setDamping((float)0.02);
	    return spring;
	}

	public void setPxOrigin(float x, float y) {
		this.xOrigin = (int)x;
		this.yOrigin = (int)y;
		
	}

	public void setPxDestination(float x, float y) {
		this.xDestination = (int)x;
		this.yDestination = (int)y;
		
	}

	public int getxOrigin() {
		return (int) spring.getOneEnd().position().x();
	}

	public int getyOrigin() {
		return (int) spring.getOneEnd().position().y();
	}

	public int getxDestination() {
		return (int) spring.getTheOtherEnd().position().x();
	}

	public int getyDestination() {
		return (int) spring.getTheOtherEnd().position().y();
	}
	
	public float getRestLength(){
		return this.spring.restLength();
	}
		
	public void setRestLength(float restlength){
		this.spring.setRestLength(restlength);

	
	}
	
	public void display() {

		
	}

	public float getAvg() {
		return avg;
	}


		
}
