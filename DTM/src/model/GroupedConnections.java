package model;

import java.util.ArrayList;

/**
 * Beh�lter f�r Verbindungsdaten f�r die Visualisierung
 * 
 * Liste von Listen, bei mehrfachem Vorhandensein von untergeordneten Listen, geben diese einen zeitlichen Verlauf gleicher Verbindungen wieder
 * 
 * @author rszabad
 *
 */

public class GroupedConnections {

	//Liste von Datens�tzen in Listenform
	//Untergeordnete Liste enth�lt Verbindungsdatens�tze mit Informationen zu 1 bestimmten Zeitpunkt
	/*
	 	TopList{
	 			LowerList{
	 					Connection1
	 					Connction2
	 					Connction3
	 					}
	 			LowerList{
	 					Connction1
	 					Connction2
	 					Connction3
	 					}
	 	}
	 */
	private ArrayList<ArrayList<Connection>> groupedConnections = new ArrayList<ArrayList<Connection>>();
	
	public GroupedConnections(){

	}
	
	//�bergabe nur als kompletter Datensatz (LowerList) m�glich
	//Index in der Liste bestimmt zeitlichen Ablauf bei Visualisierung
	//�bergabereihenfolge wichtig
	public void addConnections(ArrayList<Connection> connections){
		this.groupedConnections.add(connections);
	}
	
	//Aufruf einzelner Verbindungen �ber Index des Datensatzes (LowerList) und Index der Verbindung im Datensatz (Conn)
	public Connection getConnection(int indexConnections, int indexConnection){
		return(groupedConnections.get(indexConnections).get(indexConnection));
	}
	
	public Connection get(int indexConnection){
		return(groupedConnections.get(0).get(indexConnection));
	}
	
	/**
	 * //Gibt die Anzahl der Datens�tze zur�ck (LowerList)
	 * 
	 * @return
	 */
	public int size(){
		return groupedConnections.size();
	}
	
	/**
	 * Gibt die Anzahl der Verbindungen in den Datens�tzen zur�ck (Connection)
	 * Gr��e sollte/m�sste in allen Datens�tzen gleich sein
	 * 
	 * @return
	 */
	public int sizeOfGroups(){
		if(groupedConnections != null){
			return groupedConnections.get(0).size();
		}
		return 0;
	}
	
	public float[] getMaxima(){
		
		float maxima[] = {0,0,0};
		
		for(int i = 0; i < this.size(); i++){
			for(int o = 0; o < this.sizeOfGroups(); o++){
				
				if(this.getConnection(i, o).getDistance() > maxima[0]){
					maxima[0] = this.getConnection(i, o).getDistance();
				}
				if(this.getConnection(i, o).getDuration() > maxima[1]){
					maxima[1] = this.getConnection(i, o).getDuration();
				}
				if(this.getConnection(i, o).getDuration() / this.getConnection(i, o).getDistance() > maxima[2]){
					maxima[2] = this.getConnection(i, o).getDuration() / this.getConnection(i, o).getDistance();
				}
				
			}

		}
		
		return maxima;
		
	}
	
	public float[] getMaxima(GroupedConnections reference){
		
		float maxima[] = {0,0,0};		
		for(int i = 0; i < this.size(); i++){
			for(int o = 0; o < this.sizeOfGroups(); o++){			
				if(this.getConnection(i, o).getDistance() > maxima[0]){
					maxima[0] = this.getConnection(i, o).getDistance();
				}
				if(this.getConnection(i, o).getDuration() > maxima[1]){
					maxima[1] = this.getConnection(i, o).getDuration();
				}
				if(this.getConnection(i, o).getDuration() / this.getConnection(i, o).getDistance() > maxima[2]){
					maxima[2] = this.getConnection(i, o).getDuration() / this.getConnection(i, o).getDistance();
				}				
			}		
		}
		
		float refMaxima[] = reference.getMaxima();
		
		for(int i = 0; i < maxima.length; i++){			
			if(maxima[i] < refMaxima[i]){
				maxima[i] = refMaxima[i];
			}
		}
		
		return maxima;
		
	}
	
	
}
