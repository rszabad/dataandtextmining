package model;

/**
 * Datenmodell f�r eine Stadt in der Visualisierung
 * 
 * @author rszabad
 *
 */

public class City {

	private String name = null;
	private double lat;
	private double lon;
	
	public City(String name, double lat, double lon){
		this.name = name;
		this.lat = lat;
		this.lon = lon;
	}
	
	public String getName() {
		return name;
	}

	public double getLat() {
		return lat;
	}

	public double getLon() {
		return lon;
	}

}
