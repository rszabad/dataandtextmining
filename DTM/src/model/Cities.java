package model;

import java.util.ArrayList;

/**
 * Hilfklasse zum halten einer veränderlichen Städteliste während der Visualisierung
 * 
 * @author rszabad
 *
 */

public class Cities{

	private ArrayList<City> cities = new ArrayList<City>();
	
	public void add(City city){
		this.cities.add(city);
	}

	public boolean contains(City other){
		for(int i = 0; i < this.cities.size(); i++){
			if(other.getName().equals(this.cities.get(i).getName())){
				return true;
			}
		}
		return false;
	}
	
	public int size(){
		return this.cities.size();
	}
	
	public City get(int i){
		return this.cities.get(i);
	}

	public int getCityID(String cityName) {
		for(int i = 0; i < this.cities.size(); i++){
			if(cityName.equals(cities.get(i).getName())){
				return i;
			}
		}
		return -1;
	}
}
