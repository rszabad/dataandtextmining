package database;import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;

/**Allgemeine Klasse City, in der alle Stadt-Eigenschaften definiert werden (keine Verbindungen)**/
public class City {

	private String name;
	private double latitude;
	private double longitude;

	private String[] names;

	private double[] latitudes;
	private double[] longitudes;

	private DbConnector dbcon;

	private Connection con;

	private Statement stt;

	private Transaction tr;

	/**Default-Konstruktor**/
	public City(){
		
	}
	
	/**City-Konstruktor für eine spezifische Stadt**/
	public City(String name) throws SQLException, InstantiationException, IllegalAccessException, ClassNotFoundException
	{
		dbcon = new DbConnector();

		con = dbcon.connect();

		stt = con.createStatement();

		tr = new CarTransaction();

		this.name = tr.getSpecificOrigin(stt, name);
		latitude= tr.getSpecificLat(stt, name);
		longitude = tr.getSpecificLon(stt, name);

		stt.close();
		con.close();
	}
	/**Konstruktor für alle Städte**/
	public City(boolean complete) throws InstantiationException, IllegalAccessException, ClassNotFoundException, SQLException
	{
		dbcon = new DbConnector();

		con = dbcon.connect();

		stt = con.createStatement();

		tr = new CarTransaction();
		


		if(complete == true) {

			latitudes = tr.getLatitudes(stt);
			longitudes = tr.getLongitudes(stt);
			names = tr.getOrigins(stt);
		}

		else {

			latitudes = tr.getLatitudesTrain(stt);
			longitudes = tr.getLongitudesTrain(stt);
			names = tr.get24Origins(stt);
			
		
			
		}
		
		
		
	}

	public String[] getNames() {
		return names;
	}
	public void setNames(String[] origins) {
		this.names = origins;
	}
	public double[] getLatitudes() {
		return latitudes;
	}
	public void setLatitudes(double[] latitudes) {
		this.latitudes = latitudes;
	}
	public double[] getLongitudes() {
		return longitudes;
	}
	public void setLongitudes(double[] longitudes) {
		this.longitudes = longitudes;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public double getLatitude() {
		return latitude;
	}

	//Convenience; lat f�r Stadt finden; rszabad
public double getLatitude(String place) {
		
		for(int i = 0; i < names.length; i++){

			if(place.equals(names[i])){
				return latitudes[i];
			}
			
		}
		return 0;
	}
	
	public void setLatitude(double latitude) {
		this.latitude = latitude;
	}

	public double getLongitude() {
		return longitude;
	}

	//Convenience; lon f�r Stadt finden; rszabad
public double getLongitude(String place) {
		
		for(int i = 0; i < names.length; i++){
			if(place.equals(names[i])){
				return longitudes[i];
			}
			
		}
		return 0;
		
	}
	
	public void setLongitude(double longitude) {
		this.longitude = longitude;
	}

}
