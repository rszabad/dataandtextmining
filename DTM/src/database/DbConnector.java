package database;

import java.sql.*;


/**Datenbank-Verbindung**/
public class DbConnector {



	public Connection connect() throws SQLException, InstantiationException, IllegalAccessException, ClassNotFoundException
	{

		String url = "jdbc:mysql://localhost:3306/";

		String user = "root";

		String password = "";


		Class.forName("com.mysql.jdbc.Driver").newInstance();
		Connection con = DriverManager.getConnection(url, user, password);

		return con;

	}
}
