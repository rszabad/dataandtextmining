package database;
import java.sql.*;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.regex.Pattern;


/**
 * 
 * @author rszabad
 *
 */

public class rewriter
{

	private static DbConnector dbcon;

	private static Connection con;

	private static Statement stt;

	private TrainTransaction tr;
	
	public static void main(String[] args) {
		
		doIt();
	
	}

	public static void doIt()
	{
		
		//Datum in DB aus Varchar parsen und als Timestamp ind DB sichern, um BETWEEN-Sql-Abfrage zu ermöglichen
		try {
		
			
			dbcon = new DbConnector();
			con = dbcon.connect();
			stt  = con.createStatement();
			stt.execute("USE requests_final");
			ResultSet rs = null;
			int temp = 0;
			
			rs = stt.executeQuery("SELECT departure_time FROM train_requests ORDER BY res_id");
			//rs = stt.executeQuery("SELECT COUNT(*) AS number FROM train_requests");

			String[] times = new String[size(rs)];
			System.out.println(size(rs));
			while(rs.next()){
				String tempString = rs.getString("departure_time");
				times[temp] = tempString.substring(4, 24);
				//System.out.println(temp + " " + times[temp]);
				temp++;
			}
			
			
			DateFormat formatter = new SimpleDateFormat("MMM dd yyyy HH:mm:ss");
			
			
			java.util.Date[] dates = new java.util.Date[times.length];
			java.sql.Timestamp[] dates2 = new java.sql.Timestamp[times.length];
			
			for(int i = 0; i < dates.length; i++){
				dates[i] = (java.util.Date)formatter.parse(times[i]);
				dates2[i] = new java.sql.Timestamp(dates[i].getTime());
			}
			
			for(int i = 0; i < dates.length; i++){
					
				stt.executeUpdate("UPDATE train_requests SET timestamp = \""+ dates2[i] + "\" WHERE res_id = " + (i+1) + "");

				System.out.println(dates.length + "/" + i);
			}
			
			
			
			
		} catch (InstantiationException | IllegalAccessException | ClassNotFoundException | SQLException | ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		
		//Fahrzzeit in DB aus Varchar parsen und als Minutenwert ind DB speichern
		/*try {
			
		dbcon = new DbConnector();
		con = dbcon.connect();
		stt  = con.createStatement();
		stt.execute("USE requests_final");
		ResultSet rs = null;
		int temp = 0;
			
		
		rs = stt.executeQuery("SELECT duration FROM train_requests ORDER BY res_id");
		//rs = stt.executeQuery("SELECT COUNT(*) AS number FROM train_requests");

		String[] duration = new String[size(rs)];
		System.out.println(size(rs));
		while(rs.next()){
			duration[temp] = rs.getString("duration");
			System.out.println(temp + " " + duration[temp]);
			temp++;

		}
		
		for(int i = 0; i < duration.length; i++){
			
			int durationMinutes = 0;
			
			duration[i] = duration[i].trim();

			String[] segs = duration[i].split(Pattern.quote(" "));

			if (segs.length == 2)
			{
				int min = Integer.parseInt(segs[0]);
				durationMinutes = min;
			}

			else 

			{
				int std = Integer.parseInt(segs[0]);
				int min = Integer.parseInt(segs[2]);
				durationMinutes = std * 60 + min;                                                                                                                                                            

			}
			
			stt.executeUpdate("UPDATE train_requests SET durationminutes = "+ durationMinutes + " WHERE res_id = " + (i+1) + "");
					
			System.out.println(i);
			
		}
		
		System.out.println("done");
		
		stt.close();

		con.close();
		
		} catch (InstantiationException | IllegalAccessException | ClassNotFoundException | SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		*/
		
	}
	
	public static int size(ResultSet res) throws SQLException {

		int size;
		res.last();
		size = res.getRow();
		res.beforeFirst();

		return size;
	}
	
}

