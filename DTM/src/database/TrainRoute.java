package database;import java.sql.*;


/**Hier werden alle Train-Verbindungsobjekte definiert**/
public class TrainRoute
{

	private int id;
	private String origin;
	private String destination;
	private double distance;
	private double duration;

	private double completePoolDayAvg;
	private double completePoolWeekAvg;
	private double completePoolAvg;

	private double specificDayAvg;
	private double specificWeekAvg;
	private double specificAvg;

	private int[] idArr;
	private String[] originArr;
	private String[] destinationArr;
	private int[] distanceArr;
	private int[] durationArr;

	private DbConnector dbcon;

	private Connection con;

	private Statement stt;

	private TrainTransaction tr;

	private double calculatedAverage = 0;
	private float completePoolDistance;
	private float completePoolDuration;
	
	

	/**Konstruktor für eine Tagesverbindung von A nach B **/
	public TrainRoute(String origin, String destination, String day) throws InstantiationException, IllegalAccessException, ClassNotFoundException, SQLException
	{

		dbcon = new DbConnector();

		con = dbcon.connect();

		stt  = con.createStatement();

		tr = new TrainTransaction();

		this.origin = tr.getSpecificOrigin(stt, origin);
		this.destination = tr.getSpecificRouteDest(stt, origin, destination);
		duration = tr.getSpecificRouteDayDuration(stt, origin, destination, day);
		distance = tr.getSpecificRouteDayDistance(stt, origin, destination, day);

		setCalculatedAverage(distance / duration);
		
		stt.close();

		con.close();
	}

	/**Konstuktor für eine Wochenverbindung von A nach B **/
	public TrainRoute(String origin, String destination, int week) throws InstantiationException, IllegalAccessException, ClassNotFoundException, SQLException

	{

		dbcon = new DbConnector();

		con = dbcon.connect();

		stt  = con.createStatement();

		tr = new TrainTransaction();

		this.origin = tr.getSpecificOrigin(stt, origin);
		this.destination = tr.getSpecificRouteDest(stt, origin, destination);

		//umgestellt auf einzelne Abfrage wegen Ladezeit; rszabad
		
		/*String[] week1 = {"04", "05", "06", "07", "08", "09", "10"};  

		String[] week2 = {"11", "12", "13", "14", "15", "16", "17"};

		String[] week3 = {"18", "19", "20", "21", "22", "23", "24"};

		String[] week4 = {"25", "26", "27", "28", "29", "30", "31"};

		double dur = 0.0;
		double dis = 0.0;

		if(week == 1)
		{
			for(int i = 0; i<week1.length; i++) 

			{

				dur = dur + tr.getSpecificRouteDayDuration(stt, origin, destination, week1[i]);
				dis = dis + tr.getSpecificRouteDayDistance(stt, origin, destination, week1[i]);

			}
		}

		if(week == 2)
		{
			for(int i = 0; i<week2.length; i++) 

			{

				dur = dur + tr.getSpecificRouteDayDuration(stt, origin, destination, week2[i]);
				dis = dis + tr.getSpecificRouteDayDistance(stt, origin, destination, week2[i]);

			}
		}

		if(week == 3)
		{
			for(int i = 0; i<week3.length; i++) 

			{

				dur = dur + tr.getSpecificRouteDayDuration(stt, origin, destination, week3[i]);
				dis = dis + tr.getSpecificRouteDayDistance(stt, origin, destination, week3[i]);

			}
		}

		if(week == 4)
		{
			for(int i = 0; i<week4.length; i++) 

			{

				dur = dur + tr.getSpecificRouteDayDuration(stt, origin, destination, week4[i]);
				dis = dis + tr.getSpecificRouteDayDistance(stt, origin, destination, week4[i]);

			}
		}

		dur = dur / 7;
		dis = dis / 7;


		duration = dur;
		distance = dis;*/


		//umgestellt auf einzelne Abfrage wegen Ladezeit; rszabad
		duration = tr.getSpecificRouteWeekDuration(stt, origin, destination, week);
		distance = tr.getSpecificRouteWeekDistance(stt, origin, destination, week);

		setCalculatedAverage(distance / duration);
		
		stt.close();

		con.close();
	}

	/**Konstruktor für eine Verbindung von A nach B gemittelt über den Gesamtzeitraum**/
	public TrainRoute(String origin, String destination) throws InstantiationException, IllegalAccessException, ClassNotFoundException, SQLException
	{

		dbcon = new DbConnector();

		con = dbcon.connect();

		stt  = con.createStatement();

		tr = new TrainTransaction();

		this.origin = tr.getSpecificOrigin(stt, origin);
		this.destination = tr.getSpecificRouteDest(stt, origin, destination);

		//umgestellt auf einzelne Abfrage wegen Ladezeit; rszabad
		
		//String[] temp = {"01", "02", "03", "04", "05", "06", "07", "08", "09", "10", "11", "12", "13", 
		//		"14", "15", "16", "17", "18", "19", "20", "21", "22", "23", "24", "25",
		//		"26", "27", "28", "29", "30", "31"}; 

		//double dur = 0.0;
		//double dis = 0.0;
		//double dur2 = 0.0;
		//double dis2 = 0.0;
		
		//for(int i = 0; i<temp.length; i++)

		//{

		//	dur2 = dur + tr.getSpecificRouteDayDuration(stt, origin, destination, temp[i]);
		//	dis2 = dis + tr.getSpecificRouteDayDistance(stt, origin, destination, temp[i]);

		//}

		//dur2 = dur2 / 31;
		//dis2 = dis2 / 31;

		//duration = dur;
		//distance = dis;

		
		//umgestellt auf einzelne Abfrage wegen Ladezeit; rszabad
		distance = tr.getSpecificRouteWholeDistance(stt, origin, destination);
		duration = tr.getSpecificRouteWholeDuration(stt, origin, destination);
			
		setCalculatedAverage(distance / duration);
	
		stt.close();

		con.close();
	}

	//Default-Konstruktor
	public TrainRoute()

	{

	}

	public double specificRouteWeekAvg(String origin, String destination, int week) throws SQLException, InstantiationException, IllegalAccessException, ClassNotFoundException

	{
		dbcon = new DbConnector();

		con = dbcon.connect();

		stt = con.createStatement();

		tr = new TrainTransaction();

		String[] week1 = {"04", "05", "06", "07", "08", "09", "10"};  

		String[] week2 = {"11", "12", "13", "14", "15", "16", "17"};

		String[] week3 = {"18", "19", "20", "21", "22", "23", "24"};

		String[] week4 = {"25", "26", "27", "28", "29", "30", "31"};

		double dur = 0.0;
		double dis = 0.0;

		if(week == 1)
		{
			for(int i = 0; i<week1.length; i++) 

			{

				dur = dur + tr.getSpecificRouteDayDuration(stt, origin, destination, week1[i]);
				dis = dis + tr.getSpecificRouteDayDistance(stt, origin, destination, week1[i]);

			}
		}

		if(week == 2)
		{
			for(int i = 0; i<week2.length; i++) 

			{

				dur = dur + tr.getSpecificRouteDayDuration(stt, origin, destination, week2[i]);
				dis = dis + tr.getSpecificRouteDayDistance(stt, origin, destination, week2[i]);

			}
		}

		if(week == 3)
		{
			for(int i = 0; i<week3.length; i++) 

			{

				dur = dur + tr.getSpecificRouteDayDuration(stt, origin, destination, week3[i]);
				dis = dis + tr.getSpecificRouteDayDistance(stt, origin, destination, week3[i]);

			}
		}

		if(week == 4)
		{
			for(int i = 0; i<week4.length; i++) 

			{

				dur = dur + tr.getSpecificRouteDayDuration(stt, origin, destination, week4[i]);
				dis = dis + tr.getSpecificRouteDayDistance(stt, origin, destination, week4[i]);

			}
		}

		dur = dur / 7;
		dis = dis / 7;

		specificWeekAvg = dis / dur;
		
		stt.close();
		
		con.close();

		return specificWeekAvg;
	}

	public double specificRouteDayAvg(String origin, String destination, String day) throws SQLException, InstantiationException, IllegalAccessException, ClassNotFoundException

	{
		dbcon = new DbConnector();

		con = dbcon.connect();

		stt = con.createStatement();

		tr = new TrainTransaction();

		double dur = tr.getSpecificRouteDayDuration(stt, origin, destination, day);
		double dis = tr.getSpecificRouteDayDistance(stt, origin, destination, day);

		specificDayAvg = dis / dur;
		
		stt.close();
		
		con.close();

		return specificDayAvg;
	}

	//Methode für die Berechnung des Durchschnitts einer Strecke von A nach B in Minuten/KM
	public double specificRouteAvg(String origin, String destination) throws SQLException, InstantiationException, IllegalAccessException, ClassNotFoundException

	{
		dbcon = new DbConnector();

		con = dbcon.connect();

		stt = con.createStatement();

		tr = new TrainTransaction();

		String[] temp = {"01", "02", "03", "04", "05", "06", "07", "08", "09", "10", "11", "12", "13", 
				"14", "15", "16", "17", "18", "19", "20", "21", "22", "23", "24", "25",
				"26", "27", "28", "29", "30", "31"}; 

		double dur = 0.0;
		double dis = 0.0;

		for(int i = 0; i<temp.length; i++)

		{

			dur = dur + tr.getSpecificRouteDayDuration(stt, origin, destination, temp[i]);
			dis = dis + tr.getSpecificRouteDayDistance(stt, origin, destination, temp[i]);

		}

		dur = dur / 31;
		dis = dis / 31;

		specificAvg = dis / dur;
		
		stt.close();
		
		con.close();

		return specificAvg;
	}

	public double completePoolWeekAvg(int week) throws InstantiationException, IllegalAccessException, ClassNotFoundException, SQLException

	{
		dbcon = new DbConnector();

		con = dbcon.connect();

		stt = con.createStatement();

		tr = new TrainTransaction();

		float maxDuration = 0;
		float maxDistance = 0;

		
		
		int[] dur = tr.getAllWeekDurations(stt, week);
		int[] dis = tr.getAllWeekDistances(stt, week);

		for(int i = 0; i < dur.length; i++)
		{
			maxDuration = (float)(maxDuration + dur[i]);
			maxDistance = (float)(maxDistance + dis[i]);
		}

		completePoolWeekAvg = maxDistance / maxDuration;

		stt.close();

		con.close();

		return completePoolWeekAvg;
	}

	public double completePoolDayAvg(String day) throws InstantiationException, IllegalAccessException, ClassNotFoundException, SQLException

	{
		dbcon = new DbConnector();

		con = dbcon.connect();

		stt = con.createStatement();

		tr = new TrainTransaction();

		float maxDuration = 0;
		float maxDistance = 0;

		int[] dur = tr.getAllDayDurations(stt, day);
		int[] dis = tr.getAllDayDistances(stt, day);

		for(int i = 0; i < dur.length; i++)
		{
			maxDuration = (float)(maxDuration + dur[i]);
			maxDistance = (float)(maxDistance + dis[i]);
		}

		completePoolDayAvg = maxDistance / maxDuration;

		stt.close();

		con.close();

		return completePoolDayAvg;
	}


	//Methode für die Berechnung des gesamten Car Pool-Durchschnitts in KM/Min
	public double completePoolAvg() throws InstantiationException, IllegalAccessException, ClassNotFoundException, SQLException

	{
		dbcon = new DbConnector();

		con = dbcon.connect();

		stt = con.createStatement();

		tr = new TrainTransaction();

		float maxDuration = 0;
		float maxDistance = 0;

		int[] dur = tr.getAllDurations(stt);
		int[] dis = tr.getAllDistances(stt);

		for(int i = 0; i < dur.length; i++)
		{
			maxDuration = (float)(maxDuration + dur[i]);
			maxDistance = (float)(maxDistance + dis[i]);
		}

		completePoolAvg = maxDistance / maxDuration;
		setCompletePoolDistance(maxDistance / dis.length);
		setCompletePoolDuration(maxDuration / dur.length);
		
		
		stt.close();

		con.close();

		return completePoolAvg;
	}

	public int getId() {
		return id;
	}

	public String getOrigin() {
		return origin;
	}

	public String getDestination() {
		return destination;
	}

	public double getDistance() {
		return distance;
	}

	public double getDuration() {
		return duration;
	}

	public String[] getDestinationArr() {
		return destinationArr;
	}

	public int[] getDistanceArr() {
		return distanceArr;
	}

	public int[] getDurationArr() {
		return durationArr;
	}

	public int[] getIdArr() {
		return idArr;
	}

	public double getCompletePoolAvg() {
		return completePoolAvg;
	}

	public String[] getOriginArr() {
		return originArr;
	}

	public double getCompletePoolDayAvg() {
		return completePoolDayAvg;
	}

	public double getCompletePoolWeekAvg() {
		return completePoolWeekAvg;
	}

	public double getSpecificDayAvg() {
		return specificDayAvg;
	}

	public double getSpecificWeekAvg() {
		return specificWeekAvg;
	}
	public double getSpecificAvg() {
		return specificAvg;
	}

	public double getCalculatedAverage() {
		return calculatedAverage;
	}

	public void setCalculatedAverage(double calculatedAverage) {
		this.calculatedAverage = calculatedAverage;
	}

	public float getCompletePoolDistance() {
		return completePoolDistance;
	}

	public void setCompletePoolDistance(float completePoolDistance) {
		this.completePoolDistance = completePoolDistance;
	}

	public float getCompletePoolDuration() {
		return completePoolDuration;
	}

	public void setCompletePoolDuration(float completePoolDuration) {
		this.completePoolDuration = completePoolDuration;
	}



}
