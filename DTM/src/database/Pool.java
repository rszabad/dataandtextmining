package database;
import java.sql.Connection;

import java.sql.SQLException;
import java.sql.Statement;


/**Hier werden alle spezifischen Pool-AVGs ermittelt**/
public class Pool {

	private int[] distanceArr;
	private int[] durationArr;

	private double carPoolAvg;

	private double trainPoolDayAvg;
	private double trainPoolWeekAvg;
	private double trainPoolAvg;

	private DbConnector dbcon;

	private Connection con;

	private Statement stt;

	private CarTransaction trC;

	private TrainTransaction trT;

	public Pool() 
	{	

	}

	/**KM/min Berechnung für alle Verbindungen im speziell gewählten Pools**/
	public void setCarPoolAvg(String[] citys) throws InstantiationException, IllegalAccessException, ClassNotFoundException, SQLException

	{

		dbcon = new DbConnector();

		con = dbcon.connect();

		stt = con.createStatement();

		trC = new CarTransaction();

		distanceArr = trC.getPoolDistances(stt, citys);

		durationArr = trC.getPoolDurations(stt, citys);

		float maxDuration = 0;
		float maxDistance = 0;

		for(int i = 0; i < distanceArr.length; i++)

		{
			maxDuration = maxDuration + durationArr[i];
			maxDistance = maxDistance + distanceArr[i];
		}

		carPoolAvg = maxDistance / maxDuration;

		stt.close();

		con.close();
	}

	/**KM/min Berechnung für alle an einem speziellen Tag gewählten Verbindungen im spezifischen Pool**/
	public void setTrainPoolDayAvg(String[] citys, String day) throws InstantiationException, IllegalAccessException, ClassNotFoundException, SQLException

	{
		dbcon = new DbConnector();

		con = dbcon.connect();

		stt = con.createStatement();

		trT = new TrainTransaction();

		distanceArr = trT.getPoolDayDistances(stt, citys, day);

		durationArr = trT.getPoolDayDurations(stt, citys, day);

		float maxDuration = 0;
		float maxDistance = 0;

		for(int i = 0; i < distanceArr.length; i++)

		{
			maxDuration = maxDuration + durationArr[i];
			maxDistance = maxDistance + distanceArr[i];
		}

		trainPoolDayAvg = maxDistance / maxDuration;

		stt.close();

		con.close();

	}

	/**KM/min Berechnung für alle in einer bestimmten Woche gewählten Verbindungen im spezifischen Pool**/
	public void setTrainPoolWeekAvg(String[] citys, int week) throws InstantiationException, IllegalAccessException, ClassNotFoundException, SQLException

	{
		dbcon = new DbConnector();

		con = dbcon.connect();

		stt = con.createStatement();

		trT = new TrainTransaction();

		String[] week1 = {"04", "05", "06", "07", "08", "09", "10"};

		String[] week2 = {"11", "12", "13", "14", "15", "16", "17"};

		String[] week3 = {"18", "19", "20", "21", "22", "23", "24"};

		String[] week4 = {"25", "26", "27", "28", "29", "30", "31"};

		double dur = 0.0;
		double dis = 0.0;

		int count = 0;

		if(week == 1)
		{
			for(int i = 0; i<week1.length; i++)

			{
				for(int j = 0; j < trT.getPoolDayDistances(stt, citys, week1[i]).length; j++) 

				{

					dur = dur + trT.getPoolDayDurations(stt, citys, week1[i])[j] / 7;

					dis = dis + trT.getPoolDayDistances(stt, citys, week1[i])[j] / 7;

					count++;

				}
			}
		}

		if(week == 2)
		{
			for(int i = 0; i<week2.length; i++)

			{
				for(int j = 0; j < trT.getPoolDayDistances(stt, citys, week2[i]).length; j++) 

				{

					dur = dur + trT.getPoolDayDurations(stt, citys, week2[i])[j] / 7;

					dis = dis + trT.getPoolDayDistances(stt, citys, week2[i])[j] / 7;

					count++;

				}
			}
		}

		if(week == 3)
		{
			for(int i = 0; i<week3.length; i++)

			{
				for(int j = 0; j < trT.getPoolDayDistances(stt, citys, week3[i]).length; j++) 

				{

					dur = dur + trT.getPoolDayDurations(stt, citys, week3[i])[j] / 7;

					dis = dis + trT.getPoolDayDistances(stt, citys, week3[i])[j] / 7;

					count++;

				}
			}
		}

		if(week == 4)
		{
			for(int i = 0; i<week4.length; i++)

			{
				for(int j = 0; j < trT.getPoolDayDistances(stt, citys, week4[i]).length; j++) 
				{
					dur = dur + trT.getPoolDayDurations(stt, citys, week4[i])[j] / 7;

					dis = dis + trT.getPoolDayDistances(stt, citys, week4[i])[j] / 7;

					count++;
				}
			}
		}

		dur = dur / count + 1;
		dis = dis / count + 1;

		trainPoolWeekAvg = dis / dur;

		stt.close();

		con.close();

	}

	/**KM/Min Berechnung für alle im spezifischen Pool vorhandenen Verbindungen**/
	public void setTrainPoolAvg(String[] citys) throws InstantiationException, IllegalAccessException, ClassNotFoundException, SQLException

	{

		dbcon = new DbConnector();

		con = dbcon.connect();

		stt = con.createStatement();

		trT = new TrainTransaction();

		double dur = 0.0;
		double dis = 0.0;


		int count = 0;

		String[] temp = {"01", "02", "03", "04", "05", "06", "07", "08", "09", "10", "11", "12", "13", 
				"14", "15", "16", "17", "18", "19", "20", "21", "22", "23", "24", "25",
				"26", "27", "28", "29", "30", "31"}; 


		for(int i = 0; i<temp.length; i++)

		{
			for(int j = 0; j < trT.getPoolDayDistances(stt, citys, temp[i]).length; j++) 

			{

				dur = dur + trT.getPoolDayDurations(stt, citys, temp[i])[j] / 31;

				dis = dis + trT.getPoolDayDistances(stt, citys, temp[i])[j] / 31;

				count++;

			}
		}

		dur = dur / count + 1;
		dis = dis / count + 1;

		trainPoolAvg = dis / dur;

		stt.close();

		con.close();

	}

	/**Getter für die AVGs**/
	public double getCarPoolAvg() {
		return carPoolAvg;
	}

	public double getTrainPoolDayAvg() {
		return trainPoolDayAvg;
	}

	public double getTrainPoolWeekAvg() {
		return trainPoolWeekAvg;
	}

	public double getTrainPoolAvg() {
		return trainPoolAvg;
	}

}
