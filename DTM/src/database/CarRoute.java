package database;import java.sql.*;



/**Hier werden alle Car-Verbindungsobjekte definiert **/
public class CarRoute

{

	private String origin;
	private String destination;
	private double distance;
	private double duration;

	private double completePoolAvg;
	private double specificAvg;

	private String[] originArr;
	private String[] destinationArr;
	private int[] distanceArr;
	private int[] durationArr;

	private DbConnector dbcon;

	private Connection con;

	private Statement stt;

	private CarTransaction tr;
	private double completePoolDistance;
	private double completePoolDuration;



	/**Verbindung von A nach B**/
	
	public CarRoute(String origin, String destination) throws InstantiationException, IllegalAccessException, ClassNotFoundException, SQLException
	{

		DbConnector dbcon = new DbConnector();

		Connection con = dbcon.connect();

		Statement stt  = con.createStatement();

		CarTransaction tr = new CarTransaction();

		this.origin = tr.getSpecificOrigin(stt, origin);
		this.destination = tr.getSpecificRouteDest(stt, origin, destination);
		duration = tr.getSpecificRouteDuration(stt, origin, destination);
		distance = tr.getSpecificRouteDistance(stt, origin, destination);

		stt.close();

		con.close();
	}

	/**Default-Konstruktor**/
	
	public CarRoute()
	
	{
		
	}

	/**Methode für die Berechnung des Durchschnitts einer Strecke von A nach B in Minuten/KM**/
	public double specificRouteAvg(String ori, String dest ) throws SQLException, InstantiationException, IllegalAccessException, ClassNotFoundException

	{
		dbcon = new DbConnector();

		con = dbcon.connect();

		stt = con.createStatement();

		tr = new CarTransaction();

		double dur = tr.getSpecificRouteDuration(stt, ori, dest);
		double dis = tr.getSpecificRouteDistance(stt, ori, dest);

		specificAvg = dis / dur;

		stt.close();

		con.close();

		return specificAvg;

	}


	/**Methode für die Berechnung des gesamten Car Pool-Durchschnitts in KM/Min**/
	public double completePoolAvg(boolean complete) throws InstantiationException, IllegalAccessException, ClassNotFoundException, SQLException

	{
		dbcon = new DbConnector();

		con = dbcon.connect();

		stt = con.createStatement();

		tr = new CarTransaction();

		double maxDuration = 0;
		double maxDistance = 0;

		int[] dur = null;
		int[] dis = null;

		if(complete)
		{
			dur = tr.getAllDurations(stt);
			dis = tr.getAllDistances(stt);
		}

		else 
		{
			dur = tr.get24Durations(stt);
			dis = tr.get24Distances(stt);
		}

		for(int i = 0; i < dur.length; i++)
		{
			maxDuration = (double)(maxDuration + dur[i]);
			maxDistance = (double)(maxDistance + dis[i]);
		}
		
		maxDuration = maxDuration / dur.length;
		maxDistance = maxDistance / dis.length;

		completePoolAvg = maxDistance / maxDuration;
		setCompletePoolDistance(maxDistance);
		setCompletePoolDuration(maxDuration);
		
		stt.close();

		con.close();

		return completePoolAvg;
	}


	public String getOrigin() {
		return origin;
	}

	public void setOrigin(String origin) {
		this.origin = origin;
	}

	public String getDestination() {
		return destination;
	}

	public void setDestination(String destination) {
		this.destination = destination;
	}

	public double getDistance() {
		return distance;
	}

	public void setDistance(double distance) {
		this.distance = distance;
	}

	public double getDuration() {
		return duration;
	}

	public void setDuration(int duration) {
		this.duration = duration;
	}


	public String[] getDestinationArr() {
		return destinationArr;
	}

	public void setDestinationArr(String[] destinationArr) {
		this.destinationArr = destinationArr;
	}

	public int[] getDistanceArr() {
		return distanceArr;
	}

	public void setDistanceArr(int[] distanceArr) {
		this.distanceArr = distanceArr;
	}

	public int[] getDurationArr() {
		return durationArr;
	}

	public void setDurationArr(int[] durationArr) {
		this.durationArr = durationArr;
	}

	public double getCompletePoolAvg() {
		return completePoolAvg;
	}

	public void setCompletePoolAvg(double poolAvg) {
		this.completePoolAvg = poolAvg;
	}

	public String[] getOriginArr() {
		return originArr;
	}

	public void setOriginArr(String[] originArr) {
		this.originArr = originArr;
	}

	public double getSpecificAvg() {
		return specificAvg;
	}
	public void setSpecificAvg(double specificAvg) {
		this.specificAvg = specificAvg;
	}

	public double getCompletePoolDistance() {
		return completePoolDistance;
	}

	public void setCompletePoolDistance(double completePoolDistance) {
		this.completePoolDistance = completePoolDistance;
	}

	public double getCompletePoolDuration() {
		return completePoolDuration;
	}

	public void setCompletePoolDuration(double completePoolDuration) {
		this.completePoolDuration = completePoolDuration;
	}

}
