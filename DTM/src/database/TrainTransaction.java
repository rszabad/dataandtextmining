package database;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.regex.Pattern;

/**Hier werden alle READ-Transactions der Train-Daten getätigt **/
public class TrainTransaction extends Transaction

{
	
	/**Hier werden alle Wochendurations einer speziell gewählten Woche selektiert und zurückgegeben**/
	public int[] getAllWeekDurations(Statement stt, int week) throws SQLException

	{

		stt.execute("USE requests_final");

		ResultSet rs = null;

		if(week == 1)
			rs = stt.executeQuery("SELECT duration FROM train_requests WHERE departure_time " +
					"LIKE '%Jan 04%' OR departure_time LIKE '%Jan 05%' OR departure_time LIKE '%Jan 06%' OR departure_time LIKE '%Jan 07%' " +
					"OR departure_time LIKE '%Jan 08%' OR departure_time LIKE '%Jan 09%' OR departure_time LIKE '%Jan 10%' ");


		if(week == 2)
			rs = stt.executeQuery("SELECT duration FROM train_requests WHERE departure_time " +
					"LIKE '%Jan 11%' OR departure_time LIKE '%Jan 12%' OR departure_time LIKE '%Jan 13%' OR departure_time LIKE '%Jan 14%' " +
					"OR departure_time LIKE '%Jan 15%' OR departure_time LIKE '%Jan 16%' OR departure_time LIKE '%Jan 17%' ");

		if(week == 3)
			rs = stt.executeQuery("SELECT duration FROM train_requests WHERE departure_time " +
					"LIKE '%Jan 18%' OR departure_time LIKE '%Jan 19%' OR departure_time LIKE '%Jan 20%' OR departure_time LIKE '%Jan 21%' " +
					"OR departure_time LIKE '%Jan 22%' OR departure_time LIKE '%Jan 23%' OR departure_time LIKE '%Jan 24%' ");

		if(week == 4)
			rs = stt.executeQuery("SELECT duration FROM train_requests WHERE departure_time " +
					"LIKE '%Jan 25%' OR departure_time LIKE '%Jan 26%' OR departure_time LIKE '%Jan 27%' OR departure_time LIKE '%Jan 28%' " +
					"OR departure_time LIKE '%Jan 29%' OR departure_time LIKE '%Jan 30%' OR departure_time LIKE '%Jan 31%' ");

		int temp = 0;

		int[] duration = new int[size(rs)];

		int std = 0;
		int min = 0;
		int dur = 0;

		String[] tempArr = new String[size(rs)];

		String[] segs;

		while(rs.next())
		
		{

			tempArr[temp] = rs.getString("duration").trim();

			segs = tempArr[temp].split(Pattern.quote(" "));

			if (segs.length == 2)
			{
				min = Integer.parseInt(segs[0]);
				dur = min;
				duration[temp] = dur;    

			}
			else 

			{
				std = Integer.parseInt(segs[0]);
				min = Integer.parseInt(segs[2]);
				dur = std * 60 + min;
				duration[temp] = dur;                                                                                                                                                               
			}
			temp++;
		}
		rs.close();
		return duration;
	}


	/** Hier werden alle Tagesdurations des gesamt Pools eines bestimmten Tages selektiert und zurückgegeben**/
	public int[] getAllDayDurations(Statement stt, String day) throws SQLException

	{

		stt.execute("USE requests_final");

		ResultSet rs = stt.executeQuery("SELECT duration FROM train_requests WHERE departure_time " +
				"LIKE '%Jan " + day + "%'");

		int temp = 0;

		int[] duration = new int[size(rs)];

		int std = 0;
		int min = 0;
		int dur = 0;

		String[] tempArr = new String[size(rs)];

		String[] segs;

		while(rs.next()){

			tempArr[temp] = rs.getString("duration").trim();

			segs = tempArr[temp].split(Pattern.quote(" "));

			if (segs.length == 2)
			{
				min = Integer.parseInt(segs[0]);
				dur = min;
				duration[temp] = dur;    

			}
			else 

			{
				std = Integer.parseInt(segs[0]);
				min = Integer.parseInt(segs[2]);
				dur = std * 60 + min;
				duration[temp] = dur;                                                                                                                                                               


			}
			temp++;
		}
		rs.close();
		return duration;
	}

	/** alle Duration-Einträge in der Datenbank für die Berechnung des Kompletten Pool-Avgs**/
	public int[] getAllDurations(Statement stt) throws SQLException
	{

		stt.execute("USE requests_final");

		ResultSet rs = stt.executeQuery("SELECT duration FROM train_requests");

		int temp = 0;

		int[] duration = new int[size(rs)];

		int std = 0;
		int min = 0;
		int dur = 0;

		String[] tempArr = new String[size(rs)];

		String[] segs;

		while(rs.next()){

			tempArr[temp] = rs.getString("duration").trim();

			segs = tempArr[temp].split(Pattern.quote(" "));

			if (segs.length == 2)
			{
				min = Integer.parseInt(segs[0]);
				dur = min;
				duration[temp] = dur;    

			}
			else 

			{
				std = Integer.parseInt(segs[0]);
				min = Integer.parseInt(segs[2]);
				dur = std * 60 + min;
				duration[temp] = dur;                                                                                                                                                               


			}
			temp++;
		}
		rs.close();
		return duration;
	}

	/** Selektierung und Rückgabe aller gewählten Wochen-Distances des Gesamtpools**/
	public int[] getAllWeekDistances(Statement stt, int week) throws SQLException

	{
		stt.execute("USE requests_final");

		ResultSet rs = null;

		if(week == 1)
			rs = stt.executeQuery("SELECT distance FROM train_requests WHERE departure_time " +
					"LIKE '%Jan 04%' OR departure_time LIKE '%Jan 05%' OR departure_time LIKE '%Jan 06%' OR departure_time LIKE '%Jan 07%' " +
					"OR departure_time LIKE '%Jan 08%' OR departure_time LIKE '%Jan 09%' OR departure_time LIKE '%Jan 10%' ");


		if(week == 2)
			rs = stt.executeQuery("SELECT distance FROM train_requests WHERE departure_time " +
					"LIKE '%Jan 11%' OR departure_time LIKE '%Jan 12%' OR departure_time LIKE '%Jan 13%' OR departure_time LIKE '%Jan 14%' " +
					"OR departure_time LIKE '%Jan 15%' OR departure_time LIKE '%Jan 16%' OR departure_time LIKE '%Jan 17%' ");


		if(week == 3)
			rs = stt.executeQuery("SELECT distance FROM train_requests WHERE departure_time " +
					"LIKE '%Jan 18%' OR departure_time LIKE '%Jan 19%' OR departure_time LIKE '%Jan 20%' OR departure_time LIKE '%Jan 21%' " +
					"OR departure_time LIKE '%Jan 22%' OR departure_time LIKE '%Jan 23%' OR departure_time LIKE '%Jan 24%' ");

		if(week == 4)
			rs = stt.executeQuery("SELECT distance FROM train_requests WHERE departure_time " +
					"LIKE '%Jan 25%' OR departure_time LIKE '%Jan 26%' OR departure_time LIKE '%Jan 27%' OR departure_time LIKE '%Jan 28%' " +
					"OR departure_time LIKE '%Jan 29%' OR departure_time LIKE '%Jan 30%' OR departure_time LIKE '%Jan 31%' ");


		int temp = 0;

		int[] distance = new int[size(rs)];

		while(rs.next()){

			distance[temp] = rs.getInt("distance") / 1000;

			temp++;
		}

		rs.close();

		return distance;

	}

	/**Selektierung und Rückgabe aller Distances eines bestimmten Tages im Gesamtpool**/
	public int[] getAllDayDistances(Statement stt, String day) throws SQLException

	{
		stt.execute("USE requests_final");

		ResultSet rs = stt.executeQuery("SELECT distance FROM train_requests WHERE departure_time " +
				"LIKE '%Jan " + day + "%'");

		int temp = 0;

		int[] distance = new int[size(rs)];

		while(rs.next()){

			distance[temp] = rs.getInt("distance") / 1000;

			temp++;
		}

		rs.close();

		return distance;

	}


	/**alle Distance-Einträge in der Datenbank, wird für die Gesamtpool-Berechnung benötigt**/
	public int[] getAllDistances(Statement stt) throws SQLException
	{

		stt.execute("USE requests_final");

		ResultSet rs = stt.executeQuery("SELECT distance FROM train_requests");

		int temp = 0;

		int[] distance = new int[size(rs)];

		while(rs.next()){

			distance[temp] = rs.getInt("distance") / 1000;

			temp++;
		}

		rs.close();

		return distance;
	}

	/**alle Distance-Einträge in der Datenbank für den spezifisch gewählten Pool**/
		public int[] getPoolDayDistances(Statement stt, String [] citys, String day) throws SQLException
		{
			stt.execute("USE requests_final");

			ResultSet rs = null;

			int temp = 0;

			if(citys.length == 1)
				System.out.println("Pool zu klein!");

			if(citys.length == 2)
			{	
				rs = stt.executeQuery("SELECT distance FROM train_requests WHERE origin ='" + citys[0] + "' AND destination = '" + citys[1] + "' " +
						"AND departure_time LIKE '%Jan " + day + "%'");

			}

			if(citys.length == 3)
			{

				rs = stt.executeQuery("SELECT distance FROM train_requests WHERE origin = '" + citys[0] + 
						"' AND departure_time LIKE '%Jan " + day + "%' AND destination IN ('" + citys[1] + "', '" + citys[2] + "')");

			}

			if(citys.length == 4)

			{
				rs = stt.executeQuery("SELECT distance FROM train_requests WHERE origin = '" + citys[0] + 
						"' AND departure_time LIKE '%Jan " + day + "%' AND destination IN ('" + citys[1] +"', '" + citys[2] + "', '" + citys[3] + "')");
			}

			if(citys.length == 5)

			{
				rs = stt.executeQuery("SELECT distance FROM train_requests WHERE origin = '" + citys[0] + 
						"' AND departure_time LIKE '%Jan " + day + "%' AND destination IN ('" + citys[1] +"', '" + citys[2] + "', '" + citys[3] + "', '" + citys[4] + "')");

			}

			if(citys.length >=6)
				System.out.println("Pool zu groß!");



			int[] distances = new int[size(rs)];

			while (rs.next())
			{
				distances[temp] = rs.getInt("distance") / 1000;

				temp++;
			}


			rs.close();

			return distances;
		}


		/**Selektierung und Rückgabe aller spezifischen Pool-City Durations eines bestimmten Tages**/
		public int [] getPoolDayDurations(Statement stt, String[] citys, String day) throws SQLException

		{

			stt.execute("USE requests_final");



			int temp = 0;

			int std = 0;
			int min = 0;
			int dur = 0;



			String[] segs;

			ResultSet rs = null;


			if(citys.length == 1)
				System.out.println("Pool zu klein!");



			if(citys.length == 2)
			{
				rs = stt.executeQuery("SELECT duration FROM train_requests WHERE origin ='" + citys[0] + "' AND departure_time LIKE '%Jan " + day + "%' AND destination = '" + citys[1] + "'");

			}

			if(citys.length == 3)
			{

				rs = stt.executeQuery("SELECT duration FROM train_requests WHERE origin = '" + citys[0] + 
						"' AND departure_time LIKE '%Jan " + day + "%' AND destination IN ('" + citys[1] +"', '" + citys[2] + "')");

			}
			if(citys.length == 4)

			{

				rs = stt.executeQuery("SELECT duration FROM train_requests WHERE origin = '" + citys[0] + 
						"' AND departure_time LIKE '%Jan " + day + "%' AND destination IN ('" + citys[1] +"', '" + citys[2] + "', '" + citys[3] + "')");
			}

			if(citys.length == 5)

			{
				rs = stt.executeQuery("SELECT duration FROM train_requests WHERE origin = '" + citys[0] + 
						"' AND departure_time LIKE '%Jan " + day + "%' AND destination IN ('" + citys[1] +"', '" + citys[2] + "', '" + citys[3] + "', '" + citys[4] + "')");

			}

			if(citys.length >=6)
				System.out.println("Pool zu groß!");



			int[] duration = new int[size(rs)];
			String[] tempArr = new String[size(rs)];


			while(rs.next()){

				tempArr[temp] = rs.getString("duration").trim();

				segs = tempArr[temp].split(Pattern.quote(" "));

				if (segs.length == 2)
				{
					min = Integer.parseInt(segs[0]);
					dur = min;
					duration[temp] = dur;    

				}

				else 

				{
					std = Integer.parseInt(segs[0]);
					min = Integer.parseInt(segs[2]);
					dur = std * 60 + min;
					duration[temp] = dur;                                                                                                                                                               

				}
				temp++;
			}

			rs.close();
			return duration;

		}


		/**Alle Destinations von Origin aus**/
		public String[] getDestinations(Statement stt, String origin) throws SQLException
		{

			stt.execute("USE requests_final");

			ResultSet rs = stt.executeQuery("SELECT destination FROM train_requests WHERE origin =  '" + origin + "'");

			int temp = 0;

			String[] tempArr = new String[size(rs)];
			String[] dest = new String[24];

			while(rs.next())

			{
				tempArr[temp] = rs.getString("destination");

				temp++;
			}

			temp = 0;

			for(int i = 0; i < tempArr.length - 1; i++)

			{
				if(i == tempArr.length - 2)
					dest[temp] = tempArr[i];

				if(tempArr[i].compareTo(tempArr[i + 1]) != 0)

				{
					dest[temp] = tempArr[i];
					temp++;
				}

			}
			rs.close();
			return dest;
		}

		/**Alle Städte in der Train-DB**/
		public String[] getOrigins(Statement stt) throws SQLException
		{

			stt.execute("USE requests_final");

			ResultSet rs = stt.executeQuery("SELECT origin FROM train_requests");


			int temp = 0;

			String[] tempArr = new String[size(rs)];
			String[] origin = new String[25];


			while(rs.next())

			{
				tempArr[temp] = rs.getString("origin");

				temp++;
			}

			temp = 0;

			for (int i = 0; i < tempArr.length -1; i++)

			{
				if(i == tempArr.length - 2)

					origin[temp] = tempArr[i];

				if(tempArr[i].compareTo(tempArr[i + 1]) != 0)

				{
					origin[temp] = tempArr[i];
					++temp;
				}

			}
			rs.close();

			return origin;

		}

		
		public String getSpecificRouteDest(Statement stt, String origin, String destination) throws SQLException
		{

			stt.execute("USE requests_final");

			ResultSet rs = stt.executeQuery("SELECT destination FROM train_requests WHERE origin =  '" 
					+ origin + "' AND destination = '" + destination + "'");

			String dest = null;

			while(rs.next()){

				dest = rs.getString("destination");
			}

			rs.close();
			return dest;
		}

		/**Selektiert und gibt die Tages-Verbindungsdauer von A nach B zurück**/
		public double getSpecificRouteDayDuration(Statement stt, String origin, String destination, String day) throws SQLException
		{
			stt.execute("USE requests_final");

			ResultSet rs = stt.executeQuery("SELECT duration FROM train_requests WHERE origin =  '" 
					+ origin + "'AND departure_time LIKE '%Jan " + day + "%' AND destination = '" + destination + "'" +
							"OR origin = '" + destination + "' AND departure_time LIKE '%Jan " + day + "%' AND destination = '"
							+ origin + "'");

			int std = 0;
			int min = 0;
			int dur = 0;

			int[] tempArr = new int[size(rs)];

			double duration = 0.0;

			String[] segs;

			String temp;

			int x = 0;

			while(rs.next()){

				temp = rs.getString("duration").trim();

				segs = temp.split(Pattern.quote(" "));

				if (segs.length == 2)
				{
					min = Integer.parseInt(segs[0]);
					dur = min;
					tempArr[x] = dur;                                                                                                                                                               

				}
				else 
				{
					std = Integer.parseInt(segs[0]);
					min = Integer.parseInt(segs[2]);
					dur = std * 60 + min ;
					tempArr[x] = dur;                                                                                                                                                               
				}
				x++;
			}

			for(int i = 0; i<tempArr.length; i++)
				duration = duration + tempArr[i];
			duration = duration / tempArr.length;

			rs.close();
			return duration;

		}

		/**neue Funktion zum Abfragen von Distanz-Gesamtdurchschnittswerten; Ladezeit; rszabad **/
		public double getSpecificRouteWholeDistance(Statement stt, String origin, String destination) throws SQLException
		{

			stt.execute("USE requests_final");

			ResultSet rs = stt.executeQuery("SELECT AVG(distance) AS summe FROM train_requests WHERE origin =  \""+ origin +"\" AND destination = \""+ destination +"\" OR origin =  \""+ destination +"\" AND destination = \""+ origin +"\"");
			
			double distance = 0;
			while(rs.next()){
				distance =  rs.getDouble("summe")/1000;
			}
			rs.close();

			return distance;

		}
		
		/**neue Funktion zum Abfragen von Fahrtzeit-Gesamtdurchschnittswerten; Ladezeit; rszabad **/
		public double getSpecificRouteWholeDuration(Statement stt, String origin, String destination) throws SQLException
		{

			stt.execute("USE requests_final");

			ResultSet rs = stt.executeQuery("SELECT AVG(durationminutes) AS summe FROM train_requests WHERE origin =  \""+ origin +"\" AND destination = \""+ destination +"\" OR origin =  \""+ destination +"\" AND destination = \""+ origin +"\"");
			
			double duration = 0;
			while(rs.next()){
				duration =  rs.getDouble("summe");
			}
			rs.close();

			return duration;

		}
		
		/**Selektiert und gibt die spezifische Tages-Distanz für die Strecke A nach B zurück**/
		public double getSpecificRouteDayDistance(Statement stt, String origin, String destination, String day) throws SQLException
		{

			stt.execute("USE requests_final");

			ResultSet rs = stt.executeQuery("SELECT distance FROM train_requests WHERE origin =  '" + origin + "" +
					"' AND departure_time LIKE '%Jan " + day + "%' AND destination = '" + destination + "' " +
							"OR origin = '" + destination + "' AND departure_time LIKE '%Jan " + day + "%' AND destination = '"
							+ origin + "'");

			double[] tempArr = new double[size(rs)];

			int temp = 0;

			double distance = 0.0;

			while(rs.next()){

				tempArr[temp] = rs.getDouble("distance") / 1000;
				temp++;
			}

			for(int i = 0; i<tempArr.length; i++)
				distance = distance + tempArr[i];
			distance = distance / tempArr.length;
			
			rs.close();

			return distance;


		}
		

		/**Gibt den spezifischen Origin zurück**/
		public String getSpecificOrigin(Statement stt, String origin)
				throws SQLException {
			{

				stt.execute("USE requests_final");

				ResultSet rs = stt.executeQuery("SELECT origin FROM train_requests WHERE origin =  '" 
						+ origin + "'");

				String temp = null;


				while(rs.next()){

					temp = rs.getString("origin");

				}


				rs.close();
				return temp;

			}
		}

		/**neue Funktion zum Abfragen von Fahrtzeit-Wochenwerten mit BETWEEN; Ladezeit; rszabad
		 * @throws SQLException **/
		public double getSpecificRouteWeekDuration(Statement stt, String origin, String destination, int week) throws SQLException {
			
			stt.execute("USE requests_final");
			ResultSet rs = null;
			
			switch(week){
				case 1: rs = stt.executeQuery("SELECT AVG(durationminutes) AS summe FROM train_requests WHERE origin =  \""+ origin +"\" AND destination = \""+ destination +"\" OR origin =  \""+ destination +"\" AND destination = \""+ origin +"\" AND timestamp BETWEEN \"2016-01-01\" AND \"2016-01-10\" "); break;
				case 2: rs = stt.executeQuery("SELECT AVG(durationminutes) AS summe FROM train_requests WHERE origin =  \""+ origin +"\" AND destination = \""+ destination +"\" OR origin =  \""+ destination +"\" AND destination = \""+ origin +"\" AND timestamp BETWEEN \"2016-01-11\" AND \"2016-01-17\" "); break;
				case 3: rs = stt.executeQuery("SELECT AVG(durationminutes) AS summe FROM train_requests WHERE origin =  \""+ origin +"\" AND destination = \""+ destination +"\" OR origin =  \""+ destination +"\" AND destination = \""+ origin +"\" AND timestamp BETWEEN \"2016-01-18\" AND \"2016-01-24\" "); break;
				case 4: rs = stt.executeQuery("SELECT AVG(durationminutes) AS summe FROM train_requests WHERE origin =  \""+ origin +"\" AND destination = \""+ destination +"\" OR origin =  \""+ destination +"\" AND destination = \""+ origin +"\" AND timestamp BETWEEN \"2016-01-24\" AND \"2016-01-31\" "); break;
				//default: rs = stt.executeQuery("SELECT AVG(durationminutes) AS summe FROM train_requests WHERE origin =  \""+ origin +"\" AND destination = \""+ destination +"\" OR origin =  \""+ destination +"\" AND destination = \""+ origin +"\" AND timestamp BETWEEN \"2016-01-01\" AND \"2016-01-31\" "); 
				
			
			}
			double duration = 0;
			while(rs.next()){
				duration =  rs.getDouble("summe");
			}
			rs.close();

			return duration;
		}
		
		
		/**neue Funktion zum Abfragen von Distanz-Wochenwerten mit BETWEEN; Ladezeit; rszabad
		 * @throws SQLException **/
		public double getSpecificRouteWeekDistance(Statement stt, String origin, String destination, int week) throws SQLException {
			
			stt.execute("USE requests_final");
			ResultSet rs = null;
			
			switch(week){
				case 1: rs = stt.executeQuery("SELECT AVG(distance) AS summe FROM train_requests WHERE origin =  \""+ origin +"\" AND destination = \""+ destination +"\" OR origin =  \""+ destination +"\" AND destination = \""+ origin +"\" AND timestamp BETWEEN \"2016-01-01\" AND \"2016-01-10\" "); break;
				case 2: rs = stt.executeQuery("SELECT AVG(distance) AS summe FROM train_requests WHERE origin =  \""+ origin +"\" AND destination = \""+ destination +"\" OR origin =  \""+ destination +"\" AND destination = \""+ origin +"\" AND timestamp BETWEEN \"2016-01-11\" AND \"2016-01-17\" "); break;
				case 3: rs = stt.executeQuery("SELECT AVG(distance) AS summe FROM train_requests WHERE origin =  \""+ origin +"\" AND destination = \""+ destination +"\" OR origin =  \""+ destination +"\" AND destination = \""+ origin +"\" AND timestamp BETWEEN \"2016-01-18\" AND \"2016-01-24\" "); break;
				case 4: rs = stt.executeQuery("SELECT AVG(distance) AS summe FROM train_requests WHERE origin =  \""+ origin +"\" AND destination = \""+ destination +"\" OR origin =  \""+ destination +"\" AND destination = \""+ origin +"\" AND timestamp BETWEEN \"2016-01-24\" AND \"2016-01-31\" "); break;
				//default: rs = stt.executeQuery("SELECT AVG(distance) AS summe FROM train_requests WHERE origin =  \""+ origin +"\" AND destination = \""+ destination +"\" OR origin =  \""+ destination +"\" AND destination = \""+ origin +"\" AND timestamp BETWEEN \"2016-01-01\" AND \"2016-01-31\" "); 
				
			
			}
			double distance = 0;
			while(rs.next()){
				distance =  rs.getDouble("summe") / 1000;
			}
			rs.close();

			return distance;
		}
	}
