package database;import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.regex.Pattern;


/**abstrakte Klasse Transaction, deklariert gemeinsame Methoden für CarTransaction und TrainTransaction und definiert
 * besondere Funktionalitäten**/
public abstract class Transaction {


	/** alle Duration-Einträge in der Datenbank**/
	public abstract int[] getAllDurations(Statement stt) throws SQLException;

	/**alle Distance-Einträge in der Datenbank, wird für die Pool-Berechnung benötigt**/
	public abstract int[] getAllDistances(Statement stt) throws SQLException;


	/**alle Origins (alle Städte)**/
	public abstract String[] getOrigins(Statement stt) throws SQLException;

	/**spezifischer Origin**/
	public abstract String getSpecificOrigin(Statement stt, String origin) throws SQLException;
	
	/**Größe des ResultSets, wird für fast alle Transaktionen benötigt**/
	public int size(ResultSet res) throws SQLException {

		int size;
		res.last();
		size = res.getRow();
		res.beforeFirst();

		return size;
	}

	/**Gibt den Latitude eines bestimmten Origins zurück**/
	public double getSpecificLat(Statement stt, String origin) throws SQLException
	{
		stt.execute("USE requests_final");

		ResultSet rs = stt.executeQuery("SELECT origin_lat_long FROM car_requests WHERE origin =  '" + origin + "'");


		double lat = 0;

		String s = new String();

		String[] segs;

		while(rs.next()){

			s = rs.getString("origin_lat_long");

			segs = s.split(Pattern.quote(", "));

			lat = Double.parseDouble(segs[0].substring(1));
			//System.out.println(temp + ":     " + lat[temp]+ "  |  " + Double.parseDouble(segs[1].substring(0, segs[1].length()-1)));

		}

		rs.close();
		return lat;
	}
	/**Gibt den Longitude eines bestimmten Origins zurück**/
	public double getSpecificLon(Statement stt, String origin) throws SQLException
	{
		stt.execute("USE requests_final");

		ResultSet rs = stt.executeQuery("SELECT origin_lat_long FROM car_requests WHERE origin =  '" + origin + "'");


		double lat = 0;

		String s = new String();

		String[] segs;


		while(rs.next()){

			s = rs.getString("origin_lat_long");

			segs = s.split(Pattern.quote(", "));

			lat = Double.parseDouble(segs[1].substring(0, segs[1].length()-1));

		}

		rs.close();
		return lat;

	}
	
	public int[] get24Durations(Statement stt) throws SQLException

	{

		stt.execute("USE requests_final");

		TrainTransaction tr = new TrainTransaction();

		String[] citys = tr.getOrigins(stt);

		ResultSet rs;

		rs = stt.executeQuery("SELECT duration FROM car_requests WHERE origin IN ( '" + citys[0] + "', '" + citys[1] + "', '" + citys[2] + "', '" + 
				citys[3] + "', '" + citys[4] + "', '" + citys[5] + "', '" + citys[6] + "', '" + citys[7] + "', " +
				"'" + citys[8] + "', '" + citys[9] + "', '" + citys[10] + "', '" + citys[11] + "', '" + citys[12] + "', '" + citys[13] + "', '" + citys[14] + "', '" + citys[15] + "', " +
				"'" + citys[16] + "', '" + citys[17] + "', '" + citys[18] + "', '" + citys[19] + "', '" + citys[20] + "', '" + citys[21] + "', '" + citys[22] + "', '" + citys[23] + "'," +
				" '" + citys[24] + "' )"		
				+" AND destination IN ( '" + citys[0] + "', '" + citys[1] + "', '" + citys[2] + "', '" + citys[3] + "', '" + citys[4] + "', '" + citys[5] + "', '" + citys[6] + "', '" + citys[7] + "', " +
				"'" + citys[8] + "', '" + citys[9] + "', '" + citys[10] + "', '" + citys[11] + "', '" + citys[12] + "', '" + citys[13] + "', '" + citys[14] + "', '" + citys[15] + "', " +
				"'" + citys[16] + "', '" + citys[17] + "', '" + citys[18] + "', '" + citys[19] + "', '" + citys[20] + "', '" + citys[21] + "', '" + citys[22] + "', '" + citys[23] + "', '" + citys[24] + "' ) ");

		int temp = 0;

		int[] duration = new int[size(rs)];

		int std = 0;
		int min = 0;
		int dur = 0;

		String[] tempArr = new String[size(rs)];

		String[] segs;

		while(rs.next())

		{

			tempArr[temp] = rs.getString("duration").trim();

			segs = tempArr[temp].split(Pattern.quote(" "));

			if (segs.length == 2)
			{
				min = Integer.parseInt(segs[0]);
				dur = min;
				duration[temp] = dur;    

			}

			else 
			{
				std = Integer.parseInt(segs[0]);
				min = Integer.parseInt(segs[2]);
				dur = std * 60 + min;
				duration[temp] = dur;                                                                                                                                                               

			}
			temp++;
		}
		rs.close();
		return duration;

	}
	
	public int[] get24Distances(Statement stt) throws SQLException

	{

		stt.execute("USE requests_final");

		TrainTransaction tr = new TrainTransaction();

		String[] citys = tr.getOrigins(stt);

		ResultSet rs;

		rs = stt.executeQuery("SELECT distance FROM car_requests WHERE origin IN ( '" + citys[0] + "', '" + citys[1] + "', '" + citys[2] + "', '" + 
				citys[3] + "', '" + citys[4] + "', '" + citys[5] + "', '" + citys[6] + "', '" + citys[7] + "', " +
				"'" + citys[8] + "', '" + citys[9] + "', '" + citys[10] + "', '" + citys[11] + "', '" + citys[12] + "', '" + citys[13] + "', '" + citys[14] + "', '" + citys[15] + "', " +
				"'" + citys[16] + "', '" + citys[17] + "', '" + citys[18] + "', '" + citys[19] + "', '" + citys[20] + "', '" + citys[21] + "', '" + citys[22] + "', '" + citys[23] + "'," +
				" '" + citys[24] + "' )"
				+ " AND destination IN ( '" + citys[0] + "', '" + citys[1] + "', '" + citys[2] + "', '" + citys[3] + "', '" + citys[4] + "', '" + citys[5] + "', '" + citys[6] + "', '" + citys[7] + "', " +
				"'" + citys[8] + "', '" + citys[9] + "', '" + citys[10] + "', '" + citys[11] + "', '" + citys[12] + "', '" + citys[13] + "', '" + citys[14] + "', '" + citys[15] + "', " +
				"'" + citys[16] + "', '" + citys[17] + "', '" + citys[18] + "', '" + citys[19] + "', '" + citys[20] + "', '" + citys[21] + "', '" + citys[22] + "', '" + citys[23] + "', '" + citys[24] + "' ) ");

		int temp = 0;

		int[] distance = new int[size(rs)];

		while(rs.next())

		{

			distance[temp] = rs.getInt("distance");

			temp++;
		}
		rs.close();
		return distance;

	}
	
	public String[] get24Origins(Statement stt) throws SQLException
	{

		stt.execute("USE requests_final");

		ResultSet rs = stt.executeQuery("SELECT origin FROM car_requests WHERE origin IN ( 'Kiel, Germany', 'Hamburg, Germany', " +
				"'Bremen, Germany', 'Hanover, Germany', 'Magdeburg, Germany', 'Berlin, Germany', 'Potsdam, Germany', " +
				"'Dresden, Germany', 'Erfurt, Germany', 'Wiesbaden, Germany',  'D�sseldorf, Germany', 'Mainz, Germany', 'Stuttgart, Germany'," +
				"'Munich, Germany', 'Saarbr�cken, Germany', 'Frankfurt, Germany', 'Nuremberg, Germany', 'Cologne, Germany', 'Leverkusen, Germany', " +
				"'Bochum, Germany', 'Kassel, Germany', 'Chemnitz, Germany', 'Gelsenkirchen, Germany', 'Bonn, Germany' )");


		int temp = 0;

		String[] originTemp = new String[size(rs)];
		
		String[] origin = new String[24];

		while(rs.next()){

			originTemp[temp] = rs.getString("origin");

			temp++;
		}
		
		
		temp = 0;

		for (int i = 0; i < originTemp.length -1; i++)

		{
			if(i == originTemp.length - 2)

				origin[temp] = originTemp[i];

			if(originTemp[i].compareTo(originTemp[i + 1]) != 0)

			{
				origin[temp] = originTemp[i];
				++temp;
			}

		}

		rs.close();
		return origin;
	}
	
	
	public String[] get24Destinations(Statement stt, String origin) throws SQLException
	{

		stt.execute("USE requests_final");

		ResultSet rs = stt.executeQuery("SELECT destination FROM car_requests WHERE origin IN ( 'Kiel, Germany', 'Hamburg, Germany', " +
				"'Bremen, Germany', 'Hanover, Germany', 'Magdeburg, Germany', 'Berlin, Germany', 'Potsdam, Germany', " +
				"'Dresden, Germany', 'Erfurt, Germany', 'Wiesbaden, Germany',  'D�sseldorf, Germany', 'Mainz, Germany', 'Stuttgart, Germany'," +
				"'Munich, Germany', 'Saarbr�cken, Germany', 'Frankfurt, Germany', 'Nuremberg, Germany', 'Cologne, Germany', 'Leverkusen, Germany', " +
				"'Bochum, Germany', 'Kassel, Germany', 'Chemnitz, Germany', 'Gelsenkirchen, Germany', 'Bonn, Germany' )");


		int temp = 0;

		String[] originTemp = new String[size(rs)];
		
		String[] originArr = new String[24];

		while(rs.next()){

			originTemp[temp] = rs.getString("origin");

			temp++;
		}
		
		
		temp = 0;

		for (int i = 0; i < originTemp.length -1; i++)

		{
			if(i == originTemp.length - 2)

				originArr[temp] = originTemp[i];

			if(originTemp[i].compareTo(originTemp[i + 1]) != 0)

			{
				originArr[temp] = originTemp[i];
				++temp;
			}

		}

		rs.close();
		return originArr;
	}

	public double[] getLatitudesTrain(Statement stt) throws SQLException, InstantiationException, IllegalAccessException, ClassNotFoundException
	{
		stt.execute("USE requests_final");

		ResultSet rs = stt.executeQuery("SELECT origin_lat_long FROM car_requests WHERE origin IN ( 'Kiel, Germany', 'Hamburg, Germany'," +
				"'Bremen, Germany', 'Hanover, Germany', 'Magdeburg, Germany', 'Berlin, Germany', 'Potsdam, Germany', " +
				"'Dresden, Germany', 'Erfurt, Germany', 'Wiesbaden, Germany',  'D�sseldorf, Germany', 'Mainz, Germany', 'Stuttgart, Germany'," +
				"'Munich, Germany', 'Saarbr�cken, Germany', 'Frankfurt, Germany', 'Nuremberg, Germany', 'Cologne, Germany', 'Leverkusen, Germany', " +
				"'Bochum, Germany', 'Kassel, Germany', 'Chemnitz, Germany', 'Gelsenkirchen, Germany', 'Bonn, Germany' )");
		
		
		double[] lat = new double[24];

		double[] a = new double[size(rs)];

		String[] tempArr = new String[size(rs)];
		
		String[] segs;

		int temp = 0;

		while(rs.next()){

			tempArr[temp] = rs.getString("origin_lat_long");
			
			segs = tempArr[temp].split(Pattern.quote(", "));

			a[temp] = Double.parseDouble(segs[0].substring(1));


			temp++;
		}
		
		temp = 0;

		for (int i = 0; i < tempArr.length -1; i++)

		{
			if(i == tempArr.length - 2)

				lat[temp] = a[i];
			
			
			if(tempArr[i].compareTo(tempArr[i + 1]) != 0)

			{
				lat[temp] = a[i];
				++temp;
			}

		}

		rs.close();
		
		return lat;
	}
	/**Holt die Latitudes aller Städte**/
	public double[] getLatitudes (Statement stt) throws SQLException
	{
		stt.execute("USE requests_final");

		ResultSet rs = stt.executeQuery("SELECT origin_lat_long FROM car_requests WHERE destination =  'Berlin, Germany' OR res_id = '1'");


		double[] lat = new double[size(rs)];

		String[] tempArr = new String[size(rs)];

		String[] segs;

		int temp = 0;

		while(rs.next()){

			tempArr[temp] = rs.getString("origin_lat_long");

			segs = tempArr[temp].split(Pattern.quote(", "));

			lat[temp] = Double.parseDouble(segs[0].substring(1));


			temp++;
		}

		rs.close();
		return lat;
		
	}
	
	public double[] getLongitudesTrain(Statement stt) throws SQLException
	{
		stt.execute("USE requests_final");

		ResultSet rs = stt.executeQuery("SELECT origin_lat_long FROM car_requests WHERE origin IN ( 'Kiel, Germany', 'Hamburg, Germany'," +
				"'Bremen, Germany', 'Hanover, Germany', 'Magdeburg, Germany', 'Berlin, Germany', 'Potsdam, Germany', " +
				"'Dresden, Germany', 'Erfurt, Germany', 'Wiesbaden, Germany',  'D�sseldorf, Germany', 'Mainz, Germany', 'Stuttgart, Germany'," +
				"'Munich, Germany', 'Saarbr�cken, Germany', 'Frankfurt, Germany', 'Nuremberg, Germany', 'Cologne, Germany', 'Leverkusen, Germany'" +
				", 'Bochum, Germany', 'Kassel, Germany', 'Chemnitz, Germany', 'Gelsenkirchen, Germany', 'Bonn, Germany' )");

		double[] lon = new double[24];

		double[] a = new double[size(rs)];

		String[] tempArr = new String[size(rs)];

		String[] segs;

		int temp = 0;

		while(rs.next()){

			tempArr[temp] = rs.getString("origin_lat_long");

			segs = tempArr[temp].split(Pattern.quote(", "));

			a[temp] = Double.parseDouble(segs[1].substring(0, segs[1].length()-1));

			temp++;
		}

		temp = 0;

		for (int i = 0; i < tempArr.length -1; i++)

		{
			if(i == tempArr.length - 2)

				lon[temp] = a[i];

			if(tempArr[i].compareTo(tempArr[i + 1]) != 0)

			{
				lon[temp] = a[i];
				++temp;
			}

		}

		rs.close();
		return lon;
	}

	/**Holt die Longitudes aller Städte**/
	public double[] getLongitudes(Statement stt) throws SQLException
	{
		stt.execute("USE requests_final");

		ResultSet rs = stt.executeQuery("SELECT origin_lat_long FROM car_requests WHERE destination =  'Berlin, Germany' OR res_id = '1'");


		double[] lon = new double[size(rs)];

		String[] tempArr = new String[size(rs)];

		String[] segs;

		int temp = 0;

		while(rs.next()){

			tempArr[temp] = rs.getString("origin_lat_long");

			segs = tempArr[temp].split(Pattern.quote(", "));

			lon[temp] = Double.parseDouble(segs[1].substring(0, segs[1].length()-1));

			temp++;
		}

		rs.close();
		return lon;
	}



}
