package database;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.regex.Pattern;

import javax.xml.datatype.Duration;

/**Hier werden alle Car-DB Transaktionen getätigt**/
public class CarTransaction extends Transaction

{
	/** alle Duration-Einträge in der Datenbank für die Berechnung des kompletten Pool-Avgs**/
	public int[] getAllDurations(Statement stt) throws SQLException
	{

		stt.execute("USE requests_final");

		ResultSet rs = stt.executeQuery("SELECT duration FROM car_requests");

		int temp = 0;

		int[] duration = new int[size(rs)];

		int std = 0;
		int min = 0;
		int dur = 0;

		String[] tempArr = new String[size(rs)];

		String[] segs;

		while(rs.next()){

			tempArr[temp] = rs.getString("duration").trim();

			segs = tempArr[temp].split(Pattern.quote(" "));

			if (segs.length == 2)
			{
				min = Integer.parseInt(segs[0]);
				dur = min;
				duration[temp] = dur;    

			}
			else 

			{
				std = Integer.parseInt(segs[0]);
				min = Integer.parseInt(segs[2]);
				dur = std * 60 + min;
				duration[temp] = dur;                                                                                                                                                               


			}
			temp++;
		}
		rs.close();
		return duration;
	}
	/**alle Distance-Einträge in der Datenbank, wird für die Gesamtpool-Berechnung benötigt**/
	public int[] getAllDistances(Statement stt) throws SQLException
	{

		stt.execute("USE requests_final");

		ResultSet rs = stt.executeQuery("SELECT distance FROM car_requests");

		int temp = 0;

		int[] distance = new int[size(rs)];

		while(rs.next()){

			distance[temp] = rs.getInt("distance");

			temp++;
		}

		rs.close();

		return distance;
	}
	
	/**Pool City Entfernungen vom Origin aus **/
	public int[] getPoolDistances(Statement stt, String[] citys) throws SQLException

	{

		stt.execute("USE requests_final");

		ResultSet rs = null;

		int[] distances = new int[citys.length -1];

		int temp = 0;

		if(citys.length == 1)
			System.out.println("Pool ist zu klein!");

		if(citys.length == 2)
		{	
			rs = stt.executeQuery("SELECT distance FROM car_requests WHERE origin ='" + citys[0] + "' AND destination = '" + citys[1] + "'");

		}

		if(citys.length == 3)
		{

			rs = stt.executeQuery("SELECT distance FROM car_requests WHERE origin = '" + citys[0] + 
					"' AND destination IN ('" + citys[1] + "', '" + citys[2] + "')");

		}

		if(citys.length == 4)

		{
			rs = stt.executeQuery("SELECT distance FROM car_requests WHERE origin = '" + citys[0] + 
					"' AND destination IN ('" + citys[1] +"', '" + citys[2] + "', '" + citys[3] + "')");
		}

		if(citys.length == 5)

		{
			rs = stt.executeQuery("SELECT distance FROM car_requests WHERE origin = '" + citys[0] + 
					"' AND destination IN ('" + citys[1] +"', '" + citys[2] + "', '" + citys[3] + "', '" + citys[4] + "')");

		}

		if(citys.length >=6)
			System.out.println("Pool zu groß!");

		while (rs.next())
		{
			distances[temp] = rs.getInt("distance");

			temp++;
		}


		rs.close();

		return distances;
	}

	/**Pool City Durations vom Origin aus **/
	public int[] getPoolDurations(Statement stt, String[] citys) throws SQLException 

	{

		stt.execute("USE requests_final");

		int[] duration = new int[citys.length -1];

		int temp = 0;

		int std = 0;
		int min = 0;
		int dur = 0;

		String[] tempArr = new String[citys.length -1];

		String[] segs;

		ResultSet rs = null;

		if(citys.length == 1)
			System.out.println("Pool zu klein!");

		if(citys.length == 2)
		{
			rs = stt.executeQuery("SELECT duration FROM car_requests WHERE origin ='" + citys[0] + "' AND destination = '" + citys[1] + "'");

		}

		if(citys.length == 3)
		{

			rs = stt.executeQuery("SELECT duration FROM car_requests WHERE origin = '" + citys[0] + 
					"' AND destination IN ('" + citys[1] +"', '" + citys[2] + "')");

		}
		if(citys.length == 4)

		{

			rs = stt.executeQuery("SELECT duration FROM car_requests WHERE origin = '" + citys[0] + 
					"' AND destination IN ('" + citys[1] +"', '" + citys[2] + "', '" + citys[3] + "')");
		}

		if(citys.length == 5)

		{
			rs = stt.executeQuery("SELECT duration FROM car_requests WHERE origin = '" + citys[0] + 
					"' AND destination IN ('" + citys[1] +"', '" + citys[2] + "', '" + citys[3] + "', '" + citys[4] + "')");

		}

		if(citys.length >=6)
			System.out.println("Pool zu groß!");


		while(rs.next()){

			tempArr[temp] = rs.getString("duration").trim();

			segs = tempArr[temp].split(Pattern.quote(" "));

			if (segs.length == 2)
			{
				min = Integer.parseInt(segs[0]);
				dur = min;
				duration[temp] = dur;    

			}

			else 

			{
				std = Integer.parseInt(segs[0]);
				min = Integer.parseInt(segs[2]);
				dur = std * 60 + min;
				duration[temp] = dur;                                                                                                                                                               

			}
			temp++;
		}



		rs.close();
		return duration;
	}

	/**Alle Destinations von Origin aus**/
	public String[] getDestinations(Statement stt, String origin) throws SQLException
	{

		stt.execute("USE requests_final");

		ResultSet rs = stt.executeQuery("SELECT destination FROM car_requests WHERE origin =  '" + origin + "'");

		int temp = 0;

		String[] dest = new String[size(rs)];

		while(rs.next()){

			dest[temp] = rs.getString("destination");

			temp++;
		}

		rs.close();
		return dest;
	}

	
	
	
	/**alle Origins (alle Städte)**/
	public String[] getOrigins(Statement stt) throws SQLException
	{

		stt.execute("USE requests_final");

		ResultSet rs = stt.executeQuery("SELECT origin FROM car_requests WHERE destination = 'Berlin, Germany' OR res_id = 1");


		int temp = 0;

		String[] origin = new String[size(rs)];

		while(rs.next()){

			origin[temp] = rs.getString("origin");

			temp++;
		}

		rs.close();
		return origin;
	}


	/**Alle Durations im Gesamtpool**/
	public int[] getDurations(Statement stt, String origin) throws SQLException
	{
		stt.execute("USE requests_final");

		ResultSet rs = stt.executeQuery("SELECT duration FROM car_requests WHERE origin =  '" + origin + "'");

		int temp = 0;

		int[] duration = new int[size(rs)];

		String[] segs;

		int std = 0;
		int min = 0;
		int dur = 0;

		String[] tempArr = new String[size(rs)];

		while(rs.next()){

			tempArr[temp] = rs.getString("duration").trim();

			segs = tempArr[temp].split(Pattern.quote(" "));


			if (segs.length == 2)
			{

				min = Integer.parseInt(segs[0]);
				dur = min;
				duration[temp] = dur;                                                                                                                                                               

			}
			else
			{
				std = Integer.parseInt(segs[0]);
				min = Integer.parseInt(segs[2]);
				dur = std * 60 + min;
				duration[temp] = dur;
			}

			temp++;
		}

		rs.close();
		return duration;
	}


	/**Holt den Destination aus der Datenbank**/
	public String getSpecificRouteDest(Statement stt, String origin, String destination) throws SQLException
	{

		stt.execute("USE requests_final");

		ResultSet rs = stt.executeQuery("SELECT destination FROM car_requests WHERE origin =  '" 
				+ origin + "' AND destination = '" + destination + "'");

		String dest = null;

		while(rs.next()){

			dest = rs.getString("destination");
		}

		rs.close();
		return dest;
	}


	/**Gibt die Verbindungsdauer von A nach B zurück**/
	public double getSpecificRouteDuration(Statement stt, String origin, String destination) throws SQLException
	{
		stt.execute("USE requests_final");

		ResultSet rs = stt.executeQuery("SELECT duration FROM car_requests WHERE origin =  '" 
				+ origin + "' AND destination = '" + destination + "' OR origin = '" + destination + "' AND destination = '" + origin + "'");

		int std = 0;
		int min = 0;
		int dur = 0;

		double duration = 0.0;
		
		double[] durationArr = new double[size(rs)];

		String[] segs;

		String temp;
		
		int x = 0;

		while(rs.next()){

			temp = rs.getString("duration").trim();

			segs = temp.split(Pattern.quote(" "));

			if (segs.length == 2)
			{
				min = Integer.parseInt(segs[0]);
				dur = min;
				durationArr[x] = dur;                                                                                                                                                               

			}
			else 
			{
				std = Integer.parseInt(segs[0]);
				min = Integer.parseInt(segs[2]);
				dur = std * 60 + min ;
				durationArr[x] = dur;                                                                                                                                                               

			}
			
			x++;
		}
		
		for(int i = 0; i<durationArr.length; i++)
			duration = duration + durationArr[i];
		
		duration = duration / durationArr.length;
		
		rs.close();
		return duration;

	}


	/**Gibt die Distanz von A nach B zurück**/
	public double getSpecificRouteDistance(Statement stt, String origin, String destination) throws SQLException
	{

		stt.execute("USE requests_final");

		ResultSet rs = stt.executeQuery("SELECT distance FROM car_requests WHERE origin =  '" + origin + "' AND destination = '" + destination + "' " +
				"OR origin = '" + destination + "' AND destination = '" + origin + "'");

		double distance = 0.0;
		
		double[] distanceArr = new double[size(rs)];
		
		String[] segs;

		String x;
		
		int temp = 0;

		while(rs.next()){
			
			x = rs.getString("distance").trim();
			
			segs = x.split(Pattern.quote(" "));

			distanceArr[temp] = Double.parseDouble(segs[0]);
			
			temp++;
		}
		
		for(int i = 0; i <distanceArr.length; i++)
			distance = distance + distanceArr[i];
		
		distance = distance / distanceArr.length;
		
		rs.close();

		return distance;

	}

	/**Gibt den spezifischen Origin zurück**/
	public String getSpecificOrigin(Statement stt, String origin)
			throws SQLException {
		{

			stt.execute("USE requests_final");

			ResultSet rs = stt.executeQuery("SELECT origin FROM car_requests WHERE origin =  '" 
					+ origin + "'");

			String temp = null;


			while(rs.next()){

				temp = rs.getString("origin");

			}


			rs.close();
			return temp;

		}
	}
}
