package view;

import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;

import controller.MainController;
import controller.ProcessingController;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.GridLayout;
import java.awt.event.ComponentEvent;
import java.awt.event.ComponentListener;
import javax.swing.JSlider;
import javax.swing.event.ChangeListener;
import javax.swing.filechooser.FileNameExtensionFilter;
import javax.swing.event.ChangeEvent;
import javax.swing.JToggleButton;
import javax.swing.UIManager;
import javax.swing.UnsupportedLookAndFeelException;
import java.awt.GridBagLayout;
import java.awt.GridBagConstraints;
import javax.swing.BoxLayout;
import java.awt.CardLayout;
import com.jgoodies.forms.layout.FormLayout;
import com.jgoodies.forms.layout.ColumnSpec;
import com.jgoodies.forms.layout.RowSpec;
import net.miginfocom.swing.MigLayout;
import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import java.awt.Component;
import javax.swing.Box;
import javax.swing.JToolBar;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JMenu;
import javax.swing.JList;
import javax.swing.JComboBox;
import javax.swing.JFileChooser;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JLabel;
import java.awt.Insets;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import javax.swing.JButton;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import javax.swing.JProgressBar;
import javax.swing.border.EmptyBorder;
import javax.swing.border.MatteBorder;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;
import java.awt.Button;

/**
 * visuelle Klasse des Main-Window
 * 
 * @author rszabad
 *
 */
public class MainWindow {

	private JFrame frame;
	ProcessingController pC;
	private MainController parent;
	private JSlider slider_Left;
	private JSlider slider_Right;
	private JComboBox comboBoxVisuLeft;
	private JComboBox comboBoxVisuRight;
	private JComboBox comboBoxBaseLeft;
	private JComboBox comboBoxBaseRight;
	
	/**
	 * Applikation erzeugen
	 */
	public MainWindow(MainController parent) {
		this.parent = parent;
		
		try {
            // Set System L&F
        UIManager.setLookAndFeel(
            UIManager.getSystemLookAndFeelClassName());
    } 
		 catch (UnsupportedLookAndFeelException e) {
		       // handle exception
		    }
		    catch (ClassNotFoundException e) {
		       // handle exception
		    }
		    catch (InstantiationException e) {
		       // handle exception
		    }
		    catch (IllegalAccessException e) {
		       // handle exception
		    }	
		initialize();
	}

	/**
	 * Frame-Inhalte initialisieren
	 */
	private void initialize() {
		setFrame(new JFrame());
		getFrame().setBounds(100, 100, 818, 413);
		getFrame().setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		getFrame().setMinimumSize(new Dimension(1024, 700));
		
		JPanel panel = new JPanel();
		getFrame().getContentPane().add(panel, BorderLayout.CENTER);
		panel.setLayout(new GridLayout(1, 0, 0, 0));
		
		JPanel panel_Visu_Left = new JPanel();
		panel_Visu_Left.setBorder(null);
		panel.add(panel_Visu_Left);
		panel_Visu_Left.setBackground(Color.WHITE);
		
		JPanel panel_Visu_Right = new JPanel();
		panel.add(panel_Visu_Right);
		panel_Visu_Right.setBackground(Color.WHITE);
		
		this.parent.registerPanels(this.frame, panel_Visu_Left, panel_Visu_Right);
		panel_Visu_Right.setLayout(new BorderLayout(0, 0));
		panel_Visu_Left.setLayout(new BorderLayout(0, 0));
		
		JPanel panel_Controls = new JPanel();
		frame.getContentPane().add(panel_Controls, BorderLayout.SOUTH);
		panel_Controls.setLayout(new BorderLayout(0, 0));
		
		JPanel panel_1 = new JPanel();
		panel_Controls.add(panel_1, BorderLayout.NORTH);
		panel_1.setLayout(new GridLayout(1, 0, 0, 0));
		
		JPanel panel_ControlsLeft = new JPanel();
		panel_ControlsLeft.setBorder(null);
		panel_ControlsLeft.setBackground(Color.WHITE);
		panel_1.add(panel_ControlsLeft);
		GridBagLayout gbl_panel_ControlsLeft = new GridBagLayout();
		gbl_panel_ControlsLeft.columnWidths = new int[] {100, 100, 100, 100};
		gbl_panel_ControlsLeft.rowHeights = new int[] {41, 41};
		gbl_panel_ControlsLeft.columnWeights = new double[]{1.0, 0.0, 0.0, 1.0};
		gbl_panel_ControlsLeft.rowWeights = new double[]{0.0, 0.0};
		panel_ControlsLeft.setLayout(gbl_panel_ControlsLeft);
		
		slider_Left = new JSlider();
		slider_Left.setBackground(Color.WHITE);
		slider_Left.addChangeListener(new ChangeListener() {
			public void stateChanged(ChangeEvent arg0) {
				parent.sliderLeftChanged(slider_Left.getValue());
			}
		});
		slider_Left.setMinorTickSpacing(1);
		slider_Left.setMajorTickSpacing(1);
		slider_Left.setValue(2);
		slider_Left.setMaximum(5);
		slider_Left.setSnapToTicks(true);
		slider_Left.setPaintTicks(true);
		GridBagConstraints gbc_slider_Left = new GridBagConstraints();
		gbc_slider_Left.insets = new Insets(0, 5, 5, 5);
		gbc_slider_Left.gridwidth = 3;
		gbc_slider_Left.fill = GridBagConstraints.BOTH;
		gbc_slider_Left.gridx = 0;
		gbc_slider_Left.gridy = 0;
		panel_ControlsLeft.add(slider_Left, gbc_slider_Left);
		
		comboBoxVisuLeft = new JComboBox();
		
		comboBoxVisuLeft.setModel(new DefaultComboBoxModel(new String[] {"Map Simple", "Map Warped", "Bar Graph", "Table"}));
		GridBagConstraints gbc_comboBoxVisuLeft = new GridBagConstraints();
		gbc_comboBoxVisuLeft.insets = new Insets(0, 0, 5, 0);
		gbc_comboBoxVisuLeft.fill = GridBagConstraints.HORIZONTAL;
		gbc_comboBoxVisuLeft.gridx = 3;
		gbc_comboBoxVisuLeft.gridy = 0;
		panel_ControlsLeft.add(comboBoxVisuLeft, gbc_comboBoxVisuLeft);
		
		
		GridBagConstraints gbc_tglbtnNormalLeft = new GridBagConstraints();
		gbc_tglbtnNormalLeft.fill = GridBagConstraints.HORIZONTAL;
		gbc_tglbtnNormalLeft.insets = new Insets(0, 0, 0, 5);
		gbc_tglbtnNormalLeft.gridx = 0;
		gbc_tglbtnNormalLeft.gridy = 1;
		
		
		JToggleButton tglbtnShowInfoLeft = new JToggleButton("Show Info");
		GridBagConstraints gbc_tglbtnShowInfoLeft = new GridBagConstraints();
		gbc_tglbtnShowInfoLeft.fill = GridBagConstraints.HORIZONTAL;
		gbc_tglbtnShowInfoLeft.insets = new Insets(0, 0, 0, 5);
		gbc_tglbtnShowInfoLeft.gridx = 1;
		gbc_tglbtnShowInfoLeft.gridy = 1;
		panel_ControlsLeft.add(tglbtnShowInfoLeft, gbc_tglbtnShowInfoLeft);
		
	
		
		JToggleButton tglbtnShowAnchorsLeft = new JToggleButton("Show Anchors");
		GridBagConstraints gbc_tglbtnShowAnchorsLeft = new GridBagConstraints();
		gbc_tglbtnShowAnchorsLeft.insets = new Insets(0, 0, 0, 5);
		gbc_tglbtnShowAnchorsLeft.fill = GridBagConstraints.HORIZONTAL;
		gbc_tglbtnShowAnchorsLeft.gridx = 2;
		gbc_tglbtnShowAnchorsLeft.gridy = 1;
		panel_ControlsLeft.add(tglbtnShowAnchorsLeft, gbc_tglbtnShowAnchorsLeft);
		
		comboBoxBaseLeft = new JComboBox();
		comboBoxBaseLeft.setModel(new DefaultComboBoxModel(new String[] {"All Dataset Connections", "Chosen Connections", "Common Connections"}));
		GridBagConstraints gbc_comboBoxBaseLeft = new GridBagConstraints();
		gbc_comboBoxBaseLeft.fill = GridBagConstraints.HORIZONTAL;
		gbc_comboBoxBaseLeft.gridx = 3;
		gbc_comboBoxBaseLeft.gridy = 1;
		panel_ControlsLeft.add(comboBoxBaseLeft, gbc_comboBoxBaseLeft);
		
		

		
		
		JPanel panel_ControlsRight = new JPanel();
		panel_ControlsRight.setBackground(Color.WHITE);
		panel_1.add(panel_ControlsRight);
		GridBagLayout gbl_panel_ControlsRight = new GridBagLayout();
		gbl_panel_ControlsRight.columnWidths = new int[] {100, 100, 100, 100};
		gbl_panel_ControlsRight.rowHeights = new int[] {41, 41};
		gbl_panel_ControlsRight.columnWeights = new double[]{1.0, 0.0, 0.0, 1.0};
		gbl_panel_ControlsRight.rowWeights = new double[]{0.0, 0.0};
		panel_ControlsRight.setLayout(gbl_panel_ControlsRight);
		
		slider_Right = new JSlider();
		slider_Right.setBackground(Color.WHITE);
		slider_Right.addChangeListener(new ChangeListener() {
			public void stateChanged(ChangeEvent arg0) {
				parent.sliderRightChanged(slider_Right.getValue());
			}
		});
		slider_Right.setValue(2);
		slider_Right.setSnapToTicks(true);
		slider_Right.setPaintTicks(true);
		slider_Right.setMinorTickSpacing(1);
		slider_Right.setMaximum(5);
		slider_Right.setMajorTickSpacing(1);
		GridBagConstraints gbc_slider_Right = new GridBagConstraints();
		gbc_slider_Right.insets = new Insets(0, 0, 5, 5);
		gbc_slider_Right.gridwidth = 3;
		gbc_slider_Right.fill = GridBagConstraints.BOTH;
		gbc_slider_Right.gridx = 0;
		gbc_slider_Right.gridy = 0;
		panel_ControlsRight.add(slider_Right, gbc_slider_Right);
		
		comboBoxVisuRight = new JComboBox();
		comboBoxVisuRight.setModel(new DefaultComboBoxModel(new String[] {"Map Simple", "Map Warped", "Bar Graph", "Table"}));
		GridBagConstraints gbc_comboBoxVisuRight = new GridBagConstraints();
		gbc_comboBoxVisuRight.insets = new Insets(0, 0, 5, 0);
		gbc_comboBoxVisuRight.fill = GridBagConstraints.HORIZONTAL;
		gbc_comboBoxVisuRight.gridx = 3;
		gbc_comboBoxVisuRight.gridy = 0;
		panel_ControlsRight.add(comboBoxVisuRight, gbc_comboBoxVisuRight);
		
		comboBoxVisuLeft.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				parent.setVizualisation(comboBoxVisuLeft.getSelectedItem().toString(), comboBoxVisuRight.getSelectedItem().toString());
			
				if(comboBoxVisuLeft.getSelectedItem().toString().equals("Map Simple") || comboBoxVisuLeft.getSelectedItem().toString().equals("Map Warped")){		
					
					tglbtnShowInfoLeft.setEnabled(true); 
					tglbtnShowAnchorsLeft.setEnabled(true); 
					comboBoxBaseLeft.setEnabled(true);		
				}
			
				if(comboBoxVisuLeft.getSelectedItem().toString().equals("Bar Graph")){		
					
					tglbtnShowInfoLeft.setEnabled(true); 
					tglbtnShowAnchorsLeft.setEnabled(false); 
					comboBoxBaseLeft.setEnabled(true);		
				}
				
				if(comboBoxVisuLeft.getSelectedItem().toString().equals("Table")){		
					
					tglbtnShowInfoLeft.setEnabled(false); 
					tglbtnShowAnchorsLeft.setEnabled(false); 
					comboBoxBaseLeft.setEnabled(false);		
				}
				
			}
		});
		
		JToggleButton tglbtnNormalRight = new JToggleButton("Normal");
		
		GridBagConstraints gbc_tglbtnNormalRight = new GridBagConstraints();
		gbc_tglbtnNormalRight.fill = GridBagConstraints.HORIZONTAL;
		gbc_tglbtnNormalRight.insets = new Insets(0, 0, 0, 5);
		gbc_tglbtnNormalRight.gridx = 0;
		gbc_tglbtnNormalRight.gridy = 1;
		panel_ControlsRight.add(tglbtnNormalRight, gbc_tglbtnNormalRight);
		
	
		
		JToggleButton toggleButtonShowAnchorsRight = new JToggleButton("Show Anchors");
		GridBagConstraints gbc_toggleButtonShowAnchorsRight = new GridBagConstraints();
		gbc_toggleButtonShowAnchorsRight.fill = GridBagConstraints.HORIZONTAL;
		gbc_toggleButtonShowAnchorsRight.insets = new Insets(0, 0, 0, 5);
		gbc_toggleButtonShowAnchorsRight.gridx = 2;
		gbc_toggleButtonShowAnchorsRight.gridy = 1;
		panel_ControlsRight.add(toggleButtonShowAnchorsRight, gbc_toggleButtonShowAnchorsRight);
		
		toggleButtonShowAnchorsRight.addChangeListener(new ChangeListener() {
			public void stateChanged(ChangeEvent arg0) {
				parent.setShowAnchors(tglbtnShowAnchorsLeft.isSelected(), toggleButtonShowAnchorsRight.isSelected());
			}
		});
		
		JToggleButton tglbtnNormalLeft = new JToggleButton("Normal");
		tglbtnNormalLeft.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				
				parent.setNormal(tglbtnNormalLeft.isSelected(), tglbtnNormalRight.isSelected());
	
			}
		});
		
		JToggleButton toggleButtonShowInfoRight = new JToggleButton("Show Info");
		GridBagConstraints gbc_toggleButtonShowInfoRight = new GridBagConstraints();
		gbc_toggleButtonShowInfoRight.insets = new Insets(0, 0, 0, 5);
		gbc_toggleButtonShowInfoRight.fill = GridBagConstraints.HORIZONTAL;
		gbc_toggleButtonShowInfoRight.gridx = 1;
		gbc_toggleButtonShowInfoRight.gridy = 1;
		panel_ControlsRight.add(toggleButtonShowInfoRight, gbc_toggleButtonShowInfoRight);
		
		comboBoxBaseRight = new JComboBox();
		
		comboBoxBaseRight.setModel(new DefaultComboBoxModel(new String[] {"All Dataset Connections", "Chosen Connections", "Common Connections"}));
		GridBagConstraints gbc_comboBoxBaseRight = new GridBagConstraints();
		gbc_comboBoxBaseRight.fill = GridBagConstraints.HORIZONTAL;
		gbc_comboBoxBaseRight.gridx = 3;
		gbc_comboBoxBaseRight.gridy = 1;
		panel_ControlsRight.add(comboBoxBaseRight, gbc_comboBoxBaseRight);
		
		comboBoxBaseLeft.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				parent.setBase(comboBoxBaseLeft.getSelectedItem().toString(), comboBoxBaseRight.getSelectedItem().toString());
			}
		});
		
		comboBoxBaseRight.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				parent.setBase(comboBoxBaseLeft.getSelectedItem().toString(), comboBoxBaseRight.getSelectedItem().toString());
			}
		});
		
		tglbtnShowAnchorsLeft.addChangeListener(new ChangeListener() {
			public void stateChanged(ChangeEvent arg0) {
				parent.setShowAnchors(tglbtnShowAnchorsLeft.isSelected(), toggleButtonShowAnchorsRight.isSelected());
			}
		});
		
		toggleButtonShowInfoRight.addChangeListener(new ChangeListener() {
			public void stateChanged(ChangeEvent e) {
				parent.setShowInfo(tglbtnShowInfoLeft.isSelected(), toggleButtonShowInfoRight.isSelected());
			}
		});
		
		tglbtnShowInfoLeft.addChangeListener(new ChangeListener() {
			public void stateChanged(ChangeEvent e) {
				parent.setShowInfo(tglbtnShowInfoLeft.isSelected(), toggleButtonShowInfoRight.isSelected());
			}
		});
		
		JPanel panel_4 = new JPanel();
		panel_4.setBorder(new MatteBorder(1, 0, 0, 0, (Color) new Color(0, 0, 0)));
		panel_Controls.add(panel_4, BorderLayout.SOUTH);
		panel_4.setBackground(Color.WHITE);
		
		JSlider slider_Speed = new JSlider();
		slider_Speed.setMajorTickSpacing(10);
		slider_Speed.setMinorTickSpacing(5);
		slider_Speed.setBackground(Color.WHITE);
		slider_Speed.addChangeListener(new ChangeListener() {
			public void stateChanged(ChangeEvent arg0) {
				parent.playbackSpeedChanged(1000 - slider_Speed.getValue());
			}
		});
		
		tglbtnNormalRight.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
						parent.setNormal(tglbtnNormalLeft.isSelected(), tglbtnNormalRight.isSelected());

			}
		});
		
		panel_ControlsLeft.add(tglbtnNormalLeft, gbc_tglbtnNormalLeft);
		
		panel_4.setLayout(new FlowLayout(FlowLayout.CENTER, 5, 5));
		slider_Speed.setMaximum(800);
		panel_4.add(slider_Speed);
		
		JToggleButton tglbtn_Play = new JToggleButton("Play");
		tglbtn_Play.addChangeListener(new ChangeListener() {
			public void stateChanged(ChangeEvent e) {
				parent.setPlaybackOn(tglbtn_Play.isSelected());
			}
		});
		panel_4.add(tglbtn_Play);
		
		JToggleButton tglbtn_Timeline = new JToggleButton("Single Timeline");
		tglbtn_Timeline.addChangeListener(new ChangeListener() {
			public void stateChanged(ChangeEvent e) {
				parent.setSingleTimeline(tglbtn_Timeline.isSelected());
			}
		});
		panel_4.add(tglbtn_Timeline);
		
		JButton btnLoadData = new JButton("Load Data");
		btnLoadData.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				parent.loadData();
			}
		});
		panel_4.add(btnLoadData);
		
		JButton btnScreenshot = new JButton("Screenshot");
		btnScreenshot.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				
				JFileChooser chooser = new JFileChooser();
				FileNameExtensionFilter filter = new FileNameExtensionFilter(
				        "PDF", "pdf");
			    chooser.setFileFilter(filter);
			    int returnVal = chooser.showSaveDialog(null);
			    if(returnVal == JFileChooser.APPROVE_OPTION) {
			     
			          
			            
			            parent.takeScreenshot( chooser.getSelectedFile().getPath());
			            
			    }
				
			}
		});
		panel_4.add(btnScreenshot);
		
		JButton btnReset = new JButton("Reset");
		btnReset.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				
				parent.reset();
				
			}
		});
		panel_4.add(btnReset);
		
		
		
		//ProcessingController////////////////////////////////////		
		//manuell, damit Gr��enwerte berechnet werden --> Sketchsize abh�ngig von Windowsize
				getFrame().setVisible(true);
				
				//Controller f�r Processingsketche
				//pC = new ProcessingController(this.frame, panel_Visu_Left, panel_Visu_Right);
		
				
				
		//Listener////////////////////////////////////
		getFrame().addComponentListener(new ComponentListener() {
			@Override
			public void componentHidden(ComponentEvent arg0) {
				// TODO Auto-generated method stub
				
			}

			@Override
			public void componentMoved(ComponentEvent arg0) {
				// TODO Auto-generated method stub
				
			}

			@Override
			public void componentResized(ComponentEvent arg0) {
				//Bei Gr��en�nderung werden Sketches skaliert
				parent.windowChanged();	
			}

			@Override
			public void componentShown(ComponentEvent arg0) {
				// TODO Auto-generated method stub
				
			}
		});
		
		comboBoxVisuRight.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				parent.setVizualisation(comboBoxVisuLeft.getSelectedItem().toString(), comboBoxVisuRight.getSelectedItem().toString());
			
				if(comboBoxVisuRight.getSelectedItem().toString().equals("Map Simple") || comboBoxVisuRight.getSelectedItem().toString().equals("Map Warped")){		
			
					toggleButtonShowInfoRight.setEnabled(true); 
					toggleButtonShowAnchorsRight.setEnabled(true); 
					comboBoxBaseRight.setEnabled(true);		
				}
			
				if(comboBoxVisuRight.getSelectedItem().toString().equals("Bar Graph")){		
					
					toggleButtonShowInfoRight.setEnabled(true); 
					toggleButtonShowAnchorsRight.setEnabled(false); 
					comboBoxBaseRight.setEnabled(true);		
				}
				
				if(comboBoxVisuRight.getSelectedItem().toString().equals("Table")){		
					
					toggleButtonShowInfoRight.setEnabled(false); 
					toggleButtonShowAnchorsRight.setEnabled(false); 
					comboBoxBaseRight.setEnabled(false);		
				}
			
			}
		});
	}

	public JFrame getFrame() {
		return frame;
	}

	public void setFrame(JFrame frame) {
		this.frame = frame;
	}
	
	public void setSliderLeft(int Maximum, int spacing){
		getSlider_Left().setMinorTickSpacing(1);
		getSlider_Left().setMajorTickSpacing(spacing);
		getSlider_Left().setValue(0);
		getSlider_Left().setMaximum(Maximum);

	}
	
	public void setSliderRight(int Maximum, int spacing){
		getSlider_Right().setMinorTickSpacing(1);
		getSlider_Right().setMajorTickSpacing(spacing);
		getSlider_Right().setValue(0);
		getSlider_Right().setMaximum(Maximum);

	}

	public JSlider getSlider_Left() {
		return slider_Left;
	}
	public JSlider getSlider_Right() {
		return slider_Right;
	}

	
	
	public JComboBox getComboBoxVisuLeft() {
		return comboBoxVisuLeft;
	}
	public JComboBox getComboBoxVisuRight() {
		return comboBoxVisuRight;
	}

	public JComboBox getComboBoxBaseLeft() {
		return comboBoxBaseLeft;
	}
	public JComboBox getComboBoxBaseRight() {
		return comboBoxBaseRight;
	}
}
