package view;

import java.awt.BorderLayout;
import java.awt.FlowLayout;

import javax.swing.AbstractButton;
import javax.swing.ButtonGroup;
import javax.swing.DefaultListModel;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import controller.MainController;
import database.City;

import javax.swing.JList;
import javax.swing.JTextField;
import javax.swing.JTable;
import java.awt.GridBagLayout;
import java.awt.GridBagConstraints;
import java.awt.Insets;
import java.util.ArrayList;
import java.util.Enumeration;

import javax.swing.table.DefaultTableModel;
import javax.swing.JScrollPane;
import javax.swing.ScrollPaneConstants;
import javax.swing.SwingConstants;
import javax.swing.JRadioButton;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import javax.swing.JProgressBar;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import javax.swing.border.MatteBorder;
import java.awt.Color;

/**
 * visuelle Dialog-Klasse zur Auswahl von Verbindungen f�r die Visualisierung
 * 
 * @author rszabad
 *
 */

public class DataLoader extends JDialog {

	private final JPanel contentPanel = new JPanel();
	private String cityList[][];
	private String[][] cityList1;
	private String[][] cityList2;
	private MainController parent;
	private JTable table;
	private JTable table_1;
	private JScrollPane scrollPane;
	private JScrollPane scrollPane_1;
	private JScrollPane scrollPane_2;
	private JTable table_2;
	private JPanel panel;
	private JPanel panel_1;
	private JButton btnAdd;
	private JButton btnRemove;
	private JPanel panel_2;
	private JRadioButton rdbtnCar;
	private JRadioButton rdbtnTrain;
	private JRadioButton rdbtnLeft;
	private JRadioButton rdbtnRight;
	private JRadioButton rdbtnBoth;

	private ArrayList<String> connectionsList = new ArrayList<String>();
	private JRadioButton rdbtnAll;
	private JRadioButton rdbtnWeekly;
	private JRadioButton rdbtnDaily;
	private JPanel panel_3;
	private JPanel panel_4;

	/**
	 * Erzeugen des Dialogs mit den beiden St�dtelisten f�r Auto und Zug
	 * 
	 * @param mainController
	 * @param cityList2
	 * @param cityList
	 */
	public DataLoader(MainController mainController, String[] cityDataList, String[] cityDataList2) {
		setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);

		this.parent = mainController;
		this.cityList1 = new String[cityDataList.length][1];
		for (int i = 0; i < cityDataList.length; i++) {
			this.cityList1[i][0] = cityDataList[i];
		}
		this.cityList2 = new String[cityDataList2.length][1];
		for (int i = 0; i < cityDataList2.length; i++) {
			this.cityList2[i][0] = cityDataList2[i];
		}

		cityList = cityList1;

		setBounds(100, 100, 764, 563);
		getContentPane().setLayout(new BorderLayout());
		contentPanel.setBorder(new EmptyBorder(5, 5, 5, 5));
		getContentPane().add(contentPanel, BorderLayout.CENTER);
		contentPanel.setLayout(new BorderLayout(0, 0));
		{
			panel = new JPanel();
			contentPanel.add(panel, BorderLayout.CENTER);
			panel.setLayout(new BorderLayout(0, 0));
			{
				JPanel panelList = new JPanel();
				panel.add(panelList, BorderLayout.CENTER);
				GridBagLayout gbl_panelList = new GridBagLayout();
				gbl_panelList.columnWidths = new int[] { 86, 86, 86 };
				gbl_panelList.rowHeights = new int[] { 193 };
				gbl_panelList.columnWeights = new double[] { 1.0, 1.0, 1.0 };
				gbl_panelList.rowWeights = new double[] { 1.0 };
				panelList.setLayout(gbl_panelList);
				{
					scrollPane_1 = new JScrollPane();
					scrollPane_1.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
					GridBagConstraints gbc_scrollPane_1 = new GridBagConstraints();
					gbc_scrollPane_1.insets = new Insets(0, 0, 5, 5);
					gbc_scrollPane_1.fill = GridBagConstraints.BOTH;
					gbc_scrollPane_1.gridx = 1;
					gbc_scrollPane_1.gridy = 0;
					panelList.add(scrollPane_1, gbc_scrollPane_1);
					{
						table_1 = new JTable();
						table_1.setShowGrid(false);
						table_1.setFillsViewportHeight(true);
						scrollPane_1.setViewportView(table_1);
						table_1.setModel(new DefaultTableModel(this.cityList, new String[] { "" }) {
							boolean[] columnEditables = new boolean[] { false };

							public boolean isCellEditable(int row, int column) {
								return columnEditables[column];
							}
						});
						// table_1.setAutoCreateRowSorter(true);
					}
				}
				{
					scrollPane = new JScrollPane();
					scrollPane.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
					GridBagConstraints gbc_scrollPane = new GridBagConstraints();
					gbc_scrollPane.insets = new Insets(0, 0, 5, 5);
					gbc_scrollPane.fill = GridBagConstraints.BOTH;
					gbc_scrollPane.gridx = 0;
					gbc_scrollPane.gridy = 0;
					panelList.add(scrollPane, gbc_scrollPane);
					{
						table = new JTable();
						table.setFillsViewportHeight(true);
						scrollPane.setViewportView(table);
						table.setShowGrid(false);
						table.setModel(new DefaultTableModel(this.cityList, new String[] { "" }) {
							boolean[] columnEditables = new boolean[] { false };

							public boolean isCellEditable(int row, int column) {
								return columnEditables[column];
							}
						});
						// table.setAutoCreateRowSorter(true);
					}
				}
				{
					scrollPane_2 = new JScrollPane();
					scrollPane_2.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);

					GridBagConstraints gbc_scrollPane_2 = new GridBagConstraints();
					gbc_scrollPane_2.insets = new Insets(0, 0, 5, 0);
					gbc_scrollPane_2.fill = GridBagConstraints.BOTH;
					gbc_scrollPane_2.gridx = 2;
					gbc_scrollPane_2.gridy = 0;
					panelList.add(scrollPane_2, gbc_scrollPane_2);
					{
						table_2 = new JTable();
						table_2.setModel(new DefaultTableModel(new Object[][] {}, new String[] { "" }));
						table_2.setShowGrid(false);
						table_2.setFillsViewportHeight(true);

						// table_2.setAutoCreateRowSorter(true);
						scrollPane_2.setViewportView(table_2);
					}
				}
			}
			{
				panel_1 = new JPanel();
				panel.add(panel_1, BorderLayout.SOUTH);
				panel_1.setLayout(new BorderLayout(0, 0));
				{
					btnAdd = new JButton("Add");
					btnAdd.addMouseListener(new MouseAdapter() {
						@Override
						public void mouseClicked(MouseEvent arg0) {

							if (table.getSelectedRows().length == 1 && table_1.getSelectedRows().length == 1
									&& table_1.getSelectedRow() != table.getSelectedRow()) {
								boolean isAlreadyIn = checkInside(cityList[table.getSelectedRow()][0],
										cityList[table_1.getSelectedRow()][0]);
								if (isAlreadyIn == false) {
									connectionsList.add(cityList[table.getSelectedRow()][0] + " - "
											+ cityList[table_1.getSelectedRow()][0]);
									setConnectionsModel();
								}
							}

							if (table.getSelectedRows().length == 1 && table_1.getSelectedRows().length > 1) {

								for (int i = 0; i < table_1.getSelectedRows().length; i++) {
									if (table.getSelectedRow() != table_1.getSelectedRows()[i]) {
										boolean isAlreadyIn = checkInside(cityList[table.getSelectedRow()][0],
												cityList[table_1.getSelectedRows()[i]][0]);
										if (isAlreadyIn == false) {
											connectionsList.add(cityList[table.getSelectedRow()][0] + " - "
													+ cityList[table_1.getSelectedRows()[i]][0]);
										}
									}
								}
								setConnectionsModel();

							}

							if (table.getSelectedRows().length > 1 && table_1.getSelectedRows().length == 1) {

								for (int i = 0; i < table.getSelectedRows().length; i++) {
									if (table.getSelectedRows()[i] != table_1.getSelectedRow()) {
										boolean isAlreadyIn = checkInside(cityList[table.getSelectedRows()[i]][0],
												cityList[table_1.getSelectedRow()][0]);
										if (isAlreadyIn == false) {
											connectionsList.add(cityList[table.getSelectedRows()[i]][0] + " - "
													+ cityList[table_1.getSelectedRow()][0]);
										}
									}
								}
								setConnectionsModel();

							}

							if (table.getSelectedRows().length > 1 && table_1.getSelectedRows().length > 1) {

								for (int o = 0; o < table.getSelectedRows().length; o++) {
									for (int i = 0; i < table_1.getSelectedRows().length; i++) {
										if (table.getSelectedRows()[o] != table_1.getSelectedRows()[i]) {
											boolean isAlreadyIn = checkInside(cityList[table.getSelectedRows()[o]][0],
													cityList[table_1.getSelectedRows()[i]][0]);
											if (isAlreadyIn == false) {
												connectionsList.add(cityList[table.getSelectedRows()[o]][0] + " - "
														+ cityList[table_1.getSelectedRows()[i]][0]);
											}
										}
									}
								}
								setConnectionsModel();

							}

						}

					});
					panel_1.add(btnAdd, BorderLayout.WEST);
				}
				{
					btnRemove = new JButton("Remove");
					btnRemove.addMouseListener(new MouseAdapter() {
						@Override
						public void mouseClicked(MouseEvent arg0) {

							for (int i = 0; i < table_2.getSelectedRows().length; i++) {
								connectionsList.remove(table_2.getSelectedRows()[i] - i);
							}
							setConnectionsModel();
						}
					});
					panel_1.add(btnRemove, BorderLayout.EAST);
				}
				{
					panel_2 = new JPanel();
					panel_1.add(panel_2, BorderLayout.CENTER);
					{
						rdbtnCar = new JRadioButton("Car");

						rdbtnCar.setSelected(true);
						panel_2.add(rdbtnCar);
					}
					{
						rdbtnTrain = new JRadioButton("Train");

						panel_2.add(rdbtnTrain);
					}
				}
			}
		}

		ButtonGroup carTrain = new ButtonGroup();
		carTrain.add(rdbtnCar);
		carTrain.add(rdbtnTrain);
		{
			panel_3 = new JPanel();
			FlowLayout flowLayout = (FlowLayout) panel_3.getLayout();
			flowLayout.setVgap(13);
			flowLayout.setHgap(1);
			panel_3.setBorder(new MatteBorder(0, 3, 0, 0, (Color) new Color(0, 0, 0)));
			panel_2.add(panel_3);
		}
		{
			rdbtnBoth = new JRadioButton("Both");
			rdbtnBoth.setSelected(true);
			panel_2.add(rdbtnBoth);
		}

		{
			rdbtnLeft = new JRadioButton("Left");
			panel_2.add(rdbtnLeft);
		}
		{
			rdbtnRight = new JRadioButton("Right");
			panel_2.add(rdbtnRight);
		}

		ButtonGroup visu = new ButtonGroup();
		visu.add(rdbtnLeft);
		visu.add(rdbtnRight);
		visu.add(rdbtnBoth);
		{
			panel_4 = new JPanel();
			FlowLayout flowLayout = (FlowLayout) panel_4.getLayout();
			flowLayout.setVgap(13);
			panel_4.setBorder(new MatteBorder(0, 3, 0, 0, (Color) new Color(0, 0, 0)));
			panel_2.add(panel_4);
		}
		{
			rdbtnAll = new JRadioButton("All");
			rdbtnAll.setEnabled(false);
			rdbtnAll.setSelected(true);
			panel_2.add(rdbtnAll);
		}
		{
			rdbtnWeekly = new JRadioButton("Weekly");
			rdbtnWeekly.setEnabled(false);
			panel_2.add(rdbtnWeekly);
		}
		{
			rdbtnDaily = new JRadioButton("Daily");
			rdbtnDaily.setEnabled(false);
			panel_2.add(rdbtnDaily);
		}

		/*
		 * rdbtnTrain.addMouseListener(new MouseAdapter() {
		 * 
		 * @Override public void mouseClicked(MouseEvent e) {
		 * 
		 * } });
		 */

		rdbtnTrain.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {

				if (getSelectedButtonText(carTrain).equals("Car")) {

					rdbtnAll.setEnabled(false);
					rdbtnWeekly.setEnabled(false);
					rdbtnDaily.setEnabled(false);

					cityList = cityList1;

					table.setModel(new DefaultTableModel(cityList, new String[] { "" }) {
						boolean[] columnEditables = new boolean[] { false };

						public boolean isCellEditable(int row, int column) {
							return columnEditables[column];
						}
					});
					table_1.setModel(new DefaultTableModel(cityList, new String[] { "" }) {
						boolean[] columnEditables = new boolean[] { false };

						public boolean isCellEditable(int row, int column) {
							return columnEditables[column];
						}
					});
					connectionsList = new ArrayList<String>();
					setConnectionsModel();
				}

				if (getSelectedButtonText(carTrain).equals("Train")) {

					rdbtnAll.setEnabled(true);
					rdbtnWeekly.setEnabled(true);
					rdbtnDaily.setEnabled(true);

					cityList = cityList2;

					table.setModel(new DefaultTableModel(cityList2, new String[] { "" }) {
						boolean[] columnEditables = new boolean[] { false };

						public boolean isCellEditable(int row, int column) {
							return columnEditables[column];
						}
					});
					table_1.setModel(new DefaultTableModel(cityList2, new String[] { "" }) {
						boolean[] columnEditables = new boolean[] { false };

						public boolean isCellEditable(int row, int column) {
							return columnEditables[column];
						}
					});
					connectionsList = new ArrayList<String>();
					setConnectionsModel();
				}

			}
		});

		rdbtnCar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {

				if (getSelectedButtonText(carTrain).equals("Car")) {

					cityList = cityList1;

					rdbtnAll.setEnabled(false);
					rdbtnWeekly.setEnabled(false);
					rdbtnDaily.setEnabled(false);

					table.setModel(new DefaultTableModel(cityList, new String[] { "" }) {
						boolean[] columnEditables = new boolean[] { false };

						public boolean isCellEditable(int row, int column) {
							return columnEditables[column];
						}
					});
					table_1.setModel(new DefaultTableModel(cityList, new String[] { "" }) {
						boolean[] columnEditables = new boolean[] { false };

						public boolean isCellEditable(int row, int column) {
							return columnEditables[column];
						}
					});
					connectionsList = new ArrayList<String>();
					setConnectionsModel();
				}
				if (getSelectedButtonText(carTrain).equals("Train")) {

					cityList = cityList2;

					rdbtnAll.setEnabled(true);
					rdbtnWeekly.setEnabled(true);
					rdbtnDaily.setEnabled(true);

					table.setModel(new DefaultTableModel(cityList2, new String[] { "" }) {
						boolean[] columnEditables = new boolean[] { false };

						public boolean isCellEditable(int row, int column) {
							return columnEditables[column];
						}
					});
					table_1.setModel(new DefaultTableModel(cityList2, new String[] { "" }) {
						boolean[] columnEditables = new boolean[] { false };

						public boolean isCellEditable(int row, int column) {
							return columnEditables[column];
						}
					});
					connectionsList = new ArrayList<String>();
					setConnectionsModel();
				}

			}
		});

		ButtonGroup period = new ButtonGroup();
		period.add(rdbtnAll);
		period.add(rdbtnWeekly);
		period.add(rdbtnDaily);

		{
			JPanel buttonPane = new JPanel();
			buttonPane.setLayout(new FlowLayout(FlowLayout.RIGHT));
			getContentPane().add(buttonPane, BorderLayout.SOUTH);
			{
				JButton okButton = new JButton("OK");
				okButton.addMouseListener(new MouseAdapter() {
					@Override
					public void mouseClicked(MouseEvent e) {

						String[][] connections = new String[connectionsList.size()][2];

						if (connectionsList.size() > 0) {

							for (int i = 0; i < connectionsList.size(); i++) {

								String c1 = connectionsList.get(i).substring(0,
										connectionsList.get(i).indexOf("-") - 1);
								String c2 = connectionsList.get(i).substring(connectionsList.get(i).indexOf("-") + 2,
										connectionsList.get(i).length());

								connections[i][0] = c1;
								connections[i][1] = c2;

							}

							setModal(false);
							setVisible(false);
							parent.loaderClosed(connections, getSelectedButtonText(carTrain),
									getSelectedButtonText(visu), getSelectedButtonText(period));
						}
						dispose();
					}
				});
				okButton.setActionCommand("OK");
				buttonPane.add(okButton);
				getRootPane().setDefaultButton(okButton);
			}
			{
				JButton cancelButton = new JButton("Cancel");
				cancelButton.addMouseListener(new MouseAdapter() {
					@Override
					public void mouseClicked(MouseEvent e) {
						dispose();
					}
				});
				cancelButton.setActionCommand("Cancel");
				buttonPane.add(cancelButton);
			}
		}

		this.setModal(true);
		this.setVisible(true);
	}

	private boolean checkInside(String c1, String c2) {
		String test = c1 + " - " + c2;
		String test2 = c2 + " - " + c1;

		for (int i = 0; i < connectionsList.size(); i++) {
			if (connectionsList.get(i).equals(test) || connectionsList.get(i).equals(test2)) {
				return true;
			}
		}
		return false;
	}

	private void setConnectionsModel() {

		String[][] tempOutput = new String[connectionsList.size()][1];
		for (int i = 0; i < connectionsList.size(); i++) {
			tempOutput[i][0] = connectionsList.get(i);
		}

		table_2.setModel(new DefaultTableModel(tempOutput, new String[] { "" }) {
			boolean[] columnEditables = new boolean[] { false };

			public boolean isCellEditable(int row, int column) {
				return columnEditables[column];
			}
		});

	}

	public String getSelectedButtonText(ButtonGroup buttonGroup) {
		for (Enumeration<AbstractButton> buttons = buttonGroup.getElements(); buttons.hasMoreElements();) {
			AbstractButton button = buttons.nextElement();

			if (button.isSelected()) {
				return button.getText();
			}
		}

		return null;
	}

}
