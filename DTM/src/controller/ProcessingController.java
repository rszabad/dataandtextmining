package controller;

import java.awt.BorderLayout;
import java.awt.Button;
import java.awt.Dimension;
import java.text.DecimalFormat;
import java.util.ArrayList;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;

import model.Connection;
import model.GroupedConnections;
import processing.PSketch;

/**
 * Verwaltet die beiden Processing-Sketche (LEFT und RIGHT)
 * 
 * @author rszabad
 *
 */

public class ProcessingController {

	// Parent-Objekte
	JFrame frame;
	JPanel panelLeft;
	JPanel panelRight;

	// Processing-Sketches
	PSketch sketchLeft;
	PSketch sketchRight;

	// Daten
	GroupedConnections groupedConnectionsLeft = null;
	GroupedConnections groupedConnectionsRight = null;
	GroupedConnections referenceConnectionsLeft = null;
	GroupedConnections referenceConnectionsRight = null;

	int stepLeft = 0;
	int stepRight = 0;
	private MainController parent;
	private boolean showInfo[] = { false, false };
	private boolean showAnchors[] = { false, false };
	private String factors[] = { "time/distance", "time/distance" };
	private String visualisation[] = { "Map Simple", "Map Simple" };
	private String[] base = { "All Dataset Connections", "All Dataset Connections" };
	float[] referenceDataLeft = { 1, 1 };
	float[] referenceDataRight = { 1, 1 };
	private String[] carTrain = { "Car", "Car" };
	DecimalFormat df = new DecimalFormat();
	private boolean[] normal = { false, false };

	/**
	 * liest die beiden Panels und das Parent Element, bevor die ersten Sketches
	 * erstellt werden
	 * 
	 * @param parent
	 * @param frame
	 * @param panelLeft
	 * @param panelRight
	 */
	public ProcessingController(MainController parent, JFrame frame, JPanel panelLeft, JPanel panelRight) {

		df.setMaximumFractionDigits(2);
		this.parent = parent;
		this.frame = frame;
		this.panelLeft = panelLeft;
		this.panelRight = panelRight;

		createSketches();
	}

	/**
	 * Start der beiden Sketches nach dem Erhalt von (Referenz-)Daten
	 * 
	 * @param groupedConnections1
	 * @param groupedConnections2
	 * @param referenceConnectionsLeft2
	 * @param referenceConnectionsRight2
	 */
	public void createSketches(GroupedConnections groupedConnections1, GroupedConnections groupedConnections2,
			GroupedConnections referenceConnectionsLeft2, GroupedConnections referenceConnectionsRight2) {

		this.groupedConnectionsLeft = groupedConnections1;
		this.groupedConnectionsRight = groupedConnections2;
		this.referenceConnectionsLeft = referenceConnectionsLeft2;
		this.referenceConnectionsRight = referenceConnectionsRight2;

		this.createSketches();
	}

	/**
	 * Start der Sketches mit Initiation von Konfigurationsparametern
	 */
	public void createSketches() {

		// Entfernen der alten Sketche, die evtl. in den Panels liegen
		try {
			sketchLeft.destroy();
		} catch (Exception e) {
		}
		try {
			sketchRight.destroy();
		} catch (Exception e) {
		}
		panelLeft.removeAll();
		panelRight.removeAll();
		try {
			sketchLeft.destroy();
		} catch (Exception e) {
		}
		try {
			sketchRight.destroy();
		} catch (Exception e) {
		}

		// Optimale Gr��e Ermitteln (immer quadratisch)
		int size;
		if (panelLeft.getWidth() <= panelLeft.getHeight() * 0.98) {
			size = (int) (panelLeft.getWidth() * 0.98);
		} else {
			size = (int) (panelLeft.getHeight());
		}

		// Sketches erzeugen und skalieren
		sketchLeft = new PSketch();
		sketchLeft.frame = this.frame;
		panelLeft.add(sketchLeft, BorderLayout.CENTER);

		sketchLeft.setPreferredSize(new Dimension(size, size));
		sketchLeft.setMinimumSize(new Dimension(size, size));
		sketchLeft.setAppHeight(size);
		sketchLeft.setAppWidth(size);
		sketchLeft.resize(size, size);
		sketchLeft.setGroupedConnections(this.groupedConnectionsLeft);
		sketchLeft.setReferenceConnections(this.referenceConnectionsLeft);
		sketchLeft.setShowAnchors(showAnchors[0]);
		sketchLeft.setShowInfos(showInfo[0]);
		sketchLeft.setFactor(factors[0]);
		sketchLeft.setVisualisation(visualisation[0]);
		sketchLeft.setBase(base[0]);
		sketchLeft.setFactor(factors[0]);
		sketchLeft.setNormal(this.normal[0]);
		sketchLeft.init();

		sketchRight = new PSketch();
		sketchRight.frame = this.frame;
		panelRight.add(sketchRight, BorderLayout.CENTER);

		sketchRight.setPreferredSize(new Dimension(size, size));
		sketchRight.setMinimumSize(new Dimension(size, size));
		sketchRight.setAppHeight(size);
		sketchRight.setAppWidth(size);
		sketchRight.resize(size, size);
		sketchRight.setGroupedConnections(this.groupedConnectionsRight);
		sketchRight.setReferenceConnections(this.referenceConnectionsRight);
		sketchRight.setShowAnchors(showAnchors[1]);
		sketchRight.setShowInfos(showInfo[1]);
		sketchRight.setFactor(factors[1]);
		sketchRight.setVisualisation(visualisation[1]);
		sketchRight.setBase(base[1]);
		sketchRight.setFactor(factors[1]);
		sketchRight.setNormal(this.normal[1]);
		sketchRight.init();

		setVisualisation(this.visualisation);

	}

	public void sliderLeftChanged(int value) {
		this.stepLeft = value;
		sketchLeft.setStep(stepLeft);

	}

	public void sliderRightChanged(int value) {
		this.stepRight = value;
		sketchRight.setStep(stepRight);

	}

	/**
	 * schaltet die Visualisierung bei automatischer Wiedergabe einen
	 * Zeitabschnitt weiter (oder zur�ck auf 0 bei �berlauf)
	 */
	public void stepForward() {

		if (this.stepLeft + 1 == this.groupedConnectionsLeft.size()) {
			this.stepLeft = 0;
		} else {
			stepLeft++;
		}

		if (this.stepRight + 1 == this.groupedConnectionsRight.size()) {
			this.stepRight = 0;
		} else {
			stepRight++;
		}

		sketchLeft.setStep(stepLeft);
		sketchRight.setStep(stepRight);

		int[] temp = new int[2];
		temp[0] = stepLeft;
		temp[1] = stepRight;

		parent.setSteps(temp);

	}

	public void setShowInfos(boolean[] showInfo2) {
		this.showInfo = showInfo2;
		sketchLeft.setShowInfos(this.showInfo[0]);
		sketchRight.setShowInfos(this.showInfo[1]);
	}

	public void setShowAnchors(boolean[] showAnchors) {
		this.showAnchors = showAnchors;
		sketchLeft.setShowAnchors(this.showAnchors[0]);
		sketchRight.setShowAnchors(this.showAnchors[1]);

	}

	public void setFactor(String[] factors) {
		this.factors = factors;
		sketchLeft.setFactor(this.factors[0]);
		sketchRight.setFactor(this.factors[1]);
	}

	/**
	 * Festlegung der Art der Visualisierung; legt f�r "Table" entsprechendes Datenmodel fest 
	 * 
	 * @param visualisation
	 */
	public void setVisualisation(String[] visualisation) {
		this.visualisation = visualisation;

		if (this.visualisation[0].equals("Table")) {

			panelLeft.remove(sketchLeft);

			Object[][] rowData = new Object[groupedConnectionsLeft.size() * groupedConnectionsLeft.sizeOfGroups()][6];

			for (int i = 0; i < groupedConnectionsLeft.size(); i++) {
				for (int o = 0; o < groupedConnectionsLeft.sizeOfGroups(); o++) {

					rowData[o + groupedConnectionsLeft.sizeOfGroups() * i][0] = groupedConnectionsLeft
							.getConnection(i, o).getOrigin();
					rowData[o + groupedConnectionsLeft.sizeOfGroups() * i][1] = groupedConnectionsLeft
							.getConnection(i, o).getDestination();
					rowData[o + groupedConnectionsLeft.sizeOfGroups() * i][2] = (int) groupedConnectionsLeft
							.getConnection(i, o).getDistance();
					rowData[o + groupedConnectionsLeft.sizeOfGroups() * i][3] = (int) groupedConnectionsLeft
							.getConnection(i, o).getDuration();
					rowData[o + groupedConnectionsLeft.sizeOfGroups() * i][4] = df
							.format(groupedConnectionsLeft.getConnection(i, o).getAvg());
					rowData[o + groupedConnectionsLeft.sizeOfGroups() * i][5] = i + 1;

				}
			}

			Object[] columnNames = null;
			if (groupedConnectionsLeft.size() == 1) {
				columnNames = new Object[5];
				columnNames[0] = "City";
				columnNames[1] = "City";
				columnNames[2] = "Distance in km";
				columnNames[3] = "Duration in min";
				columnNames[4] = "Average in km/min";
			}
			if (groupedConnectionsLeft.size() == 4) {
				columnNames = new Object[6];
				columnNames[0] = "City";
				columnNames[1] = "City";
				columnNames[2] = "Distance in km";
				columnNames[3] = "Duration in min";
				columnNames[4] = "Average in km/min";
				columnNames[5] = "Week of Month";
			}
			if (groupedConnectionsLeft.size() > 4) {
				columnNames = new Object[6];
				columnNames[0] = "City";
				columnNames[1] = "City";
				columnNames[2] = "Distance in km";
				columnNames[3] = "Duration in min";
				columnNames[4] = "Average in km/min";
				columnNames[5] = "Day of Month";
			}

			JTable table = new JTable(rowData, columnNames);

			JScrollPane scrollPane = new JScrollPane(table);
			panelLeft.add(scrollPane, BorderLayout.CENTER);
			panelLeft.repaint();
			panelLeft.revalidate();
		} else {
			panelLeft.removeAll();
			panelLeft.add(sketchLeft);
		}

		if (this.visualisation[1].equals("Table")) {

			panelRight.remove(sketchRight);

			Object[][] rowData = new Object[groupedConnectionsRight.size() * groupedConnectionsRight.sizeOfGroups()][6];

			for (int i = 0; i < groupedConnectionsRight.size(); i++) {
				for (int o = 0; o < groupedConnectionsRight.sizeOfGroups(); o++) {

					rowData[o + groupedConnectionsRight.sizeOfGroups() * i][0] = groupedConnectionsRight
							.getConnection(i, o).getOrigin();
					rowData[o + groupedConnectionsRight.sizeOfGroups() * i][1] = groupedConnectionsRight
							.getConnection(i, o).getDestination();
					rowData[o + groupedConnectionsRight.sizeOfGroups() * i][2] = (int) groupedConnectionsRight
							.getConnection(i, o).getDistance();
					rowData[o + groupedConnectionsRight.sizeOfGroups() * i][3] = (int) groupedConnectionsRight
							.getConnection(i, o).getDuration();
					rowData[o + groupedConnectionsRight.sizeOfGroups() * i][4] = df
							.format(groupedConnectionsRight.getConnection(i, o).getAvg());
					rowData[o + groupedConnectionsRight.sizeOfGroups() * i][5] = i + 1;

				}
			}

			Object[] columnNames = null;
			if (groupedConnectionsRight.size() == 1) {
				columnNames = new Object[5];
				columnNames[0] = "City";
				columnNames[1] = "City";
				columnNames[2] = "Distance in km";
				columnNames[3] = "Duration in min";
				columnNames[4] = "Average in km/min";
			}
			if (groupedConnectionsRight.size() == 4) {
				columnNames = new Object[6];
				columnNames[0] = "City";
				columnNames[1] = "City";
				columnNames[2] = "Distance in km";
				columnNames[3] = "Duration in min";
				columnNames[4] = "Average in km/min";
				columnNames[5] = "Week of Month";
			}
			if (groupedConnectionsRight.size() > 4) {
				columnNames = new Object[6];
				columnNames[0] = "City";
				columnNames[1] = "City";
				columnNames[2] = "Distance in km";
				columnNames[3] = "Duration in min";
				columnNames[4] = "Average in km/min";
				columnNames[5] = "Day of Month";
			}

			JTable table = new JTable(rowData, columnNames);

			JScrollPane scrollPane = new JScrollPane(table);
			panelRight.add(scrollPane, BorderLayout.CENTER);
			panelRight.repaint();
			panelRight.revalidate();
		} else {
			panelRight.removeAll();
			panelRight.add(sketchRight);
		}

		sketchLeft.setVisualisation(this.visualisation[0]);
		sketchRight.setVisualisation(this.visualisation[1]);

	}

	public void setBase(String[] base) {
		this.base = base;

		sketchLeft.setBase(this.base[0]);
		sketchRight.setBase(this.base[1]);

	}

	public void setcarTrain(String[] carTrain) {
		this.carTrain = carTrain;

		sketchLeft.setCarTrain(this.carTrain[0]);
		sketchRight.setCarTrain(this.carTrain[1]);
	}

	public void takeScreenshot(String path) {

		String pathLeft = path + "-Left";
		String pathRight = path + "-Right";

		sketchLeft.takeScreenshot(pathLeft);
		sketchRight.takeScreenshot(pathRight);
	}

	public void setNormal(boolean[] normal) {

		this.normal = normal;

		sketchLeft.setNormal(this.normal[0]);
		sketchRight.setNormal(this.normal[1]);
	}

}
