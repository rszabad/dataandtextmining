package controller;

import java.util.TimerTask;

public class TimeStepper extends TimerTask {

	MainController parent = null;

	public TimeStepper(MainController parent) {
		this.parent = parent;
	}

	public void run() {
		parent.timeOver();
	}
}
