package controller;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Timer;

import javax.swing.JFrame;
import javax.swing.JPanel;

import database.CarRoute;
import database.City;

import database.TrainRoute;
import model.Connection;
import model.GroupedConnections;
import view.DataLoader;
import view.MainWindow;

/**
 * Main Controller zum speichern und verwalten aller n�tigen Prozesse 
 * 
 * @author rszabad
 *
 */

public class MainController {

	// Visualisierungsdaten
	private GroupedConnections groupedConnectionsLeft;
	private GroupedConnections groupedConnectionsRight;
	// Referenzdaten
	private GroupedConnections referenceConnectionsLeft;
	private GroupedConnections referenceConnectionsRight;
	// Controller f�r Processing-Sketche
	private ProcessingController processingController;
	// Referenz zum Hauptfenster
	private MainWindow mainWindow;
	// Referenz zum DataLoader-Fenster
	private DataLoader dataLoader;
	// Wiedergabegeschwindigkeit in Visualisierung
	private int playbackSpeed = 0;
	// Flag f�r Wiedergabe
	boolean playbackOn = false;
	// Objekt um Wiedergabe zu timen
	Timer timer;
	// Flag zum Verbinden der Timelines
	boolean singleTimeline = false;
	// Flag f�r Ankerpunktanzeige
	private boolean showAnchors[] = { false, false };
	// Flag f�r Infoanzeige
	private boolean showInfo[] = { false, false };
	// Vergleichsfaktor f�r Visualisierung
	private String factor[] = { "time/distance", "time/distance" };
	// Art der Visualisierung
	private String visualisation[] = { "Map Simple", "Map Simple" };
	private String[] base = { "All Dataset Connections", "All Dataset Connections" };
	private String[] carTrain = { "Car", "Car" };
	private boolean[] normal = { false, false };

	/**
	 * Main
	 * 
	 * erzeugt View
	 */
	public MainController() {
		mainWindow = new MainWindow(this);
	}

	/**
	 * registriert die Panel-Objekte aus Swing, auf die zur Sketcherzeugung zugegriffen werden muss
	 * 
	 * @param mainFrame
	 * @param panel_Visu_Left
	 * @param panel_Visu_Right
	 */
	public void registerPanels(JFrame mainFrame, JPanel panel_Visu_Left, JPanel panel_Visu_Right) {
		processingController = new ProcessingController(this, mainFrame, panel_Visu_Left, panel_Visu_Right);
	}

	/**
	 * startet Sketches neu (called nach Fenstergr��en�nderung)
	 */
	public void windowChanged() {
		processingController.createSketches();
	}

	// Neue Daten einlesen, Steuerelemente zur�cksetzen, Visualisierung mit
	// neuen Daten starten
	
	/**
	 * liest neue Daten zur Visualisierung ein, setzt Steuerelemente zur�ck, startet Visualisierung neu
	 * 
	 * @param gc1 = Daten linke Visu
	 * @param gc2 = Daten rechte Visu
	 * @param gc3 = Referenzdaten linke Visu
	 * @param gc4 = Referenzdaten rechte Visu
	 */
	public void setConnectionData(GroupedConnections gc1, GroupedConnections gc2, GroupedConnections gc3,
			GroupedConnections gc4) {
		this.groupedConnectionsLeft = gc1;
		this.groupedConnectionsRight = gc2;
		this.referenceConnectionsLeft = gc3;
		this.referenceConnectionsRight = gc4;

		mainWindow.setSliderLeft(this.groupedConnectionsLeft.size() - 1, this.groupedConnectionsLeft.size() - 1);
		mainWindow.setSliderRight(this.groupedConnectionsRight.size() - 1, this.groupedConnectionsRight.size() - 1);
		mainWindow.getComboBoxBaseLeft().setSelectedItem(base[0]);
		mainWindow.getComboBoxBaseRight().setSelectedItem(base[1]);

		processingController.createSketches(this.groupedConnectionsLeft, this.groupedConnectionsRight,
				this.referenceConnectionsLeft, this.referenceConnectionsRight);

	}

	public void sliderLeftChanged(int value) {
		processingController.sliderLeftChanged(value);
		if (singleTimeline == true) {
			processingController.sliderRightChanged(value);
			mainWindow.getSlider_Right().setValue(processingController.stepRight);
		}

	}

	public void sliderRightChanged(int value) {
		processingController.sliderRightChanged(value);
		if (singleTimeline == true) {
			processingController.sliderLeftChanged(value);
			mainWindow.getSlider_Left().setValue(processingController.stepLeft);
		}

	}

	public void playbackSpeedChanged(int i) {
		this.playbackSpeed = i;
		if (timer != null) {
			this.timer.cancel();
			this.timer.purge();
		}
		this.timer = new Timer();

		timer.schedule(new TimeStepper(this), 0, playbackSpeed);
	}

	public void timeOver() {

		if (playbackOn == true) {
			processingController.stepForward();
		}
	}

	public void setPlaybackOn(boolean selected) {
		this.playbackOn = selected;

	}

	public void setSteps(int[] temp) {

		mainWindow.getSlider_Left().setValue(temp[0]);
		mainWindow.getSlider_Right().setValue(temp[1]);

	}

	public void setSingleTimeline(boolean selected) {
		this.singleTimeline = selected;
		if (this.singleTimeline == true) {
			processingController.stepRight = processingController.stepLeft;
			mainWindow.getSlider_Right().setValue(processingController.stepRight);
		}

	}

	public void setShowAnchors(boolean selectedLeft, boolean selectedRight) {
		this.showAnchors[0] = selectedLeft;
		this.showAnchors[1] = selectedRight;
		processingController.setShowAnchors(this.showAnchors);
	}

	public void setShowInfo(boolean selectedLeft, boolean selectedRight) {
		this.showInfo[0] = selectedLeft;
		this.showInfo[1] = selectedRight;
		processingController.setShowInfos(this.showInfo);
	}

	public void factorChanged(String factorLeft, String factorRight) {

		this.factor[0] = factorLeft;
		this.factor[1] = factorRight;
		processingController.setFactor(this.factor);
	}

	public void setVizualisation(String visuLeft, String visuRight) {
		this.visualisation[0] = visuLeft;
		this.visualisation[1] = visuRight;
		processingController.setVisualisation(this.visualisation);

	}

	public void setBase(String baseLeft, String baseRight) {

		this.base[0] = baseLeft;
		this.base[1] = baseRight;
		processingController.setBase(this.base);
	}

	/**
	 * �ffnet den Verbindungslader mit 2 Listen der verf�gbaren St�dte aus denen der Nutzer ausw�hlen kann 
	 **/
	public void loadData() {

		City cityList = null;
		City cityList2 = null;
		try {
			cityList = new City(true);
			cityList2 = new City(false);
		} catch (InstantiationException | IllegalAccessException | ClassNotFoundException | SQLException e) {
			e.printStackTrace();
		}

		dataLoader = new DataLoader(this, cityList.getNames(), cityList2.getNames());

	}
	
	
	/**
	 * erzeugt PDF-Bilder am angegebenen Pfad
	 * @param path
	 */
		public void takeScreenshot(String path) {
			processingController.takeScreenshot(path);
		}

		/**
		 * reset der Processing-Sketches ohne neue Daten einzulesen
		 */
		public void reset() {

			processingController.createSketches(this.groupedConnectionsLeft, this.groupedConnectionsRight,
					this.referenceConnectionsLeft, this.referenceConnectionsRight);

		}

		public void setNormal(boolean selected, boolean selected2) {

			this.normal[0] = selected;
			this.normal[1] = selected2;

			processingController.setNormal(this.normal);
		}
	
	

	/**
	 * Nach dem Schlie�en des Verbindungslader-Windows, l�dt die gew�nschten Verbindungen aus der Datenbank
	 * 
	 * @param connectionsList = Liste der Verbindungen
	 * @param carTrain = Datens�tze Zug oder Auto
	 * @param visu = linke oder rechte Visualisierung
	 * @param period = Zeitbereich bei Zugdaten
	 */
	public void loaderClosed(String[][] connectionsList, String carTrain, String visu, String period) {

		City cityList = null;

		// Daten f�r Auto gew�nscht
		if (carTrain.equals("Car")) {

			// Datenlisten anlegen
			GroupedConnections tempGroupedConnections = new GroupedConnections();
			ArrayList<Connection> connections = new ArrayList<Connection>();
			GroupedConnections referenceConnections = new GroupedConnections();
			ArrayList<Connection> referencesComplete = new ArrayList<Connection>();
			ArrayList<Connection> referencesPool = new ArrayList<Connection>();
			ArrayList<Connection> referencesCommonPool = new ArrayList<Connection>();
			// zum Mitschreiben einiger Durchschnittswerte
			float routesAvg = 0;
			float durationsAvg = 0;
			float distancesAvg = 0;

			// Laden einer St�dteliste inkl. Lat/Lon-Angaben
			try {
				cityList = new City(true);
			} catch (InstantiationException | IllegalAccessException | ClassNotFoundException | SQLException e1) {
				e1.printStackTrace();
			}

			// Laden der Angeforderten Verbindungen, einzeln
			CarRoute tempRoute = null;
			for (int i = 0; i < connectionsList.length; i++) {

				try {
					tempRoute = new CarRoute(connectionsList[i][0], connectionsList[i][1]);
					if (tempRoute.getOrigin() == null || tempRoute.getDestination() == null) {
						tempRoute = new CarRoute(connectionsList[i][1], connectionsList[i][0]);
					}

				} catch (InstantiationException | IllegalAccessException | ClassNotFoundException | SQLException e) {

					e.printStackTrace();
				}

				//hinzuf�gen der Routendaten in ein Array f�r die Visualisierung
				try {
					connections.add(new Connection(tempRoute.getOrigin(), tempRoute.getDestination(),
							cityList.getLatitude(tempRoute.getOrigin()), cityList.getLongitude(tempRoute.getOrigin()),
							cityList.getLatitude(tempRoute.getDestination()),
							cityList.getLongitude(tempRoute.getDestination()), Calendar.getInstance(),
							(float) tempRoute.getDuration(), (float) tempRoute.getDistance(),
							(float) tempRoute.specificRouteAvg(tempRoute.getOrigin(), tempRoute.getDestination())));
				} catch (InstantiationException | IllegalAccessException | ClassNotFoundException | SQLException e) {

					e.printStackTrace();
				}
				try {
					routesAvg += tempRoute.specificRouteAvg(tempRoute.getOrigin(), tempRoute.getDestination());
				} catch (InstantiationException | IllegalAccessException | ClassNotFoundException | SQLException e) {

					e.printStackTrace();
				}
				durationsAvg += tempRoute.getDuration();
				distancesAvg += tempRoute.getDistance();

			}
			
			//die entstandene Liste wird in eine weitere Liste geschrieben
			//diese doppelte Verschachtelung kommt bei Zugverbindungen zum Tragen
			//da hier mehrere wie oben erzeugte Listen die Verbindungen zu verschiedenen
			//Zeitpunkten beschreiben
			tempGroupedConnections.addConnections(connections);

			//Erzeugen der Listen f�r die Referenzwerte
			float refComplete = 0, refPool, distComplete = 0, distPool, durComplete = 0, durPool, refCommonComplete = 0,
					distCommonComplete = 0, durCommonComplete = 0;

			//Auslesen von Referenzwerten aus der Datenbank
			try {
				//f�r den Gesamtdurchschnitt
				refComplete = (float) tempRoute.completePoolAvg(true);
				distComplete = (float) tempRoute.getCompletePoolDistance();
				durComplete = (float) tempRoute.getCompletePoolDuration();
			} catch (InstantiationException | IllegalAccessException | ClassNotFoundException | SQLException e) {

				e.printStackTrace();
			}
			try {
				//f�r Verbindungen in Auto und Bahn
				refCommonComplete = (float) tempRoute.completePoolAvg(false);
				distCommonComplete = (float) tempRoute.getCompletePoolDistance();
				durCommonComplete = (float) tempRoute.getCompletePoolDuration();
			} catch (InstantiationException | IllegalAccessException | ClassNotFoundException | SQLException e) {

				e.printStackTrace();
			}

			//Berechnen der Referenzwerte f�r die Ausgew�hlten Verbindungen
			refPool = routesAvg / connectionsList.length;
			durPool = durationsAvg / connectionsList.length;
			distPool = distancesAvg / connectionsList.length;

			//Schreiben der Referenzwerte in eine verschachtelte Liste
			for (int i = 0; i < tempGroupedConnections.sizeOfGroups(); i++) {
				referencesComplete.add(new Connection(tempGroupedConnections.get(i).getOrigin(),
						tempGroupedConnections.get(i).getDestination(),
						cityList.getLatitude(tempGroupedConnections.get(i).getOrigin()),
						cityList.getLongitude(tempGroupedConnections.get(i).getOrigin()),
						cityList.getLatitude(tempGroupedConnections.get(i).getDestination()),
						cityList.getLongitude(tempGroupedConnections.get(i).getDestination()), Calendar.getInstance(),
						durComplete, distComplete, refComplete));
				referencesPool.add(new Connection(tempGroupedConnections.get(i).getOrigin(),
						tempGroupedConnections.get(i).getDestination(),
						cityList.getLatitude(tempGroupedConnections.get(i).getOrigin()),
						cityList.getLongitude(tempGroupedConnections.get(i).getOrigin()),
						cityList.getLatitude(tempGroupedConnections.get(i).getDestination()),
						cityList.getLongitude(tempGroupedConnections.get(i).getDestination()), Calendar.getInstance(),
						durPool, distPool, refPool));
				referencesCommonPool.add(new Connection(tempGroupedConnections.get(i).getOrigin(),
						tempGroupedConnections.get(i).getDestination(),
						cityList.getLatitude(tempGroupedConnections.get(i).getOrigin()),
						cityList.getLongitude(tempGroupedConnections.get(i).getOrigin()),
						cityList.getLatitude(tempGroupedConnections.get(i).getDestination()),
						cityList.getLongitude(tempGroupedConnections.get(i).getDestination()), Calendar.getInstance(),
						durCommonComplete, distCommonComplete, refCommonComplete));
			}
			referenceConnections.addConnections(referencesComplete);
			referenceConnections.addConnections(referencesPool);
			referenceConnections.addConnections(referencesCommonPool);

			//Auswahl, an welche Visualisierung die Daten gesendet werden und �bergabe
			if (visu.equals("Both")) {
				this.carTrain[0] = carTrain;
				this.carTrain[1] = carTrain;
				this.groupedConnectionsLeft = tempGroupedConnections;
				this.groupedConnectionsRight = tempGroupedConnections;
				this.referenceConnectionsLeft = referenceConnections;
				this.referenceConnectionsRight = referenceConnections;

				processingController.setcarTrain(this.carTrain);
				setConnectionData(groupedConnectionsLeft, groupedConnectionsRight, referenceConnectionsLeft,
						referenceConnectionsRight);
			}

			if (visu.equals("Left")) {
				this.carTrain[0] = carTrain;
				this.carTrain[1] = carTrain;
				this.groupedConnectionsLeft = tempGroupedConnections;
				this.referenceConnectionsLeft = referenceConnections;

				processingController.setcarTrain(this.carTrain);
				setConnectionData(groupedConnectionsLeft, groupedConnectionsRight, referenceConnectionsLeft,
						referenceConnectionsRight);
			}

			if (visu.equals("Right")) {
				this.carTrain[0] = carTrain;
				this.carTrain[1] = carTrain;

				this.groupedConnectionsRight = tempGroupedConnections;
				this.referenceConnectionsRight = referenceConnections;

				processingController.setcarTrain(this.carTrain);
				setConnectionData(groupedConnectionsLeft, groupedConnectionsRight, referenceConnectionsLeft,
						referenceConnectionsRight);
			}

		}

		//alle folgenden Analog zu "Car"
		if (carTrain.equals("Train")) {

			long now = System.currentTimeMillis();

			if (period.equals("All")) {

				GroupedConnections tempGroupedConnections = new GroupedConnections();
				ArrayList<Connection> connections = new ArrayList<Connection>();

				GroupedConnections referenceConnections = new GroupedConnections();
				ArrayList<Connection> referencesComplete = new ArrayList<Connection>();
				ArrayList<Connection> referencesPool = new ArrayList<Connection>();
				ArrayList<Connection> referencesCommonPool = new ArrayList<Connection>();

				float routesAvg = 0;
				float durationsAvg = 0;
				float distancesAvg = 0;

				try {

					cityList = new City(false);

				} catch (InstantiationException | IllegalAccessException | ClassNotFoundException | SQLException e1) {

					e1.printStackTrace();
				}

				TrainRoute tempRoute = null;
				for (int i = 0; i < connectionsList.length; i++) {

					now = System.currentTimeMillis();
					try {
						tempRoute = new TrainRoute(connectionsList[i][0], connectionsList[i][1]);
						if (tempRoute.getOrigin() == null || tempRoute.getDestination() == null) {
							tempRoute = new TrainRoute(connectionsList[i][1], connectionsList[i][0]);

						}
						// System.out.println(System.currentTimeMillis() - now +
						// " - " + i +" - loading route from db");
						now = System.currentTimeMillis();

					} catch (InstantiationException | IllegalAccessException | ClassNotFoundException
							| SQLException e) {

						e.printStackTrace();
					}

					connections.add(new Connection(tempRoute.getOrigin(), tempRoute.getDestination(),
							cityList.getLatitude(tempRoute.getOrigin()), cityList.getLongitude(tempRoute.getOrigin()),
							cityList.getLatitude(tempRoute.getDestination()),
							cityList.getLongitude(tempRoute.getDestination()), Calendar.getInstance(),
							(float) tempRoute.getDuration(), (float) tempRoute.getDistance(),
							(float) tempRoute.getCalculatedAverage()));

					routesAvg += tempRoute.getCalculatedAverage();
					durationsAvg += tempRoute.getDuration();
					distancesAvg += tempRoute.getDistance();

				}

				tempGroupedConnections.addConnections(connections);

				float refComplete = 0, refPool, distComplete = 0, distPool, durComplete = 0, durPool,
						refCommonComplete = 0, distCommonComplete = 0, durCommonComplete = 0;

				try {
					refComplete = (float) tempRoute.completePoolAvg();
					distComplete = (float) tempRoute.getCompletePoolDistance();
					durComplete = (float) tempRoute.getCompletePoolDuration();
					refCommonComplete = refComplete;
					distCommonComplete = distComplete;
					durCommonComplete = durComplete;

				} catch (InstantiationException | IllegalAccessException | ClassNotFoundException | SQLException e) {

					e.printStackTrace();
				}

				refPool = routesAvg / connectionsList.length;
				durPool = durationsAvg / connectionsList.length;
				distPool = distancesAvg / connectionsList.length;

				for (int i = 0; i < tempGroupedConnections.sizeOfGroups(); i++) {

					referencesComplete.add(new Connection(tempGroupedConnections.get(i).getOrigin(),
							tempGroupedConnections.get(i).getDestination(),
							cityList.getLatitude(tempGroupedConnections.get(i).getOrigin()),
							cityList.getLongitude(tempGroupedConnections.get(i).getOrigin()),
							cityList.getLatitude(tempGroupedConnections.get(i).getDestination()),
							cityList.getLongitude(tempGroupedConnections.get(i).getDestination()),
							Calendar.getInstance(), durComplete, distComplete, refComplete));
					referencesPool.add(new Connection(tempGroupedConnections.get(i).getOrigin(),
							tempGroupedConnections.get(i).getDestination(),
							cityList.getLatitude(tempGroupedConnections.get(i).getOrigin()),
							cityList.getLongitude(tempGroupedConnections.get(i).getOrigin()),
							cityList.getLatitude(tempGroupedConnections.get(i).getDestination()),
							cityList.getLongitude(tempGroupedConnections.get(i).getDestination()),
							Calendar.getInstance(), durPool, distPool, refPool));
					referencesCommonPool.add(new Connection(tempGroupedConnections.get(i).getOrigin(),
							tempGroupedConnections.get(i).getDestination(),
							cityList.getLatitude(tempGroupedConnections.get(i).getOrigin()),
							cityList.getLongitude(tempGroupedConnections.get(i).getOrigin()),
							cityList.getLatitude(tempGroupedConnections.get(i).getDestination()),
							cityList.getLongitude(tempGroupedConnections.get(i).getDestination()),
							Calendar.getInstance(), durCommonComplete, distCommonComplete, refCommonComplete));

				}

				referenceConnections.addConnections(referencesComplete);
				referenceConnections.addConnections(referencesPool);
				referenceConnections.addConnections(referencesCommonPool);

				if (visu.equals("Both")) {
					this.carTrain[0] = carTrain;
					this.carTrain[1] = carTrain;
					this.groupedConnectionsLeft = tempGroupedConnections;
					this.groupedConnectionsRight = tempGroupedConnections;
					this.referenceConnectionsLeft = referenceConnections;
					this.referenceConnectionsRight = referenceConnections;

					processingController.setcarTrain(this.carTrain);
					setConnectionData(groupedConnectionsLeft, groupedConnectionsRight, referenceConnectionsLeft,
							referenceConnectionsRight);
				}

				if (visu.equals("Left")) {
					this.carTrain[0] = carTrain;
					this.carTrain[1] = carTrain;
					this.groupedConnectionsLeft = tempGroupedConnections;
					this.referenceConnectionsLeft = referenceConnections;

					processingController.setcarTrain(this.carTrain);
					setConnectionData(groupedConnectionsLeft, groupedConnectionsRight, referenceConnectionsLeft,
							referenceConnectionsRight);
				}

				if (visu.equals("Right")) {
					this.carTrain[0] = carTrain;
					this.carTrain[1] = carTrain;

					this.groupedConnectionsRight = tempGroupedConnections;
					this.referenceConnectionsRight = referenceConnections;

					processingController.setcarTrain(this.carTrain);
					setConnectionData(groupedConnectionsLeft, groupedConnectionsRight, referenceConnectionsLeft,
							referenceConnectionsRight);
				}

			}

			if (period.equals("Weekly")) {
				GroupedConnections tempGroupedConnections = new GroupedConnections();
				ArrayList<Connection> connections = new ArrayList<Connection>();

				GroupedConnections referenceConnections = new GroupedConnections();
				ArrayList<Connection> referencesComplete = new ArrayList<Connection>();
				ArrayList<Connection> referencesPool = new ArrayList<Connection>();
				ArrayList<Connection> referencesCommonPool = new ArrayList<Connection>();

				float routesAvg = 0;
				float durationsAvg = 0;
				float distancesAvg = 0;

				try {
					cityList = new City(false);
				} catch (InstantiationException | IllegalAccessException | ClassNotFoundException | SQLException e1) {

					e1.printStackTrace();
				}
				TrainRoute tempRoute = null;
				for (int week = 1; week <= 4; week++) {
					connections = new ArrayList<Connection>();

					for (int i = 0; i < connectionsList.length; i++) {

						now = System.currentTimeMillis();
						try {
							tempRoute = new TrainRoute(connectionsList[i][0], connectionsList[i][1], week);
							if (tempRoute.getOrigin() == null || tempRoute.getDestination() == null) {
								tempRoute = new TrainRoute(connectionsList[i][1], connectionsList[i][0], week);
							}

							// System.out.println(System.currentTimeMillis() -
							// now + " - " + i +" - loading route from db");
							now = System.currentTimeMillis();

						} catch (InstantiationException | IllegalAccessException | ClassNotFoundException
								| SQLException e) {

							e.printStackTrace();
						}

						connections.add(new Connection(tempRoute.getOrigin(), tempRoute.getDestination(),
								cityList.getLatitude(tempRoute.getOrigin()),
								cityList.getLongitude(tempRoute.getOrigin()),
								cityList.getLatitude(tempRoute.getDestination()),
								cityList.getLongitude(tempRoute.getDestination()), Calendar.getInstance(),
								(float) tempRoute.getDuration(), (float) tempRoute.getDistance(),
								(float) tempRoute.getCalculatedAverage()));

						routesAvg += tempRoute.getCalculatedAverage();

						durationsAvg += tempRoute.getDuration();
						distancesAvg += tempRoute.getDistance();

					}

					tempGroupedConnections.addConnections(connections);

				}

				float refComplete = 0, refPool, distComplete = 0, distPool, durComplete = 0, durPool,
						refCommonComplete = 0, distCommonComplete = 0, durCommonComplete = 0;

				try {
					refComplete = (float) tempRoute.completePoolAvg();
					distComplete = (float) tempRoute.getCompletePoolDistance();
					durComplete = (float) tempRoute.getCompletePoolDuration();
					refCommonComplete = refComplete;
					distCommonComplete = distComplete;
					durCommonComplete = durComplete;
				} catch (InstantiationException | IllegalAccessException | ClassNotFoundException | SQLException e1) {

					e1.printStackTrace();
				}

				refPool = routesAvg / connectionsList.length / 4;
				durPool = durationsAvg / connectionsList.length / 4;
				distPool = distancesAvg / connectionsList.length / 4;

				for (int i = 0; i < tempGroupedConnections.sizeOfGroups(); i++) {

					referencesComplete.add(new Connection(tempGroupedConnections.get(i).getOrigin(),
							tempGroupedConnections.get(i).getDestination(),
							cityList.getLatitude(tempGroupedConnections.get(i).getOrigin()),
							cityList.getLongitude(tempGroupedConnections.get(i).getOrigin()),
							cityList.getLatitude(tempGroupedConnections.get(i).getDestination()),
							cityList.getLongitude(tempGroupedConnections.get(i).getDestination()),
							Calendar.getInstance(), durComplete, distComplete, refComplete));
					referencesPool.add(new Connection(tempGroupedConnections.get(i).getOrigin(),
							tempGroupedConnections.get(i).getDestination(),
							cityList.getLatitude(tempGroupedConnections.get(i).getOrigin()),
							cityList.getLongitude(tempGroupedConnections.get(i).getOrigin()),
							cityList.getLatitude(tempGroupedConnections.get(i).getDestination()),
							cityList.getLongitude(tempGroupedConnections.get(i).getDestination()),
							Calendar.getInstance(), durPool, distPool, refPool));
					referencesCommonPool.add(new Connection(tempGroupedConnections.get(i).getOrigin(),
							tempGroupedConnections.get(i).getDestination(),
							cityList.getLatitude(tempGroupedConnections.get(i).getOrigin()),
							cityList.getLongitude(tempGroupedConnections.get(i).getOrigin()),
							cityList.getLatitude(tempGroupedConnections.get(i).getDestination()),
							cityList.getLongitude(tempGroupedConnections.get(i).getDestination()),
							Calendar.getInstance(), durCommonComplete, distCommonComplete, refCommonComplete));

				}

				referenceConnections.addConnections(referencesComplete);
				referenceConnections.addConnections(referencesPool);
				referenceConnections.addConnections(referencesCommonPool);

				if (visu.equals("Both")) {
					this.carTrain[0] = carTrain;
					this.carTrain[1] = carTrain;
					this.groupedConnectionsLeft = tempGroupedConnections;
					this.groupedConnectionsRight = tempGroupedConnections;
					this.referenceConnectionsLeft = referenceConnections;
					this.referenceConnectionsRight = referenceConnections;

					processingController.setcarTrain(this.carTrain);
					setConnectionData(groupedConnectionsLeft, groupedConnectionsRight, referenceConnectionsLeft,
							referenceConnectionsRight);
				}

				if (visu.equals("Left")) {
					this.carTrain[0] = carTrain;
					this.carTrain[1] = carTrain;
					this.groupedConnectionsLeft = tempGroupedConnections;
					this.referenceConnectionsLeft = referenceConnections;

					processingController.setcarTrain(this.carTrain);
					setConnectionData(groupedConnectionsLeft, groupedConnectionsRight, referenceConnectionsLeft,
							referenceConnectionsRight);
				}

				if (visu.equals("Right")) {
					this.carTrain[0] = carTrain;
					this.carTrain[1] = carTrain;

					this.groupedConnectionsRight = tempGroupedConnections;
					this.referenceConnectionsRight = referenceConnections;

					processingController.setcarTrain(this.carTrain);
					setConnectionData(groupedConnectionsLeft, groupedConnectionsRight, referenceConnectionsLeft,
							referenceConnectionsRight);
				}
			}

			if (period.equals("Daily")) {
				GroupedConnections tempGroupedConnections = new GroupedConnections();
				ArrayList<Connection> connections = new ArrayList<Connection>();

				GroupedConnections referenceConnections = new GroupedConnections();
				ArrayList<Connection> referencesComplete = new ArrayList<Connection>();
				ArrayList<Connection> referencesPool = new ArrayList<Connection>();
				ArrayList<Connection> referencesCommonPool = new ArrayList<Connection>();

				float routesAvg = 0;
				float durationsAvg = 0;
				float distancesAvg = 0;

				try {
					cityList = new City(false);
				} catch (InstantiationException | IllegalAccessException | ClassNotFoundException | SQLException e1) {

					e1.printStackTrace();
				}
				TrainRoute tempRoute = null;
				for (int day = 1; day <= 31; day++) {
					connections = new ArrayList<Connection>();
					for (int i = 0; i < connectionsList.length; i++) {

						String dayToString = "";
						if (day < 10) {
							dayToString += "0";
						}
						dayToString += day;
						now = System.currentTimeMillis();
						try {
							tempRoute = new TrainRoute(connectionsList[i][0], connectionsList[i][1], dayToString);
							if (tempRoute.getOrigin() == null || tempRoute.getDestination() == null) {
								tempRoute = new TrainRoute(connectionsList[i][1], connectionsList[i][0], dayToString);
							}

							System.out.println(System.currentTimeMillis() - now + " - day " + day + " - conn " + i
									+ " - loading route from db");
							now = System.currentTimeMillis();

						} catch (InstantiationException | IllegalAccessException | ClassNotFoundException
								| SQLException e) {

							e.printStackTrace();
						}

						connections.add(new Connection(tempRoute.getOrigin(), tempRoute.getDestination(),
								cityList.getLatitude(tempRoute.getOrigin()),
								cityList.getLongitude(tempRoute.getOrigin()),
								cityList.getLatitude(tempRoute.getDestination()),
								cityList.getLongitude(tempRoute.getDestination()), Calendar.getInstance(),
								(float) tempRoute.getDuration(), (float) tempRoute.getDistance(),
								(float) tempRoute.getCalculatedAverage()));

						routesAvg += tempRoute.getCalculatedAverage();

						durationsAvg += tempRoute.getDuration();
						distancesAvg += tempRoute.getDistance();

					}

					tempGroupedConnections.addConnections(connections);

				}

				float refComplete = 0, refPool, distComplete = 0, distPool, durComplete = 0, durPool,
						refCommonComplete = 0, distCommonComplete = 0, durCommonComplete = 0;

				try {
					refComplete = (float) tempRoute.completePoolAvg();
					distComplete = (float) tempRoute.getCompletePoolDistance();
					durComplete = (float) tempRoute.getCompletePoolDuration();
					refCommonComplete = refComplete;
					distCommonComplete = distComplete;
					durCommonComplete = durComplete;
				} catch (InstantiationException | IllegalAccessException | ClassNotFoundException | SQLException e1) {

					e1.printStackTrace();
				}

				refPool = routesAvg / connectionsList.length / 31;
				durPool = durationsAvg / connectionsList.length / 31;
				distPool = distancesAvg / connectionsList.length / 31;

				for (int i = 0; i < tempGroupedConnections.sizeOfGroups(); i++) {

					referencesComplete.add(new Connection(tempGroupedConnections.get(i).getOrigin(),
							tempGroupedConnections.get(i).getDestination(),
							cityList.getLatitude(tempGroupedConnections.get(i).getOrigin()),
							cityList.getLongitude(tempGroupedConnections.get(i).getOrigin()),
							cityList.getLatitude(tempGroupedConnections.get(i).getDestination()),
							cityList.getLongitude(tempGroupedConnections.get(i).getDestination()),
							Calendar.getInstance(), durComplete, distComplete, refComplete));
					referencesPool.add(new Connection(tempGroupedConnections.get(i).getOrigin(),
							tempGroupedConnections.get(i).getDestination(),
							cityList.getLatitude(tempGroupedConnections.get(i).getOrigin()),
							cityList.getLongitude(tempGroupedConnections.get(i).getOrigin()),
							cityList.getLatitude(tempGroupedConnections.get(i).getDestination()),
							cityList.getLongitude(tempGroupedConnections.get(i).getDestination()),
							Calendar.getInstance(), durPool, distPool, refPool));
					referencesCommonPool.add(new Connection(tempGroupedConnections.get(i).getOrigin(),
							tempGroupedConnections.get(i).getDestination(),
							cityList.getLatitude(tempGroupedConnections.get(i).getOrigin()),
							cityList.getLongitude(tempGroupedConnections.get(i).getOrigin()),
							cityList.getLatitude(tempGroupedConnections.get(i).getDestination()),
							cityList.getLongitude(tempGroupedConnections.get(i).getDestination()),
							Calendar.getInstance(), durCommonComplete, distCommonComplete, refCommonComplete));

				}

				referenceConnections.addConnections(referencesComplete);
				referenceConnections.addConnections(referencesPool);
				referenceConnections.addConnections(referencesCommonPool);

				if (visu.equals("Both")) {
					this.carTrain[0] = carTrain;
					this.carTrain[1] = carTrain;
					this.groupedConnectionsLeft = tempGroupedConnections;
					this.groupedConnectionsRight = tempGroupedConnections;
					this.referenceConnectionsLeft = referenceConnections;
					this.referenceConnectionsRight = referenceConnections;

					processingController.setcarTrain(this.carTrain);
					setConnectionData(groupedConnectionsLeft, groupedConnectionsRight, referenceConnectionsLeft,
							referenceConnectionsRight);
				}

				if (visu.equals("Left")) {
					this.carTrain[0] = carTrain;
					this.carTrain[1] = carTrain;
					this.groupedConnectionsLeft = tempGroupedConnections;
					this.referenceConnectionsLeft = referenceConnections;

					processingController.setcarTrain(this.carTrain);
					setConnectionData(groupedConnectionsLeft, groupedConnectionsRight, referenceConnectionsLeft,
							referenceConnectionsRight);
				}

				if (visu.equals("Right")) {
					this.carTrain[0] = carTrain;
					this.carTrain[1] = carTrain;

					this.groupedConnectionsRight = tempGroupedConnections;
					this.referenceConnectionsRight = referenceConnections;

					processingController.setcarTrain(this.carTrain);
					setConnectionData(groupedConnectionsLeft, groupedConnectionsRight, referenceConnectionsLeft,
							referenceConnectionsRight);
				}
			}

		}

	}

	

}
